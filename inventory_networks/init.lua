--[[
Minetest+specing Client-Side Mod (CSM) for automatically moving items between inventories
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

-- general idea:
--   register multiple locations (node, inventory, detached) hereafter refered to as 'machines'
--   each machine can be either a converter or storage
--   each converter has an associated recipe
--   -> accepts predetermined amount of different items and produces an amount of output items
--   -> for now, we assume that output is only one. (battery recipe has two - bucket and battery)
--
-- in each step, we:
-- 1) pick one machine from the list
-- 2) go through all other machines and collect available amount of input material
-- 3) move as much input material as possible (up to max_stack), such that there will be no recipe leftovers

-- TODO: alloyers are a bit special... they can take 3 input per output item while only having 2 input stacks.

local mod_storage = core.get_mod_storage()
local invnet = minetest.deserialize(mod_storage:get_string("network")) or {}
invnet.enable   = invnet.enable or false
invnet.machines = invnet.machines or {}
invnet.stack_max= invnet.stack_max or {}
invnet.log_level= nil -- migrated to .log
local l = library.Logger:new("invnet")
if invnet.recipes then -- migrate recipes
	library.load_recipes(invnet.recipes)
	invnet.recipes = nil
	mod_storage:set_string("network", minetest.serialize(invnet))
end



invnet.machine_type = invnet.machine_type or {
	["biofuel:refinery"] = "refining",
	["biofuel:refinery_active"] = "refining",
	["technic:hv_grinder"] = "grinding",
	["technic:hv_furnace"] = "cooking",
	["technic:hv_electric_furnace"] = "cooking",
}


library.register_on_jumpdrive_jump(function (jump)
	l:debug("Translating inventory network coordinates on jump..")
	for _,m in pairs(invnet.machines) do
		if m.pos then
			library.translate_position_if_required(jump, m.pos)
		end
	end
	mod_storage:set_string("network", minetest.serialize(invnet))
end)


local function invnet_reset()
	invnet.enable = false
	invnet.machines = {}
end


local function invnet_list(machines)
	for i,m in pairs(machines) do
		l:info("Machine #" .. i .. " at " .. library.inventory_location_to_string(m.pos) .. ": ")
		for name,count in pairs(m.recipe or {}) do
			l:info("  recipe consumes " .. count .. " of " .. name)
			l:debug("    from input list name " .. m.input_list_name)
		end
		for name,count in pairs(m.output or {}) do
			l:info("  recipe produces " .. count .. " of " .. name)
			l:debug("    in output list name " .. m.output_list_name)
		end

	end
end



-- returns true on success, false on failure
local function invnet_setup_item_move(machine)
	-- for each input item adjust total max_stack to minimum of them all, then
	-- try to move at once as many stacks as there are items in the recipe
	local sources = {}
	local max_stack = math.huge -- max transfer size
	local total_stacks = 0

	for item_name,_ in pairs(machine.recipe) do
		max_stack = math.min(max_stack, ItemStack(item_name):get_stack_max())
		if invnet.stack_max[item_name] then
			max_stack = math.min(max_stack, invnet.stack_max[item_name])
		end
		--l:debug("  corrected stack max to " .. max_stack .. " for " .. item_name)
	end
	l:debug("  corrected stack max to " .. max_stack)

	for item_name,count in pairs(machine.recipe) do
		local from = library.ItemLocations:new()
		for _,src_machine in pairs(invnet.machines) do
			-- Prevent items being moved back and forth if there are multiple machines
			-- registered as a destination for this item:
			-- We have three kinds of locations:
			--   1) machines taking that item as recipe, with limited storage capacity
			--   2) buffer chests taking that item as recipe. Their output list name == input list name
			--   3) everything else
			-- Ok moves: 3->1, 3->2, 2->1 (in decreasing priority)
			-- Bad moves: 2->2
			-- Do not take if machine has such item in its recipe
			if library.distance(src_machine.pos, machine.pos) < library.get_item_range()
			and	(  not src_machine.recipe
				or not src_machine.recipe[item_name]
				-- at this point the source machine clearly has this item in its recipe.
				-- Only take the item if the source machine is a chest and the destination is not a chest
				--machine.output_list_name -- this is not a chest
				or src_machine.input_list_name == "main" and machine.input_list_name ~= "main"
				) then
				for src_list_name,_ in pairs(core.get_inventory(src_machine.pos) or {}) do
					from:collect(src_machine.pos, src_list_name, item_name)
				end
				--from:collect(src_machine.pos, src_machine.output_list_name, item_name)
			end
		end

		from:sort_by_count_dsc() -- larger stacks first
		for i = 1,count do
			if i > #from.locations or from.locations[i].count < max_stack then
				l:debug("  insufficient stacks for item " .. item_name .. ", got only " .. (i-1))
				return false
			end
		end
		sources[item_name] = from
		total_stacks = total_stacks + count
	end

	-- Do we have enough free slots to deposit items to?
	local to = library.ItemLocations:new()
	to:collect(machine.pos, machine.input_list_name, "")
	if #to.locations < total_stacks then
		l:debug("  insufficient space to deposit " .. total_stacks .. " stacks")
		return false
	end

	-- If we made it to here, then we have enough items to craft the recipe => setup moving
	-- list of from_loc & to_loc & count
	local move_list = {}
	local dst_i = 1

	for item_name,from in pairs(sources) do
		local count = machine.recipe[item_name]

		for i = 1,count do
			local src_loc = from.locations[i]
			local dst_loc = to.locations[dst_i]

			dst_i = dst_i + 1
			l:debug("  will move " .. max_stack .. "*" .. item_name
			        .. " from " .. src_loc:to_string() .. " to " .. dst_loc:to_string())
			table.insert(move_list, { src = src_loc, dst = dst_loc, count = max_stack })
		end
	end

	-- schedule actual moves
	for i,item in pairs(move_list) do
		queue:prepend(function() return library.queued_move_stack(item.src, item.dst, item.count) end)
		if i % 5 == 4 then -- add a small delay
			queue:prepend(function() return 0.2 end)
		end
	end

	return true
end


local step_machine_last_index = 1
local function invnet_step()
	if not invnet.enable then return end
	--l:debug("inventory network step")
	local machines = invnet.machines

	local step_machine_index = step_machine_last_index
	while true do
		-- increment and wrap around
		step_machine_index = (step_machine_index % (#machines)) + 1
		--l:debug(" testing index " .. step_machine_index)
		local machine = machines[step_machine_index]
		if not machine then
			l:err("machine " .. step_machine_index .. " does not exist")
		end

		if machine and library.distance(machine.pos, nil) < library.get_item_range() then
			l:debug("Selected machine at " .. library.inventory_location_to_string(machine.pos))

			if machine.recipe and invnet_setup_item_move(machine) then
				step_machine_last_index = step_machine_index
				core.after(0.5, invnet_step) -- use after to not block other tasks
				queue:walk()
				return true
			end

			-- Else machine setup was unsuccessful, see if its input is empty and we can reconfigure it
			if machine.input_list_name and machine.pos then
				local list = core.get_inventory(machine.pos)
				list = list and list[machine.input_list_name]

				if list and library.is_list_empty(list) then
					l:debug("empty input at: " .. library.inventory_location_to_string(machine.pos))
					-- Try to assign another, random recipe to this machine
					local node = minetest.get_node_or_nil(machine.pos)
					local m_type = invnet.machine_type[node.name]
					local recipe_def = library.get_random_recipe(m_type)
					if recipe_def then
						l:debug("Reconfiguring machine " .. node.name .. " at index " .. step_machine_index)
						machine.recipe = library.recipe_items_to_counts(recipe_def.items)
					end
				end
			end
		end

		-- Have we wrapped around yet? (did not find any suitable machine)
		if step_machine_index == step_machine_last_index then
			core.after(2, invnet_step) -- use after to not block other tasks
			return false
		end
	end
end


local function get_machine_in_list(pos)
	for i,m in pairs(invnet.machines) do
		if library.distance(m.pos, pos) < 0.1 then
			return i
		end
	end
	return #invnet.machines+1
end


local wait_for_punch = false
library.register_on_punchnode_once_per_pos(function(pos, node)
	if not wait_for_punch then return end

	local machine_index = get_machine_in_list(pos)
	if invnet.machines[machine_index] then
		l:info("node has already been added as index " .. tostring(machine_index))
		return
	end

	if node.name == "pipeworks:autocrafter" then
		local inv = core.get_inventory(pos)
		local item_string = inv["output"][1]:get_name()

		if item_string == "" then
			l:err("autocrafter unconfigured")
		else
			local output_gain = inv["output"][1]:get_count()
			local m = {}
			m.recipe = library.recipe_items_to_counts(inv["recipe"])
			m.input_list_name = "src"
			m.output = {
				[item_string] = output_gain
			}
			m.output_list_name = "dst"
			m.pos = pos
			invnet.machines[machine_index] = m
			l:info("Added autocrafter for: " .. output_gain .. " of " .. item_string)
		end
	else
		l:info("Unknown node punched: " .. node.name .. " adding as chest. Use .invnet config to [re]configure.")
		local inv = core.get_inventory(pos)
		if not inv then
			l:err("node has no inventory, aborting.")
			return
		end

		local m = {}

		if inv["src"] then
			m.input_list_name = "src"
		elseif inv["input"] then
			m.input_list_name = "input"
		elseif inv["main"] then
			m.input_list_name = "main"
		end

		if inv["dst"] then
			m.output_list_name = "dst"
		elseif inv["output"] then
			m.output_list_name = "output"
		elseif inv["main"] then
			m.output_list_name = "main"
		end

		m.pos = pos
		invnet.machines[machine_index] = m

		if m.input_list_name and m.output_list_name then
			l:info("Added id " .. machine_index)
		else
			l:err("Added id " .. machine_index .. " with missing inventory lists, please fix")
		end
	end

	mod_storage:set_string("network", minetest.serialize(invnet))
end)


local function help()
	return ".invnet: inventory networks (automatic item moving between inventories), usage:\n"
		.. " nb: for recipe manipulation see .recipe\n"
	    .. ".invnet on|off|start|stop -- replace pipeworks :)\n"
	    .. ".invnet add -- toggles adding nodes to the network\n"
	    .. ".invnet addself -- add my player inventory to the network, use config to setup\n"
	    .. ".invnet config <n> recipe [crafting grid slot 1 itemstring] ... -- changes machine's recipe\n"
	    .. ".invnet config <n> ilist|olist <string> -- changes machine's input/output lists\n"
		.. ".invnet mtype add <nodename> <string> -- sets machine type for all these nodes\n"
		.. ".invnet mtype del <nodename> -- clears machine type for all these nodes\n"
		.. ".invnet mtype list -- list machine types\n"
	    .. ".invnet del <n> -- delete machine <n> from network and shift everything down\n"
	    .. ".invnet list -- lists all inventories in the network\n"
	    .. ".invnet reset -- clears the entire network\n"
	    .. ".invnet stackmax <itemstring> <n> -- decrease transfer stack size to force more frequent transfers\n"
end


core.register_chatcommand("invnet", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "add" then
			if wait_for_punch then
				wait_for_punch = false
			else
				l:info("punch nodes to add them into inventory network. repeat this command to stop")
				wait_for_punch = true
			end
		elseif args[1] == "addself" then
			local machine_index = get_machine_in_list(nil)
			local m = {}
			m.input_list_name = "main"
			m.output_list_name = "main"
			m.pos = nil
			invnet.machines[machine_index] = m
			mod_storage:set_string("network", minetest.serialize(invnet))

		elseif args[1] == "config" then
			local index = tonumber(args[2])
			if not index or not invnet.machines[index] then
				l:err(help() .. "Must provide index!")
				return
			end

			local m = invnet.machines[index]
			if args[3] == "recipe" then
				m.recipe = {}
				for item_i = 4,#args do
					local item = args[item_i]
					m.recipe[item] = (m.recipe[item] or 0) + 1
				end
				invnet_list({ [index] = m })
			elseif args[3] == "ilist" and args[4] then
				m.input_list_name = args[4]
			elseif args[3] == "olist" and args[4] then
				m.output_list_name = args[4]
			else
				l:err(help() .. "what to configure?")
			end
			mod_storage:set_string("network", minetest.serialize(invnet))

		elseif args[1] == "del" then
			local index = tonumber(args[2])
			if not index or not invnet.machines[index] then
				l:err(help() .. "Must provide index!")
				return
			end

			local top = #invnet.machines
			for i = index, top - 1 do
				invnet.machines[i] = invnet.machines[i+1]
			end
			invnet.machines[top] = nil
			mod_storage:set_string("network", minetest.serialize(invnet))

		elseif args[1] == "mtype" then
			local command = args[2]
			if command == "add" then
				if args[3] and args[4] then
					invnet.machine_type[args[3]] = args[4]
					mod_storage:set_string("network", minetest.serialize(invnet))
				else
					l:err(help() .. " which nodename? Which type?")
				end

			elseif command == "del" or command == "delete" and args[3] then
				invnet.machine_type[args[3]] = nil
				mod_storage:set_string("network", minetest.serialize(invnet))
			elseif command == "list" then
				for nodename,mtype in pairs(invnet.machine_type) do
					l:info("  '" .. nodename .. "' is registered as '" .. mtype .. "'")
				end
			else
				l:err(help())
			end
		elseif args[1] == "stackmax" and args[2] then
			local max = tonumber(args[3])
			if max then
				invnet.stack_max[args[2]] = max
				mod_storage:set_string("network", minetest.serialize(invnet))
			else
				l:err("Stack max is not a number: '" .. args[3] .. "'")
			end
		elseif args[1] == "list" then
			invnet_list(invnet.machines)
		elseif args[1] == "reset" then
			invnet_reset()
		elseif args[1] == "on" or args[1] == "start" then
			invnet.enable = true
			invnet_step()
		elseif args[1] == "off" or args[1] == "stop" then
			invnet.enable = false
		else
			l:info(help())
		end
	end,
})

core.after(2, invnet_step)
