--[[
Inventory management CSM - slab refill
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

core.register_on_placenode(function(_, node)
	if node.name == "moreblocks:slab_super_glow_glass_1" then
		local my_inv = core.get_inventory()
		local slab_index = library.find_item_index(my_inv.main, "moreblocks:slab_super_glow_glass_1")

		if not slab_index then
			print("No slabs in inventory?")
			return
		end

		local stack = my_inv["main"][slab_index]
		if stack:get_count() > 1 then
			print("Still " .. tostring(stack:get_count()) .. " > 1 slabs remain, skipping")
			return
		end

		local inv_saw_inv_name = "invsaw_" .. core.localplayer:get_name()
		local inv_saw = core.get_inventory(inv_saw_inv_name)
		if not inv_saw then
			print("Inventory saw's inventory is missing?")
			return
		end

		local sgg_index = library.find_item_index(my_inv["main"], "moreblocks:super_glow_glass")
		if not sgg_index then
			print("No more SGG in main, skipping")
			return
		end

		-- Push SGG into saw and pull out the slabs.
		core.move_stack(nil, "main", sgg_index, inv_saw_inv_name, "input", 1, 1)
		core.move_stack(inv_saw_inv_name, "output", 18, nil, "main", slab_index, 8)
	end
end)
