--[[
Minetest+specing Client-Side Mod (CSM) for moving stuff from nodes to a drawer controller
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local l = library.Logger:new("inv_to_drawers")
-- TODO: digtron item excludes that are not hardcoded as below


local digtron_node_names = {}
-- grep --no-filename --only-matching -r 'register_node.*digtron:.*",' | sed -e 's/register_node(/digtron_node_names[/' -e 's/,/] = 0/' | sort
digtron_node_names["digtron:auto_controller"] = 0
digtron_node_names["digtron:axle"] = 0
digtron_node_names["digtron:battery_holder"] = 0
digtron_node_names["digtron:builder"] = 0
digtron_node_names["digtron:combined_storage"] = 1
digtron_node_names["digtron:controller"] = 0
digtron_node_names["digtron:corner_panel"] = 0
digtron_node_names["digtron:digger"] = 0
digtron_node_names["digtron:dual_digger"] = 0
digtron_node_names["digtron:dual_soft_digger"] = 0
--digtron_node_names["digtron:duplicator"] = 0 not really part of a digtron
digtron_node_names["digtron:edge_panel"] = 0
--digtron_node_names["digtron:empty_crate"] = 0
--digtron_node_names["digtron:empty_locked_crate"] = 0
digtron_node_names["digtron:fuelstore"] = 0
digtron_node_names["digtron:intermittent_digger"] = 0
digtron_node_names["digtron:intermittent_soft_digger"] = 0
digtron_node_names["digtron:inventory"] = 1
digtron_node_names["digtron:inventory_ejector"] = 0
digtron_node_names["digtron:light"] = 0
--digtron_node_names["digtron:loaded_crate"] = 0
--digtron_node_names["digtron:loaded_locked_crate"] = 0
digtron_node_names["digtron:panel"] = 0
digtron_node_names["digtron:power_connector"] = 0
digtron_node_names["digtron:pusher"] = 0
digtron_node_names["digtron:soft_digger"] = 0
digtron_node_names["digtron:structure"] = 0

function is_digtron(node_name)
	return digtron_node_names[node_name]
end



local state = {
	controller_pos = nil,
	-- list of positions
	sources = {},

	moved_count = 0,
	moved_stacks = 0,
}

local function drawers_step()
	if not state.enabled then return end
	if not state.controller_pos then
		l:err("need a controller added.")
		return
	end

	local my_pos = core.localplayer:get_pos()
	local source_pos = state.sources[#state.sources]

	if #state.sources == 0 then
		l:info("processing complete.")
		return
	end

	if vector.distance(my_pos, state.controller_pos) > 8
	or vector.distance(my_pos, source_pos) > 8 then
		l:info("Too far from nodes, waiting")
		core.after(15, drawers_step) -- use after as it takes forever
		return
	end
	queue:prepend(drawers_step)

	local node_inv = core.get_inventory(source_pos)
	if not node_inv then
		l:err("source does not have an inventory at " .. minetest.pos_to_string(source_pos))
		state.sources[#state.sources] = nil
		return
	end
	local pos_str = minetest.pos_to_string(source_pos)

	for list_name,list in pairs(node_inv) do
		for index = 1,#list do
			local stack = list[index]
			if stack:get_count() > 0
			  and stack:get_name() ~= "default:ladder_wood"
			  and stack:get_name() ~= "biofuel:fuel_can"
			  and stack:get_name() ~= "moreblocks:slab_super_glow_glass_1" then
				queue:prepend(drawers_step)

				l:debug("Attempting to move " .. stack:get_count() .. "*" .. stack:get_name()
				        .. " at " .. pos_str .. "[" .. index .. "] ")
				state.moved_stacks = state.moved_stacks + 1
				state.moved_count  = state.moved_count + stack:get_count()
				local from = { inv_loc = source_pos,           list_name = list_name, list_index = index }
				local to   = { inv_loc = state.controller_pos, list_name = "src",     list_index = 1 }
				-- lastly, ensure that controller is empty (has consumed the item)
				queue:prepend(function() return library.queued_ensure_stack(from, "", 0, 5) end)
				-- then move
				queue:prepend(function() return library.queued_move_stack(from, to, stack:get_count()) end)
				-- first ensure that destination is really empty
				queue:prepend(function() return library.queued_ensure_stack(to, "", 0, 5) end)

				return
			end
		end
	end

	l:info("node " .. pos_str .. " processing complete, moved "
	       .. state.moved_count .. " items in " .. state.moved_stacks .. " stacks")
	state.sources[#state.sources] = nil
	state.moved_stacks = 0
	state.moved_count = 0
end



local wait_for_punch = false
library.register_on_punchnode_once_per_pos(function(pos, node)
	if wait_for_punch then
		if node.name == "drawers:controller" then
			l:info("Adding controller")
			state.controller_pos = pos
		elseif is_digtron(node.name) then
			l:debug("Adding all digtron inventory nodes around this node")
			local visited = {}
			local positions = {}
			find_face_connected_nodes(pos, is_digtron, visited, positions)
			for _,p in pairs(positions) do
				table.insert(state.sources, p.pos)
			end
		else
			table.insert(state.sources, pos)
			l:info("Added source inventory")
		end
		return true
	end
	return false
end)

local function help()
	return ".drawer add                 -- toggles adding nodes/controller\n"
	    .. ".drawer add-pos-bb          -- add all positions inside a bounding box defined by .pos\n"
	    .. ".drawer start               -- starts moving stuff from nodes to drawer controller\n"
	    .. ".drawer stop                -- stops moving stuff"
end

core.register_chatcommand("drawer", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "add" then
			if wait_for_punch then
				wait_for_punch = false
			else
				l:info("punch controller (and then nodes to process them). Run 'add' again to end this process.")
				wait_for_punch = true
			end
		elseif args[1] == "add-pos-bb" then
			positions:iterate_on_bounding_box(function(pos)
				table.insert(state.sources, pos)
			end)
		elseif args[1] == "start" then
			state.enabled = true
			queue:prepend(drawers_step)
			queue:walk()
		elseif args[1] == "stop" then
			state.enabled = false
		else
			l:info(help())
		end
	end,
})
