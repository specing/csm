--[[
Minetest+specing Client-Side Mod (CSM) for refilling spacesuit: armor with air
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--]]
local l = library.Logger:new("spacesuit_refill")

local space_items = {}
space_items["spacesuit:chestplate"] = 1
space_items["spacesuit:pants"] = 1
space_items["spacesuit:helmet"] = 1
space_items["spacesuit:boots"] = 1


local function spacesuit_repair_step()
	local armor_inv_loc = core.localplayer:get_name() .. "_armor"
	local inv = core.get_inventory()
	local armor_inv  = core.get_inventory(armor_inv_loc)
	if armor_inv then
		armor_inv = armor_inv["armor"]
	end

	if not armor_inv then
--		l:err("Unable to access the armor inventory")
		return
	end

	-- First step: check if we are wearing a full spacesuit
	local spacesuit_part_count = 0
	for i = 1,#armor_inv do
		local item_stack = armor_inv[i]
		if space_items[item_stack:get_name()] then
			spacesuit_part_count = spacesuit_part_count + 1
		end
	end

	if spacesuit_part_count == 0 then
		return
	end

	local bottles_at = library.find_item(inv, "vacuum:air_bottle")
	if not bottles_at then
		l:err("There are no air bottles in personal inventory. Space suit is not repairable!")
		-- TODO teleport?
		--l:err("Failed to refill space suit, /spawn-ing away before you suffocate")
		--minetest.send_chat_message("/spawn")
		--queue:pause()
		return
	end

	local bottle_count = inv[bottles_at.list_name][bottles_at.index]:get_count()

	-- If we are wearing parts of the suit, warn way before it becomes a problem
	if bottle_count > 0 and bottle_count < 10 then
		l:warn("Count of air bottles in personal inventory is low. Can only repair/refill "
		       .. bottle_count .. " spacesuit_parts")
	end

	-- Do we have anything to repair?
	for i = 1,#armor_inv do
		local item_stack = armor_inv[i]
		-- 64k is aprox 2% left
		if bottle_count > 0 and item_stack:get_wear() > 59000 and space_items[item_stack:get_name()] then
			-- we have at least one air bottle and at least one part of a space suit to repair
			local armor_invloc = { inv_loc = armor_inv_loc, list_name = "armor",              list_index = i }
			local bottle_invloc= { inv_loc = nil,           list_name = bottles_at.list_name, list_index = bottles_at.index }
			bottle_count = bottle_count - 1

			autocrafter_setup_crafting(item_stack:get_name(), 1, armor_invloc, {
				armor_invloc, bottle_invloc, nil,
				nil, nil, nil,
				nil, nil, nil })

			queue:prepend(function() l:info("Repairing " .. item_stack:get_name()) end)
		end
	end

end


local function spacesuit_repair()
	queue:prepend(spacesuit_repair_step)
	queue:walk()
	return true
end

core.register_chatcommand("spacesuit_repair", {
	func = spacesuit_repair,
})

local function spacesuit_repair_periodic()
	spacesuit_repair()
	core.after(60, spacesuit_repair_periodic)
end
core.after(60, spacesuit_repair_periodic)




-- Hide the 3darmor Spacesuit warning chat spam
--local match_pattern = "\\27%(T@3d_armor%)Your \\27FSpacesuit %a+\\27E is almost broken!"
local match_pattern = ".*Your.*Spacesuit %a+.*is almost broken!"
core.register_on_receiving_chat_message(function(message)
--[[
	local newstr = "_"
	for i = 1,#message do
		newstr = newstr .. message[i] .. "_"
	end
	l:debug(newstr)
--]]
	local captures = string.match(message, match_pattern)
	if captures then
		return true
	else
		return false
	end
end)
