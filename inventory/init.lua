--[[
Common nodes auto-compressing CSM
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)

dofile(csm_path .. "refill.lua")
dofile(csm_path .. "circular_saw.lua")
dofile(csm_path .. "spacesuit_refill.lua")
dofile(csm_path .. "chests_and_bones.lua")
dofile(csm_path .. "inv_to_drawers.lua")
dofile(csm_path .. "sulfur_coal_compactor.lua")

local move_stack = function(from, from_i, to, to_i, count)
	--print("moving " .. tostring(count) .. " from " .. from .. tostring(from_i) .. " to " .. to .. tostring(to_i))
	core.move_stack(nil, from, from_i, nil, to, to_i, count)
end

local function do_autocrafting(input_slots, input_total, input_max, result_slots)
	if #result_slots == 0 then return false end
	local empty_i = result_slots[1].index

	local comp_count = math.min(input_max, math.min(result_slots[1].remaining, math.floor(input_total / 9)))
	if comp_count == 0 then return false end
	print ("Total input to auto-crafting: " .. input_total)

	-- lets craft!
	for craft_i = 1,9 do
		local craft_missing = comp_count
		-- find some cobble
		for k,v in pairs(input_slots) do
			if v.count >= craft_missing then
				move_stack(v.list_name, v.list_index, "craft", craft_i, craft_missing)
				input_slots[k].count = v.count - craft_missing
				break
			else -- less than craft_missing, we need to go to another slot
				craft_missing = craft_missing - v.count
				move_stack(v.list_name, v.list_index, "craft", craft_i, v.count)
				input_slots[k] = nil
			end
		end
	end

	core.craft_stack(comp_count)
	move_stack("craftresult", 1, "bag2contents", empty_i, comp_count)
	input_total = input_total - 9*comp_count
	table.remove(result_slots, 1)

	core.after(0.2, do_autocrafting, input_slots, input_total, input_max, result_slots)
end

-- returns number of crafted items
local function convert(inventory, what, result)
	-- Find stacks of input and turn them into result
	local input_max = ItemStack(what):get_stack_max()
	--local result_max = ItemStack(result):get_stack_max()

	-- Figure out where the stacks to be converted are
	local input_slots = {}
	local input_total = 0
	for list_name,list in pairs(inventory) do
		for i = 1, #list do
			if list[i]:get_name() == what then
				-- save count as well, for the code below. As it is not updated immidiately after move.
				table.insert(input_slots, { list_name = list_name, list_index = i, count = list[i]:get_count() })
				--input_slots[i] = main[i]:get_count()
				input_total = input_total + list[i]:get_count()
				--print ("slot " .. i .. " has input: " .. list[i]:get_name())
			end
		end
	end

	-- Figure out where to deposit crafting results
	local result_slots = library.get_result_slots(inventory, "bag2contents", result)
	if #result_slots == 0 then return false end

	core.after(0.2, do_autocrafting, input_slots, input_total, input_max, result_slots)
	return false
end
--			local move_count = math.min(input_count, comp_count)
--			move_stack("main", input_i, "craft", craft_i, move_count)
--			craft_missing = craft_missing - move_count

--			if move_count == input_count then -- emptied this stack
--				input_slots[input_i] = nil
--			end
--			if craft_missing

local auto_convert = {}
auto_convert["default:cobble"] = { count = 0, lump = "default:cobble", result = "moreblocks:cobble_compressed" }
auto_convert["default:stone"] = auto_convert["default:cobble"]
auto_convert["default:stone_with_coal"] = { count = 0, lump = "default:coal_lump", result = "default:coalblock" }
auto_convert["technic:mineral_sulfur"] = { count = 0, lump = "technic:sulfur_lump", result = "technic:sulfur_block" }

local function count_contents()
	local main = core.get_inventory()["main"]
	for _,v in pairs(auto_convert) do
		v.count = 0

		for _,stack in pairs(main) do
			if v.lump == stack:get_name() then
				v.count = v.count + stack:get_count()
			end
		end
	end
end

core.after(4, count_contents)

local function on_dignode(_, node)
	local ac = auto_convert[node.name]
	if ac then
		ac.count = ac.count + 1

		local stack = ItemStack(ac.lump)
		local stack_max = 64
		if stack then
			stack_max = stack:get_stack_max()
		end

		if ac.count >= 0.9 * stack_max then
			print("Count of " .. node.name .. " is " .. ac.count .. " >= " .. stack_max .. ", compacting...")
			ac.count = 0
			convert(core.get_inventory(), ac.lump, ac.result)
			core.after(3, count_contents)
		end
	end
end

function compact_inventory()
--	queue:prepend(function() convert(core.get_inventory(), "moreblocks:cobble_compressed", "extra:cobble_condensed"); return 0.5; end)

	queue:prepend(function() convert(core.get_inventory(), "default:cobble", "moreblocks:cobble_compressed"); return 0.5; end)
	queue:prepend(function() convert(core.get_inventory(), "default:desert_cobble", "moreblocks:desert_cobble_compressed"); return 0.5; end)
--	queue:prepend(function() convert(core.get_inventory(), "default:gravel", "gravelsieve:compressed_gravel"); return 0.5; end)
	queue:prepend(function() convert(core.get_inventory(), "default:dirt", "moreblocks:dirt_compressed"); return 0.5; end)
	queue:prepend(function() convert(core.get_inventory(), "technic:sulfur_lump", "technic:sulfur_block"); return 0.5; end)
	queue:prepend(function() convert(core.get_inventory(), "default:coal_lump", "default:coalblock"); return 0.5; end)
end


--[[
core.register_on_dignode(on_dignode)

core.register_chatcommand("convert_all", {
	func = function(param)
		return true
	end,
})
--]]
