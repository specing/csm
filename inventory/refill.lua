--[[
Inventory management CSM - refill wielded itemstack on place
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

-- Note: this function is called when player right-clicks with item in hand.
-- Meaning that stack:get_count() will return number of items before the place
-- operation is executed.
-- There is no feedback on whether or not the server actually accepted the placement.
-- As such, we have to assume that at least one item might not be placed.
-- Furthermore, something else might get placed into an empty inventory slot, meaning
-- that we should refill before that can happen. However, some items might not stack.
-- In that case, refill when the last item is moved.
local refill_at = 3 -- 2 items still remain after place
core.register_on_placenode(function()--pointed_thing, node)
	local stack = core.localplayer:get_wielded_item()
	local at = math.min(refill_at, stack:get_stack_max())

	if stack:get_count() <= at and stack:get_stack_max() > 1 then
		local my_inv = core.get_inventory()
		local wield_index = core:get_wield_index()
		local index = library.find_item_index_exclude(my_inv.main, stack:get_name(), wield_index)

		if index then
			print("[refill] Moving from main:" .. tostring(index) .. " to main:" .. tostring(wield_index))
			-- count = 0 => all
			core.move_stack(nil, "main", index, nil, "main", wield_index, 0)
		end
	end
end)
