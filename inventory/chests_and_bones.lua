--[[
Minetest+specing Client-Side Mod (CSM) for automatically retrieving chest/bone contents
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local function chest_move(pos)
	print("chest: move started")
	local nremaining = library.setup_queued_move_list(pos, "main", nil, "main")
	if nremaining > 0 then
		print(nremaining .. " items will still be left in node")
	end
	queue:walk()
end


core.register_chatcommand("chest_move", {
	func = function(param)
		local args = string.split(param, ' ')

		if #args > 1 then
			local inv_loc = library.parse_inventory_location(args, 1)
			local inv = core.get_inventory(inv_loc)
			if not inv then
				print("No inventory there!")
				return true
			end

			chest_move(inv_loc)
		else
			print("punch a chest to retrieve its contents")
			library.register_on_punchnode_once(function(pos,_)
				chest_move(pos)
			end)
		end

		return true
	end,
})
