--[[
Minetest+specing Client-Side Mod (CSM) for automatically compacting sulfur/coal from digtrons
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local l = library.Logger:new("sulfur&coal_compactor")

local state = {
	enabled = false,

	-- item to pos, or index to pos if not configured yet
	autocrafters = {},
	-- list of positions
	source_chests = {},
}

local function max_distance(point, list_of_points)
	local max_dist = 0
	for _,p in pairs(list_of_points) do
		max_dist = math.max(max_dist, vector.distance(point, p))
	end
	return max_dist
end

local function scc_move_all(what, where)
	local dest = core.get_inventory(where)
	if dest then
		dest = dest["src"]
	end
	if not dest then
		l:err("destination does not exist")
		return
	end

	local dest_i = library.find_item_index(dest, "")

	for _,pos in ipairs(state.source_chests) do
		local src = core.get_inventory(pos)
		if src then
			src = src["main"]
		end
		if not src then
			l:err("source does not exist at " .. minetest.pos_to_string(pos))
			return
		end

		for src_i = 1,#src do
			local stack = src[src_i]
			if stack:get_name() == what then
				if not dest_i then
					l:err("no empty slots left")
					return
				end
				local from = { inv_loc = pos,   list_name = "main", index = src_i }
				local to   = { inv_loc = where, list_name = "src",  index = dest_i }
				queue:prepend(function() return library.queued_move_stack(from, to, stack:get_count()) end)
				dest_i = library.find_item_index(dest, "", dest_i + 1)
			end
		end
	end
end

local function scc_step()
	if not state.enabled then return end

	if not state.sulfur_crafter_pos then
		l:err("missing sulfur autocrafter!")
	elseif not state.coal_crafter_pos then
		l:err("missing coal autocrafter!")
	elseif #state.source_chests == 0 then
		l:err("missing source chests!")
	else
		local my_pos = core.localplayer:get_pos()
		if vector.distance(my_pos, state.sulfur_crafter_pos) > 5
		or vector.distance(my_pos, state.coal_crafter_pos) > 5
		or max_distance(my_pos, state.source_chests) > 5 then
			l:info("Too far from nodes, waiting")
			core.after(15, scc_step) -- use after as it takes forever
			return
		end

		-- do some moving
		scc_move_all("default:coal_lump", state.coal_crafter_pos)
		scc_move_all("technic:sulfur_lump", state.sulfur_crafter_pos)


		core.after(30, scc_step) -- use after as it takes forever
	end
end



local wait_for_punch = false
library.register_on_punchnode_once_per_pos(function(pos, node)
	if wait_for_punch then
		if node.name == "digtron:inventory" then
			l:info("Adding source chest")
			table.insert(state.source_chests, pos)
		elseif node.name == "pipeworks:autocrafter" then
			local inv = core.get_inventory(pos)
			local item_string = inv["output"][1]:get_name()

			if item_string == "technic:sulfur_block" then
				l:info("Adding sulfur autocrafter")
				state.sulfur_crafter_pos = pos
			elseif item_string == "default:coalblock" then
				l:info("Adding coal autocrafter")
				state.coal_crafter_pos = pos
			elseif item_string == "" then
				l:err("autocrafter unconfigured, skipping")
			else
				l:info("Unknown autocrafter, skipping: " .. item_string)
			end
		else
			l:err("Unsuitable node punched: " .. node.name)
		end
	end
end)


local function help()
	return ".scc reg     -- to start registering nodes\n"
	    .. ".scc unreg   -- to stop registering nodes\n"
	    .. ".scc start   -- start transferring\n"
	    .. ".scc stop    -- stop transferring (at next scheduled run)\n"
end


core.register_chatcommand("scc", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "reg" then
			l:info("punch autocrafters/chests to add them")
			wait_for_punch = true
		elseif args[1] == "unreg" then
			wait_for_punch = false
		elseif args[1] == "start" then
			state.enabled = true
			queue:prepend(scc_step)
			queue:walk()
		elseif args[1] == "stop" then
			state.enabled = false
		else
			l:info(help())
		end
	end,
})
