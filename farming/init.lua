--[[
Minetest+specing Client-Side Mod (CSM) for automatically farming
Copyright (C) 2022 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local mod_storage = core.get_mod_storage()
local state = minetest.deserialize(mod_storage:get_string("state")) or {}
state.enable   = false --state.enable or false
state.log_level= nil -- migrated to .log
local l = library.Logger:new("farming")


-- TODO: farming is actually a special case of worldedit: remove [& place]
state.plant_data = { --state.plant_data or {
	["default:papyrus"] = { papyrus_like = 1, },
	["nodes_nature:alaf"] = {},
	["nodes_nature:anperla"] = { replace_with = "nodes_nature:anperla_seed", },
	["nodes_nature:vansano"] = { replace_with = "nodes_nature:vansano_seed", },
	["nodes_nature:cana"]   = { papyrus_like = 1, },
	["nodes_nature:chalin"] = { papyrus_like = 1, },
	["nodes_nature:gemedi"] = { papyrus_like = 1, },
	["nodes_nature:maraka_nut"] = {},
	["nodes_nature:panasee_fruit"] = {},
	["nodes_nature:tangkal_fruit"] = {},
	["ncrafting:bundle_treated_indigo"] = {
		replace_with = "ncrafting:bundle_indigo",
	},
	["tech:mashed_anperla_cooked"] = {
		replace_with = "tech:mashed_anperla",
	},
	["tech:mashed_anperla_burned"] = { -- handle oops
		replace_with = "tech:mashed_anperla",
	},
	["tech:peeled_anperla_cooked"] = {
		replace_with = "tech:peeled_anperla",
	},
	["tech:peeled_anperla_burned"] = { -- handle oops
		replace_with = "tech:peeled_anperla",
	},
	["tech:retted_cana_bundle"] = {
		can_act = function(pos)
			local pos_above = { x = pos.x, y = pos.y + 1, z = pos.z }
			local node_above = minetest.get_node_or_nil(pos_above)
			if string.find(node_above.name, "source") then
				-- digging causes water to fall and then I have to pick
				-- up entities.
				return false
			else
				return true
			end
		end,
		replace_with = "tech:unretted_cana_bundle",
	},
	["tech:slag"] = {}, -- just dig
	["tech:pane_tray_clear"] = {
		after_dig = function(pos)
			queue:prepend(function()
				-- Have to place above tray
				library.place_node("tech:clear_glass_mix", {x = pos.x, y = pos.y + 1, z = pos.z})
			end)
			queue:prepend(function()
				return library.queued_ensure_node(pos, "tech:pane_tray", 10)
			end)
		end,
	},
	["artifacts:antiquorium_chest"] = {
		harvest = function(pos)
			-- Workflow: iron compaction, soil making, loam production, pottery, stone collection/boulder compaction
			if state.mode == "i" or state.mode == "iron" then -- ironstone&boulders&cobbles
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "nodes_nature:ironstone_boulder"
				    end)
				then return true
				end
			elseif state.mode == "b" or state.mode == "boulders" then -- put boulders into inventory
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return string.find(stack:get_name(), "boulder")
				    end)
				then return true
				end
			elseif state.mode == "s" or state.mode == "soil" then -- put ash + loam into inventory
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        --leave super-stacks alone wherever they may be
				        return (stack:get_name() == "nodes_nature:loam" and stack:get_count() < 3)
				            or string.find(stack:get_name(), "tech:wood_ash") -- +_block
				    end)
				then return true
				end
			elseif state.mode == "l" or state.mode == "loam" then -- put sand+silt+clay into inv -> loam
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "nodes_nature:sand"
				            or stack:get_name() == "nodes_nature:silt"
				            or stack:get_name() == "nodes_nature:clay"
				    end)
				then return true
				end
			elseif state.mode == "J" or state.mode == "jade" then
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "nodes_nature:jade_boulder"
				            or stack:get_name() == "nodes_nature:jade_brick"
				            or stack:get_name() == "nodes_nature:jade_block"
				    end)
				then return true
				end
			elseif state.mode == "L" or state.mode == "limestone" then
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "nodes_nature:limestone_boulder"
				            or stack:get_name() == "nodes_nature:limestone_brick"
				            or stack:get_name() == "nodes_nature:limestone_block"
				    end)
				then return true
				end
			elseif state.mode == "Gn" or state.mode == "gneiss" then
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "nodes_nature:gneiss_boulder"
				            or stack:get_name() == "nodes_nature:gneiss_brick"
				            or stack:get_name() == "nodes_nature:gneiss_block"
				    end)
				then return true
				end
			elseif state.mode == "G" or state.mode == "granite" then
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "nodes_nature:granite_boulder"
				            or stack:get_name() == "nodes_nature:granite_brick"
				            or stack:get_name() == "nodes_nature:granite_block"
				    end)
				then return true
				end
			elseif state.mode == "B" or state.mode == "basalt" then
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "nodes_nature:basalt_boulder"
				            or stack:get_name() == "nodes_nature:basalt_brick"
				            or stack:get_name() == "nodes_nature:basalt_block"
				    end)
				then return true
				end
			elseif state.mode == "gravel" then
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "nodes_nature:gravel"
				    end)
				then return true
				end
			elseif state.mode == "pottery" then
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(stack)
				        return stack:get_name() == "tech:broken_pottery"
				            or stack:get_name() == "tech:clay_water_pot"
				            or stack:get_name() == "tech:clay_storage_pot"
				            or stack:get_name() == "tech:clay_oil_lamp_empty"
				    end)
				then return true
				end
			elseif state.mode == "*" then
				if library.setup_queued_move_items_step(pos, "main", nil, "main", function(_)
				        return true
				    end)
				then return true
				end
			end
		end,
	},
}

------------------------------------------------------------------------------
-- Automatic soil watering ---------------------------------------------------
------------------------------------------------------------------------------
local water_sources_handler = {
	harvest = function(pos)
		-- use pot to get freshwater
		if library.find_item_index_in_hotbar("tech:clay_watering_can") then
			library.use_item("tech:clay_watering_can", pos)
		elseif library.find_item_index_in_hotbar("tech:wooden_watering_can") then
			library.use_item("tech:wooden_watering_can", pos)
		end
	end,
}

state.plant_data["nodes_nature:freshwater_source"] = water_sources_handler
state.plant_data["tech:wooden_water_pot_freshwater"] = water_sources_handler
state.plant_data["tech:clay_water_pot_freshwater"] = water_sources_handler
-- placed can
state.plant_data["tech:clay_watering_can_freshwater"] = water_sources_handler
state.plant_data["tech:wooden_watering_can"] = water_sources_handler

local watered_soil_handler = {
	harvest = function(pos)
		local pos_above = { x = pos.x, y = pos.y + 1, z = pos.z }
		local node_above = minetest.get_node_or_nil(pos_above)

		if not node_above then
			return false
		elseif not (node_above.name == "nodes_nature:chalin"
		         or node_above.name == "nodes_nature:chalin_dead"
		         or node_above.name == "nodes_nature:cana"
		         or node_above.name == "nodes_nature:cana_dead") then
			return false
		end

		if library.find_item_index_in_hotbar("tech:clay_watering_can_freshwater") then
			library.use_item("tech:clay_watering_can_freshwater", pos)
		elseif library.find_item_index_in_hotbar("tech:wooden_watering_can_freshwater") then
			library.use_item("tech:wooden_watering_can_freshwater", pos)
		end
	end,
}

for _,sediment in ipairs({"clay","sand","silt","gravel","loam"}) do
	state.plant_data["nodes_nature:"..sediment] = watered_soil_handler
	state.plant_data["nodes_nature:"..sediment.."_agricultural_soil"] = watered_soil_handler
	state.plant_data["nodes_nature:"..sediment.."_fertile_soil_agricultural_soil"] = watered_soil_handler
end

------------------------------------------------------------------------------
-- Automatic soil re-fertilisation -------------------------------------------
------------------------------------------------------------------------------
-- TODO: gets to use_item. It's actually used properly, but nothing happens:
-- 6262)lib/nodes: Using stairs:slab_compost at (-3,94,-55)@#ffffff)
-- Still, it works as punching node above is now handled by server.
local depleted_soil_handler = {
	harvest = function(pos)
		local pos_above = { x = pos.x, y = pos.y + 1, z = pos.z }
		local node_above = minetest.get_node_or_nil(pos_above)

		if not node_above then
			return false
		--elseif not (node_above.name == "nodes_nature:chalin" TODO: test if plant and nor buried
		end

		-- use slabs first
		if library.find_item_index_in_hotbar("stairs:slab_compost") then
			library.use_item("stairs:slab_compost", pos)
			return true
		elseif library.find_item_index_in_hotbar("nodes_nature:compost") then
			library.use_item("nodes_nature:compost", pos)
			return true
		else
			-- TODO: will be spammy without last-failed-memory
			l:debug("Depleted soil in range, but no compost available.")
			return false
		end
	end,
}

for _,sediment in ipairs({"clay","sand","silt","gravel","loam"}) do
	state.plant_data["nodes_nature:"..sediment.."_agricultural_soil"] = depleted_soil_handler
	state.plant_data["nodes_nature:"..sediment.."_agricultural_soil_depleted"] = depleted_soil_handler
	--TODO: this overrides the one from watering above
end

------------------------------------------------------------------------------
-- Automatic fire maintenance ------------------------------------------------
------------------------------------------------------------------------------

-- small wood fires: replace only if pot is cooking above them
state.plant_data["tech:wood_ash"] = {
	can_act = function(pos)
		local meta = core.get_meta(pos)
		if not meta then
			l:err("unable to obtain cooking pot metadata")
			return false
		end

		if "Soup" ~= meta:get_string("type") then
			return false
		end

		if not library.find_item_index_in_hotbar("inferno:fire_sticks") then
			return false
		end
		return true
	end,
	replace_with = "tech:small_wood_fire_unlit",
	after_replace = function(pos)
		queue:prepend(function()
			library.use_item("inferno:fire_sticks", pos)
			return 0
		end)
	end,
}

-- smelter. replace only if something is being smelted
state.plant_data["tech:wood_ash_block"] = {
	can_act = function(pos)
		local check = function(check_pos)
			local meta = core.get_meta(check_pos)
			return meta and ((tonumber(meta:get_string("temp")) or 0) > 200)
		end

		if not library.find_item_index_in_hotbar("inferno:fire_sticks") then
			return false
		end

		return check({ x = pos.x - 1, y = pos.y - 1, z = pos.z - 1})
		    or check({ x = pos.x - 1, y = pos.y - 1, z = pos.z})
		    or check({ x = pos.x - 1, y = pos.y - 1, z = pos.z + 1})
		    or check({ x = pos.x,     y = pos.y - 1, z = pos.z - 1})
		    or check({ x = pos.x,     y = pos.y - 1, z = pos.z + 1})
		    or check({ x = pos.x + 1, y = pos.y - 1, z = pos.z - 1})
		    or check({ x = pos.x + 1, y = pos.y - 1, z = pos.z})
		    or check({ x = pos.x + 1, y = pos.y - 1, z = pos.z + 1})
	end,
	replace_with = "tech:charcoal_block",
	after_replace = function(pos)
		queue:prepend(function()
			library.use_item("inferno:fire_sticks", pos)
			return 0
		end)
	end,
}

------------------------------------------------------------------------------
-- Automatic soupmaking / cooking --------------------------------------------
------------------------------------------------------------------------------
-- I could code pots to use the global work-queue, but let's try this
-- node state cache approach as well. Technically it's better as
-- it does not block up the workqueue until soup is finished. But that's
-- a deficiency of the workqueue implementation.
-- Either is required because otherwise this code would apply same step
-- to the same pot multiple times, resulting in increased water consumption
-- and +10 thirst, +0 hunger soup.
local pot_cache = {}

local function cooking_pot_fsm(pos)
	local my_inv = core.get_inventory()
	local my_main_inv = my_inv["main"]
	local meta = core.get_meta(pos)
	if not meta then
		l:err("unable to obtain cooking pot metadata")
		return false
	end

	local hashed_pos = minetest.hash_node_position(pos)
	local cached = pot_cache[hashed_pos]
	if not cached then
		pot_cache[hashed_pos] = {time = library.time, step = 0}
		cached = pot_cache[hashed_pos]
	end
	if cached.time + 5 < library.time then
		cached.step = nil
	end
	cached.time = library.time -- update
	cached.step = cached.step or 0

	-- starts with status:""
	-- when water is put inside, status remains "", type is set to "Soup"
	-- when cooking, type is Soup, status is "cooking"
	-- when finished, type is Soup, status is "finished"?
	local pot_type = meta:get_string("type")
	local pot_status = meta:get_string("status")


	if pot_type == "" and cached.step ~= 1 then -- unprepared, punch with water bucket
		for _,water_source in pairs({"tech:wooden_water_pot_freshwater",
		                             "tech:clay_water_pot_freshwater",
		                             "tech:glass_bottle_green_freshwater"}) do
			if library.find_item_index(my_main_inv, water_source) then
				if library.use_item(water_source, pos) then
					cached.step = 1
					return true
				end
			end
		end

		return false

	elseif pot_type == "Soup" and pot_status == "" and cached.step ~= 2 then

		local requirements = {
			-- 20*12(momo quality) is the highest that divides by 10 (soup
			-- count)  without remainder.
			{ what = "nodes_nature:momo_fruit", count = 20 },
--			{ what = "tech:peeled_anperla", count = 36 },
--			{ what = "tech:peeled_anperla_cooked", count = 48 },
--			{ what = "tech:mashed_anperla", count = 6  },
--			{ what = "tech:mashed_anperla_cooked", count = 8 },
--			{ what = "nodes_nature:maraka_nut", count = 8 }, -- 8 of 24, because I don't have enough :P
		}

		local moves = library.can_multimove(nil, "main", pos, "main", requirements)
		if not moves then return false end

		-- Cooking magic happens on formspec input. But, there is
		-- no input to be done, so just quit it.
		queue:prepend(function() core.send_fields(pos, "", { quit = "true" } ) end)
		queue:prepend(function() return 1 end)

		for _,move in pairs(moves) do
			-- this is an immediate action
			library.queued_move_stack(move.src, move.dst, move.count)
		end
		cached.step = 2

	elseif pot_status == "finished" and cached.step ~= 3 then -- take the soup out

		local moves = library.can_multimove(pos, "main", nil, "main",
		                                    {{ what = "tech:soup", count = 10 }})
		if not moves then return false end

		-- Cooking magic happens on formspec input. But, there is
		-- no input to be done, so just quit it.
		queue:prepend(function() core.send_fields(pos, "", { quit = "true" } ) end)
		queue:prepend(function() return 1 end)

		for _,move in pairs(moves) do
			-- this is an immediate action
			library.queued_move_stack(move.src, move.dst, move.count)
		end

		cached.step = 3
	else
		return false
	end
	return true
end

state.plant_data["tech:cooking_pot"] = { harvest = cooking_pot_fsm }
------------------------------------------------------------------------------
-- Automatic cooking ---------------------------------------------------------
------------------------------------------------------------------------------
-- temp >= 1 is charcoal, temp >= 2 is no air around it
local cooking_data = {
	["tech:roasted_iron_ore"] = {
		replace_with = "tech:crushed_iron_ore",
	},
	["tech:loose_brick"] = {
		replace_with = "tech:loose_brick_unfired",
	},
	["tech:iron_bloom"] = {
		replace_with = "tech:iron_smelting_mix",
		temp = 1,
		need_hole = 1,
	},
	["tech:green_glass_ingot"] = {
		replace_with = "tech:green_glass_mix",
		temp = 2,
	},
	["tech:quicklime"] = {
		replace_with = "tech:crushed_lime",
	},
}

-- for each pos, return string of what to replace the node with
local function cooking_handler(pos)
	--local node = minetest.get_node_or_nil(pos)
	--if not node then return end

	local my_inv = core.get_inventory()
	local my_main_inv = my_inv["main"]

	-- Don't bother with intelligent picking for now, just check any-of
	for _,data in pairs(cooking_data) do
		if library.find_item_index(my_main_inv, data.replace_with) then
			return data.replace_with
		end
	end

	return nil
end

for what,_ in pairs(cooking_data) do
	state.plant_data[what] = { replace_with = cooking_handler }
end
------------------------------------------------------------------------------
-- Main code -----------------------------------------------------------------
------------------------------------------------------------------------------

local plant_list = {} -- for find_nodes_in_area
for plant,_ in pairs(state.plant_data) do
	table.insert(plant_list, plant)
end


-- return height, lowest node pos, highest node pos
local function measure_height(pos, name, max_dist)
	local height = 1 -- current node

	local lowest_pos = pos
	local highest_pos = pos
	-- upwards
	for off_y = 1,max_dist do
		local check_pos = {x = pos.x, y = pos.y + off_y, z = pos.z}
		local check_node = minetest.get_node_or_nil(check_pos)

		if not check_node then
			l:err("get_node_or_nil (above) returned nil!")
			return
		end

		if check_node.name == name then
			height = height + 1
			highest_pos = check_pos
		else
			break
		end
	end
	-- downwards
	for off_y = 1,max_dist do
		local check_pos = {x = pos.x, y = pos.y - off_y, z = pos.z}
		local check_node = minetest.get_node_or_nil(check_pos)

		if not check_node then
			l:err("get_node_or_nil (above) returned nil!")
			return
		end

		if check_node.name == name then
			height = height + 1
			lowest_pos = check_pos
		else
			break
		end
	end

	return height, lowest_pos, highest_pos
end

local function process_pos(pos, node, data)
	-- How many can still fit in inventory?
	local my_main_inv = core.get_inventory(nil)["main"]
	local space_left = library.get_remaining_space(my_main_inv, node.name)
	if space_left <= 0 then
		return nil
	end

	local harvest_pos = pos

	if data.papyrus_like then
		-- we get all papyrus in range, so must find ground/top nodes and height
		local height, _, highest_pos = measure_height(pos, node.name, 10)
		--l:debug("height is " .. tostring(height))

		if height < 3 then -- leave it growing still
			return nil
		end

		local harvest_count = math.min(space_left, height - 1)

		l:debug("Harvesting: " .. node.name .. " space left: " .. tostring(space_left) .. ", height: "
		  .. tostring(height) .. ", harvest count: " .. tostring(harvest_count))
		if 0 == harvest_count then
			return nil
		end

		harvest_pos = {x = highest_pos.x, y = highest_pos.y - harvest_count + 1, z = highest_pos.z}
	end

	return harvest_pos
end

-- TODO: check second return value of find_nodes_in_area to bail early if no papyrus present
local function processing_loop()
	if not state.enable then return end

	l:debug("farming loop:")

	local my_pos = minetest.localplayer:get_pos()
	-- TODO: get head node position from engine. Requires engine changes I think.
	my_pos.y = my_pos.y + 1 -- harvesting with arms, not legs
	local max_distance = library.get_item_range()
	local pos_m = vector.subtract(my_pos, max_distance)
	local pos_M = vector.add(my_pos, max_distance)

	local plant_data = state.plant_data

	local plant_positions = minetest.find_nodes_in_area(pos_m, pos_M, plant_list)
	for _,pos in pairs(plant_positions) do
	-- find_node_near seems to prefer closer nodes and returns only one node,
	-- which is bad if the node was already harvested and there's nothing left to do.
	--local pos = minetest.find_node_near(my_pos, max_distance, plant_list, true)
	--if pos then
		local node = minetest.get_node_or_nil(pos)
		if not node then
			l:err("get_node_or_nil returned nil!")
			return
		end

		local data = plant_data[node.name]
		if data then
			if data.harvest then
				local success = (vector.distance(my_pos, pos) < max_distance) and data.harvest(pos)
				if success then
					queue:append(function() return 0.1 end) -- small delay for bugz
					queue:append(processing_loop)
					queue:walk()
					return
				end
			else
				local harvest_pos = process_pos(pos, node, data)
				local found = true
				local replace_with = data.replace_with
				if replace_with then
					if type(replace_with) == "function" then
						replace_with = replace_with(pos)
						found = replace_with -- whatever or nil
					else
						local my_main_inv = core.get_inventory()["main"]
						found = library.find_item_index(my_main_inv, replace_with)
					end
				end

				local can_act = not data.can_act or data.can_act(pos)

				if found and can_act and harvest_pos and vector.distance(my_pos, harvest_pos) < max_distance then

					queue:prepend(processing_loop)
					queue:prepend(function() return 0.1 end) -- small delay for bugz
					if data.after_replace then
						queue:prepend(function()
							data.after_replace(harvest_pos)
						end)
					end
					if replace_with then
						queue:prepend(function()
							return library.queued_ensure_node(pos, replace_with, 10)
						end)

						queue:prepend(function()
							library.place_node(replace_with, pos)
						end)
					end
					if data.after_dig then
						queue:prepend(function()
							data.after_dig(harvest_pos)
						end)
					end
					queue:prepend(function() return library.queued_ensure_node_removed(harvest_pos, node.name, 10) end)
					if not setup_queued_digging(harvest_pos) then
						queue:clear()
						return
					end
					queue:walk()
					return
				end
			end
		end
	end

	core.after(0.5, processing_loop) -- use after to not block other tasks
end



local function help()
	return ".farming: automatic farming, usage:\n"
	    .. ".farming on|off|start|stop -- :)\n"
	    .. ".farming mode i|iron|l|loam|b|boulders|s|soil|pottery|*  warehouse looting assistance\n"
	    .. ".farming mode Gn|gneiss|G|granite|L|limestone|J|jade|B|basalt  warehouse looting assistance,\n"
end


core.register_chatcommand("farming", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "on" or args[1] == "start" then
			state.enable = true
			processing_loop()
		elseif args[1] == "off" or args[1] == "stop" then
			state.enable = false
		elseif args[1] == "mode" then
			state.mode = args[2]
		else
			l:info(help())
		end
	end,
})
