--[[
Automatic eating Client-Side Mod (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local l = library.Logger:new("auto_eat")

-- Assumptions: 0 <= stamina <= 20  and 0 <= hp <= 20
local modstorage = core.get_mod_storage()

local foods = modstorage:get_string("foods")
if foods == "" then
	l:info("no foods defined!")
	foods = {}
else
	foods = minetest.deserialize(foods)

	-- migrate to new foods structure
	local foods2 = {}
	for name,v in pairs(foods) do
		if type(v) == "number" then
			foods2[name] = { stamina = 0 }
		else
			foods2[name] = v
		end
	end
	foods = foods2
end

local is_cooled_down = true
local eat_enabled = true
local stamina_thresh = 4*2
local last_food = nil
local last_stamina = nil
local hp_thresh = 17
local stamina_updates = {}

local function help()
	return "Invalid use of .auto_eat, help:\n"
	    .. ".auto_eat on|off\n"
	    .. ".auto_eat thresh stamina <number> -- Will eat when stamina falls below <number>\n"
	    .. ".auto_eat thresh                 -- Reset to default threshold\n"
	    .. ".auto_eat thresh hp <number> -- Will eat if hp < <number>\n"
	    .. ".auto_eat thresh hp          -- Reset to default threshold\n"
	    .. ".auto_eat register <string> -- Registers new food item <string>\n"
	    .. ".auto_eat register <number> -- Registers new food item at main[<number>]\n"
	    .. ".auto_eat register          -- Registers wielded item as a food\n"
	    .. ".auto_eat list              -- list registered food items\n"
	    .. ".auto_eat unreg <name>      -- unregister food <name>\n"
	    .. ".auto_eat unreg <index>     -- unregister food at <index> in list"
end

core.register_chatcommand("auto_eat", {
	func = function(param)
		-- if param is nil, use wielded item. Else add food as named
		local args = string.split(param, ' ')

		if args[1] == "thresh" then
			if args[2] == "stamina" then
				stamina_thresh = tonumber(args[3]) or 4*2
				l:info("Will auto-eat when stamina falls below " .. stamina_thresh)
			elseif args[2] == "hp" then
				hp_thresh = tonumber(args[3]) or 17
				l:info("Will attempt to eat for hp as soon as hp falls below " .. hp_thresh)
			else
				l:info(help())
			end
		elseif args[1] == "register" then
			local name = nil
			local index = nil
			if type(args[2]) == "string" then
				name = args[2]
			elseif type(args[2]) == "number" then
				index = tonumber(args[2])
			else
				index = core.get_wield_index()
			end

			if index then
				local inv = core.get_inventory()
				local main = inv["main"]
				if 1 <= index and index <= #main then
					name = main[index]:get_name()
				else
					l:err("index given, but is not in range 1 .. " .. #main)
					l:info(help())
				end
			end

			if name and name ~= "" then
				foods[name] = { stamina = 0 }
				modstorage:set_string("foods", minetest.serialize(foods))
			end
		elseif args[1] == "list" then
			local i = 1
			for name,v in pairs(foods) do
				l:info("  " .. i .. ": '" .. name .. "': stamina boost(" .. v.stamina .. ")")
				i = i + 1
			end
		elseif args[1] == "unreg" and type(args[2]) == "string" then
			local name = args[2]
			local n = tonumber(name)
			if n then
				for key,_ in pairs(foods) do
					if n == 1 then
						name = key
					end
					n = n - 1
				end
			end
			if foods[name] then
				l:info("Removing '" .. name .. "' from food table.")
				foods[name] = nil
				modstorage:set_string("foods", minetest.serialize(foods))
			else
				l:err("Food '" .. name .. "' does not exist.")
			end
		elseif args[1] == "on" then
			eat_enabled = true
		elseif args[1] == "off" then
			eat_enabled = false
		else
			l:info(help())
		end

		return true
	end,
})


core.register_on_stamina_modification(function(new_stamina)
	--l:debug("Stamina modified " .. tostring(new_stamina))

	-- Stamina gets updated to a large value if eating is before death (and respawn),
	-- so we require getting the same stamina boost twice before updating the
	-- "nutritional" value.
	if last_stamina and last_food then
		local diff = new_stamina - last_stamina
		last_stamina = nil

		-- Maybe update food value?
		if diff == stamina_updates[last_food] then
			local old = foods[last_food].stamina
			if diff > old then
				l:info("Updating nutritional value for food " .. last_food .. " to +" .. diff .. " stamina")
				foods[last_food].stamina = diff
				modstorage:set_string("foods", minetest.serialize(foods))
			end
		end

		stamina_updates[last_food] = diff
	end

	if not eat_enabled or not is_cooled_down then
		return false
	end

	-- eat to regain HP
	local hp = core.localplayer:get_hp()
	--l:debug("HP is " .. hp)

	if new_stamina < stamina_thresh or hp < hp_thresh then
		local old_i = core.get_wield_index()

		-- Some actions make stamina drop really fast.
		-- Add a cool down to prevent over-eating
		core.after(1, function()
			is_cooled_down = true
		end)

		local inv = core.get_inventory()
		local main = inv["main"]
		for i = 1,#main do
			local food = foods[main[i]:get_name()]
			if food and (new_stamina + food.stamina) <= 19 then -- last point is lost immidiately
				l:debug("Eating")
				core.set_wield_index(i)
				core.use_item()
				core.set_wield_index(old_i)
				is_cooled_down = false

				last_stamina = new_stamina
				last_food = main[i]:get_name()

				return false
			end
		end

		-- this is in case where stamina is high but hp is still below threshold
		if new_stamina < stamina_thresh then
			l:err("no food found in main inventory!")
		end
	end

	return false
end)


--[[
core.register_on_hp_modification(function(hp)
	l:debug("New hp: " .. hp)
end)
--]]
