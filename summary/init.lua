--[[
Minetest+specing chest contents summary Client-Side Mod (CSM)
Copyright (C) 2024-2025 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Quickly get a summary of amount of items in a chest without opening it
--]]
local l = library.Logger:new("summary")

local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)


local state = {}
state.enabled = true
state.distance = 50
-- Format: map of node_pattern -> set of list_pattern
-- It is an error for both to be missing (check when adding)
state.exclude_patterns = state.exclude_patterns or {}

-- Concept: node name -> list name AND patterns map.
--   The list name matches form the set part
--   The patterns form the sequential (array) part.
-- ipairs seems to only return the latter, so we can use that to
-- test patterns if primary O(M*log N) search fails.
-- (M = length of string, N = number of lists for this node)
local exclude_cache = {}



local function add_exclude(node_pattern, list_pattern)
	state.exclude_patterns[node_pattern] = state.exclude_patterns[node_pattern] or {}
	state.exclude_patterns[node_pattern][list_pattern] = true
	exclude_cache = {} -- flush
end

local function remove_exclude(node_pattern, list_pattern)
	local ep = state.exclude_patterns[node_pattern] or {}
	ep[list_pattern] = nil
	if is_table_empty(ep) then
		state.exclude_patterns[node_pattern] = nil
	end
	exclude_cache = {} -- flush
end

local function print_excludes()
	for node_pattern,list_patterns in pairs(state.exclude_patterns) do
		local str = ""
		for list_pattern,_ in pairs(list_patterns) do
			str = str .. list_pattern .. ", "
		end
		l:info("All nodes matching " .. node_pattern ..
		       " have lists matching the following patterns excluded: " .. str)
	end
end


-- Decent defaults to start with
local function add_default_excludes()
	add_exclude("technic:[lmh]v_", "dst_tmp")
	add_exclude("technic:[lmh]v_", "upgrade1")
	add_exclude("technic:[lmh]v_", "upgrade2")
	add_exclude("pipeworks:autocrafter", "recipe")
end
add_default_excludes()


local function change_summary(new_text)
	local lplayer = core.localplayer
	if not lplayer then return end

	if not state.hud_id then
		state.hud_id = lplayer:hud_add({
			hud_elem_type = "text",
			position  = { x = 0.45, y = 0.51 },
			alignment = { x =  1,   y = 1 }, -- left and top justified.
			direction = 3, -- bottom-top
			number = 0xffFFff, -- white
			style = 4, -- monospace
		})

		if not state.hud_id then
			l:err("WTF? Unable to create summary HUD?!")
			return
		end
	end

	lplayer:hud_change(state.hud_id, "text", new_text)
end

local function remove_hud()
	if state.hud_id then
		local lplayer = core.localplayer
		if not lplayer then return end

		l:debug("Removing HUD")
		lplayer:hud_remove(state.hud_id)
		state.hud_id = nil
	end
end




local function get_list_exclude_patterns(node_name)
	local lpatterns = exclude_cache[node_name]
	if not lpatterns then
		lpatterns = {}
		l:debug("Computing cache for node " .. node_name)

		for node_pattern,list_patterns in pairs(state.exclude_patterns) do
			if string.find(node_name, node_pattern) then
				l:debug("node " .. node_name .. " matched exclude " .. node_pattern)
				for p,_ in pairs(list_patterns) do
					table.insert(lpatterns, p)
				end
			end
		end

		exclude_cache[node_name] = lpatterns
	end

	return lpatterns
end

local function is_list_excluded(list_exclude_patterns, list_name)
	local is_excluded = list_exclude_patterns[list_name]
	if is_excluded == nil then
		l:debug("Computing cache for list " .. list_name)
		-- if not is_excluded then  -- false (cached negative) would cause
		-- this to be recomputed every time? Did not bother checking.
		for _,list_pattern in ipairs(list_exclude_patterns) do
			if string.find(list_name, list_pattern) then
				l:debug("list " .. list_name .. " matched exclude " .. list_pattern)
				list_exclude_patterns[list_name] = true
				return true
			end
		end
	end
	list_exclude_patterns[list_name] = false
	return false
end


local function summarise_inventory(pos)
	local node = minetest.get_node_or_nil(pos)
	local node_name = (node and node.name) or ""
	l:debug("Summarising " .. node_name)

	local item_to_total = {}
	local inv = core.get_inventory(pos)
	if inv then
		local lpatterns = get_list_exclude_patterns(node_name)
		-- lpatterns now contains all list patterns applicable to this node

		for list_name,list in pairs(inv) do
			if not is_list_excluded(lpatterns, list_name) then
				for _,stack in pairs(list) do
					local stack_name = stack:get_name()
					local stack_count= stack:get_count()
					item_to_total[stack_name] = (item_to_total[stack_name] or 0) + stack_count
				end
			end
		end
	else
		l:debug ("No inventory under cursor")
	end
	item_to_total[""] = nil -- clear empty stacks
	return item_to_total
end


-- first by names ascending then by amount desc
local function sort_name_then_amount_sub(lhs, rhs)
	local lname = lhs.name
	local rname = rhs.name
	if lname < rname then
		return true
	elseif rname < lname then
		return false
	else
		return lhs.total > rhs.total
	end
end

-- first by amount desc then by names ascending
local function sort_amount_then_name_sub(lhs, rhs)
	local ltotal = lhs.total
	local rtotal = rhs.total
	if ltotal > rtotal then
		return true
	elseif rtotal > ltotal then
		return false
	else
		return lhs.name < rhs.name
	end
end

-- Sort inventory and build a display string
local function summarise(pos)
	local item_to_count = summarise_inventory(pos)
	local sorted_totals = {}

	for name,total in pairs(item_to_count) do
		local sdesc = string.lower(ItemStack(name):get_short_description())
		table.insert(sorted_totals, { name = sdesc, total = total })
	end

	local lplayer = core.localplayer
	local mods = (lplayer and lplayer:get_control()) or {}
	if mods.sneak then
		table.sort(sorted_totals, sort_name_then_amount_sub)
	else
		table.sort(sorted_totals, sort_amount_then_name_sub)
	end

	local summary = ""
	for index in ipairs(sorted_totals) do
		--local stack_max = ItemStack(name)
		--stack_max = (stack_max and stack_max:get_stack_max()) or "?"

		local total = sorted_totals[index].total
		local name  = sorted_totals[index].name

		summary = string.format("%s%4d %s\n", summary, total, name)
	end
	change_summary(summary)
end


-- pos may have an inventory or not..
local old_end_pos = vector.new(1e6, 1e6, 1e6)
core.register_globalstep(function(dtime)
	if not state.enabled then
		remove_hud()
		return
	end

	local camera = core.camera
	if not camera then
		l:debug("No camera?")
		return
	end

	if camera:get_camera_mode() ~= 0 then
		--l:debug("camera not in first person view")
		return
	end

	local lplayer = core.localplayer
	if not lplayer then return end

	local start_pos = lplayer:get_pos()
	start_pos.y = start_pos.y + 1.5 -- magic value used in various throwing mods
	--start_pos = vector.add(start_pos, camera:get_offset())
	local end_pos = vector.add(start_pos,
	                           vector.multiply(camera:get_look_dir(),
	                                           state.distance))

	-- don't re-summarise same chest.
	if vector.distance(old_end_pos, end_pos) < 0.01 then
		return
	end
	old_end_pos = { x = end_pos.x, y = end_pos.y, z = end_pos.z }


	l:debug("raycasting from " .. library.p2s(start_pos)
	        .. " to " .. library.p2s(end_pos))

	local ray = minetest.raycast(start_pos, end_pos, false, false)
	for pointed_thing in ray do
		if pointed_thing.type == "node" then
			local under_node = minetest.get_node_or_nil(pointed_thing.under)
			local under_name = (under_node and under_node.name) or ""
			l:debug("ray hit: " .. under_name)
			summarise(pointed_thing.under)
			return
		end
	end
	change_summary("") -- if no node was hit, remove text
end)


local help = library.create_help_function("summary", {
	"distance <n> -- set scanning distance",
	"excludes |list",
	"excludes add|del|remove <node pattern> <list pattern>"
	.. " -- use \"\" for any (uses string.find)",
})

library.register_state_chatcommand("summary", state, {
	func = function(_, args)
		if args[1] == "distance" and tonumber(args[2]) then
			state.distance = tonumber(args[2]) -- meh :P
		elseif args[1] == "excludes" then
			local sub_cmd = args[2] or "list"
			if sub_cmd == "add" and args[3] and args[4] then
				add_exclude(args[3], args[4])
			elseif (sub_cmd == "del" or sub_cmd == "remove") and args[3] and args[4] then
				remove_exclude(args[3], args[4])
			elseif sub_cmd == "list" then
				print_excludes()
			else
				l:info(help())
			end
		else
			l:info(help())
		end
		return true
	end,
})
