--[[
Minetest+specing Client-Side Mod (CSM) library for managing recipes
Copyright (C) 2022 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local mod_storage = core.get_mod_storage()
local state = minetest.deserialize(mod_storage:get_string("recipes")) or {}
local l = library.Logger:new("lib/recipes")

-- example recipe, conforms with what a recipe dump produces
state.recipes = state.recipes or {
	{
		["type"] = "grinding",
		["items"] = {"technic:zinc_lump"},
		["output"] = "technic:zinc_dust 2",
		["width"] = 0,
	},
}



-- Given recipe 'recipe', print it on console in human readable two-line form
function library.print_recipe(recipe, n)
	local recipe_type = recipe.type or "unknown"
	local output = recipe.output or "unknown"
	local items = recipe.items or {}
	local width = recipe.width or 3

	l:info("Recipe <" .. (tostring(n) or "") .. "> type " .. recipe_type .. " produces '" .. output .. "' and consumes:")

	local string = "  "
	for index,itemstring in ipairs(items or {}) do
		string = string .. "'" .. itemstring .. "', "
		if index % width < 1 then
			l:info(string)
			string = "  "
		end
	end
	if #string > 0 then
		l:info(string)
	end
end


-- Given nil 'recipes', list all stored recipes. Else list recipes in 'recipes'
function library.list_recipes(recipes)
	for key,value in pairs(recipes or state.recipes) do
		library.print_recipe(value, key)
	end
end


-- Given recipe's items, return itemstring-indexed itemcount summary.
-- 'recipe' can be a list of ItemStack or list of strings
function library.recipe_items_to_counts(recipe)
	local counts = {}
	for _,s in pairs(recipe) do
		local stack = s
		if type(stack) == "string" then
			stack = ItemStack(stack)
		end

		local name = stack:get_name()
		if name ~= "" then
			counts[name] = (counts[name] or 0) + stack:get_count()
		end
	end
	return counts
end



-- Given args and start index, return itemstring or nil and args index of next argument.
function library.parse_cmdline_itemstring(args, args_start_index)
	if tonumber(args[args_start_index + 1]) then
		return args[args_start_index] .. " " .. args[args_start_index + 1], args_start_index+2
	else
		return args[args_start_index], args_start_index+1 -- already nil if it does not exist at all
	end
end


-- Parse command-line recipe
-- recipe specification: <type> <output item_string> <input1 item_string>
function library.parse_cmdline_recipe(args, args_start_index)
	local recipe = {}
	if not args[args_start_index] then
		l:err("Recipe specification must start with a type (string)")
		return
	end
	recipe.type = args[args_start_index]

	local oitem,args_cont_index = library.parse_cmdline_itemstring(args, args_start_index + 1)
	if not oitem then
		l:err("Recipe specification must include output itemstring")
		return
	end
	recipe.output = oitem
	recipe.items = {}

	repeat
		local iitem,next_index = library.parse_cmdline_itemstring(args, args_cont_index)
		args_cont_index = next_index -- luacheck complains if I set this directly above
		table.insert(recipe.items, iitem)
	until not iitem

	return recipe
end



-- Given recipe type, return random recipe of that type (or nil if it does not exist)
-- do not use. This is to support proof of concept code
function library.get_random_recipe(recipe_type)
	local good = {}

	for _,recipe_def in pairs(state.recipes) do
		if recipe_def.type == recipe_type then
			table.insert(good, recipe_def)
		end
	end

	--may return nil
	if #good > 0 then
		return good[math.random(#good)]
	end
end


-- Given list of recipes as expect by code here, override local list with the recipes provided
function library.load_recipes(recipes)
	state.recipes = recipes
	mod_storage:set_string("recipes", minetest.serialize(state))
end




-- Recipe manipulation API
local function help()
	return ".recipe (library recipe manager), usage:\n"
		.. "recipe specification: <type> <output item_string> <input1 item_string> ...\n"
	    .. ".recipe add <recipe_specification> -- add new recipe\n"
	    .. ".recipe del <N|name> -- delete this recipe\n"
	    .. ".recipe list -- list recipes\n"
end


local function chatcommand(args)
	local command = args[1]
	if command == "add" then

		local recipe = library.parse_cmdline_recipe(args, 2) -- parse from args[2] onwards
		if recipe then
			table.insert(state.recipes, recipe)
			mod_storage:set_string("recipes", minetest.serialize(state))
		end

	elseif command == "del" or command == "delete" and args[2] then
		state.recipes[tonumber(args[2]) or args[2]] = nil
		mod_storage:set_string("recipes", minetest.serialize(state))
	elseif command == "list" then
		library.list_recipes(state.recipes)
	else
		l:err(help())
	end
end


core.register_chatcommand("recipe", {
	func = function(param)
		local args = string.split(param, ' ')
		return chatcommand(args)
	end,
})
