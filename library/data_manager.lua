--[[
Library for Client-Side Mods (CSM)
Copyright (C) 2020-2023 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


CSM local data manager
--]]
local l = library.Logger:new("lib/data_manager")

-- Provides:
--    a way to save/load a global setting
--    a way to save/load a server setting (depends on hostname + port)
--    a way to save/load an account setting (depends on hostname + port + player account name)
--    plus utility versions of all above to also serialise/deserialise

library.Data_Manager = {
	managers = {},
}

function library.Data_Manager:new (name, mod_storage)
	-- so multiple files in same mod can access same storage.
	-- intentional global storage (. instead of :)
	if library.Data_Manager.managers[name] then
		return library.Data_Manager.managers[name]
	end

	local o = {
		name = name,
		mod_storage = mod_storage,
		obj = nil,
		setting_types = {},
		default_values = {},
	}
	setmetatable(o, self)
	self.__index = self

	library.Data_Manager.managers[name] = o
	return o
end

-- locality can be one of:
--    "global": by default applies to all servers/accounts
--    "server": by default applies to server (identified by hostname + port)
--    "account": by default applies to account (identified by hostname + port + player account name)
function library.Data_Manager:register_setting(name, locality, default_value)
	self.setting_types[name] = locality
	self.default_values[name] = default_value
end

function library.Data_Manager:update_info()
	if not self.server_id then
		local server_info = minetest.get_server_info()
		local lplayer = minetest.localplayer

		self.server_id = server_info.address .. ":" .. server_info.port
		if lplayer then
			self.account_name = lplayer:get_name()
		else
			l:err("Unable to get local player name!")
			self.account_name = "error?"
		end
		l:info ("DM init: server id: " .. self.server_id .. ", account: " .. self.account_name)
	end
end

-- returns "DM:"..setting_name..":"..server_addr..":"..server_port..":"..account_name
-- depending on what type the setting is
-- returns "DM:"..server_addr..":"..server_port..":"..account_name..":"..setting_name would prolly be better
--
-- first try loading account setting, else server one, else global one, else return default
function library.Data_Manager:get_string(setting_name)
	self:update_info()

	local global_obj_name = "DM:" .. self.name .. ":" .. setting_name
	local server_obj_name = global_obj_name .. ":" .. self.server_id
	local account_obj_name = server_obj_name .. ":" .. self.account_name

	local str
	str = self.mod_storage:get(account_obj_name)
	if str then return str end

	str = self.mod_storage:get(server_obj_name)
	if str then return str end

	str = self.mod_storage:get(global_obj_name)
	if str then return str end

	return nil
end


function library.Data_Manager:set_string(setting_name, string)
	self:update_info()

	local global_obj_name = "DM:" .. self.name .. ":" .. setting_name
	local server_obj_name = global_obj_name .. ":" .. self.server_id
	local account_obj_name = server_obj_name .. ":" .. self.account_name

	local typ = self.setting_types[setting_name]
	local name
	if typ == "account" then
		name = account_obj_name
	elseif typ == "server" then
		name = server_obj_name
	elseif typ == "global" then
		name = global_obj_name
	else
		-- fallback to global
		name = global_obj_name
		l:err("setting type for '" .. setting_name .. "' is not valid: " .. (typ or "nil"))
	end

	return self.mod_storage:set_string(name, string)
end

--[[
-- string interface
function library.Data_Manager:get_string(setting_name)
	return self.mod_storage:get_string(self:get_setting_real_name(setting_name))
end

function library.Data_Manager:set_string(setting_name, obj)
	return self.mod_storage:set_string(self:get_setting_real_name(setting_name), obj)
end--]]

-- object interface (convenience)
function library.Data_Manager:get_obj(setting_name)
	local str = self:get_string(setting_name)
	if str then
		return minetest.deserialize(str)
	else
		--l:info("Setting " ..setting_name .. " has no value, using default")
		return self.default_values[setting_name]
	end
end

function library.Data_Manager:set_obj(setting_name, obj)
	return self:set_string(setting_name, minetest.serialize(obj))
end


-- Key order on two iterations is not guaranteed and is in fact different.
-- To reliably refer to the same thing, we have to cache the keys in a list
function library.Data_Manager:list_settings()
	self.settings_list = {}

	local settings = self.mod_storage:to_table()
	if not settings then l:info("No settings found") return end
	settings = settings.fields or {}

	for key,_ in pairs(settings) do
		table.insert(self.settings_list, key)

		l:info("setting: #" .. tostring(#(self.settings_list)) .. ": " .. key)
	end
end

function library.Data_Manager:delete_setting(index)
	-- for _,index in ipairs(arg or {}) do -- delete these settings
	if index then -- delete this setting
		local key = self.settings_list[index]
		if not key then
			l:err("setting #" .. tostring(index) .. " does not exist.")
			return
		end

		self.mod_storage:set_string(key, "")
		self.settings_list[index] = nil
		l:info("setting: #" .. tostring(index) .. ": " .. key .. " has been deleted.")
	end
end
