--[[
Library of clock functions for Client-Side Mods (CSM)
Copyright (C) 2024 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
--local csm_name = assert(core.get_current_modname())
--local csm_path = core.get_modpath(csm_name)

library = library or {}

library.time = 0.0

-- TODO: get real time from someplace else?
core.register_globalstep(function(dtime)
	library.time = library.time + dtime
end)
