--[[
Library for Client-Side Mods (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Inventory manipulation helpers
--]]
local l = library.Logger:new("lib/inventory")

-- A wrapper that always returns something sensible.
function library.get_hotbar_itemcount()
	local lplayer = minetest.localplayer
	if not lplayer then
		l:warn("Localplayer was nil, do not call this at CSM init phase, assuming 8")
		return 8
	elseif lplayer.get_hotbar_itemcount then
		return lplayer:get_hotbar_itemcount()
	else
		l:warn("your engine lacks localplayer:get_hotbar_itemcount(), assuming 8")
		return 8
	end
end


-- Find empty index in inventory list, or nil if all slots are occupied.
function library.find_empty_index(list)
	for i = 1, #list do
		if list[i]:get_name() == "" then
			return i
		end
	end
	return nil
end

-- Find index of item in inventory list, nil if none is present.
-- (optionally) start the search from start
function library.find_item_index(list, name, start)
	for i = (start or 1), #list do
		if list[i]:get_name() == name then
			return i
		end
	end
	return nil
end

function library.find_item_index_rev(list, name, start)
	for i = (start or #list), 1, -1 do
		if list[i]:get_name() == name then
			return i
		end
	end
	return nil
end

function library.find_item_index_exclude(list, name, exclude)
	for i = 1, #list do
		if i ~= exclude and list[i]:get_name() == name then
			return i
		end
	end
	return nil
end

-- main_list is optional
function library.find_item_index_in_hotbar(name, main_list)
	if not main_list then
		local inventory = core.get_inventory()
		main_list = inventory["main"]
	end

	local index = library.find_item_index(main_list, name)
	if index and index <= library.get_hotbar_itemcount() then
		return index
	else
		return nil
	end
end



function library.find_item(inv, name)
	for list_name,list in pairs(inv) do
		local index = library.find_item_index(list, name)
		if index then
			return { list_name = list_name, index = index }
		end
	end
	return nil
end



-- Can leave item_stack nil for item in hand
function library.get_item_range(item_stack)
	local lplayer = minetest.localplayer

	if not item_stack then
		item_stack = lplayer and lplayer:get_wielded_item()
	end

	-- TODO
--	local def = item_stack and item_stack:get_definition()
	-- the above does not work as :get_definition expects server environment
--	local range = def.range or 4.0
	local range = 4.0
	l:debug("Using " .. range .. " as range")
	return range
end



-- remaining space for what in list list
function library.get_remaining_space(list, what)
	local stack_max = ItemStack(what):get_stack_max()
	local space_left = 0
	for _,stack in pairs(list) do
		if "" == stack:get_name() then
			space_left = space_left + stack_max
		elseif what == stack:get_name() then
			space_left = space_left + (stack_max - stack:get_count())
		end
	end
	return space_left
end

-- Return all positions in list_name that can hold item_name (and how many)
function library.get_result_slots(inventory, list_name, item_name)
	local result_slots = {} -- list of { index, remaining }
	local result_max = ItemStack(item_name):get_stack_max()

	local list = inventory[list_name]
	if not list then
		--print("library.get_result_slots: inventory[" .. list_name .. "] is nil?");
		return {};
	end

	for i = 1, #list do
		if list[i]:get_name() == "" then
			table.insert(result_slots, { index = i, remaining = result_max })
		-- ignore full stacks
		elseif list[i]:get_name() == item_name and list[i]:get_count() < result_max then
			table.insert(result_slots, { index = i, remaining = result_max - list[i]:get_count() })
			break
		end
	end
	table.sort(result_slots, function(lhs,rhs) return lhs.remaining < rhs.remaining end)
	--if #result_slots == 0 then return nil end

	return result_slots
end

-- Is the inventory list empty?
function library.is_list_empty(list)
	for _,stack in pairs(list) do
		if stack:get_count() > 0 then
			return false
		end
	end
	return true
end

-- Looks for an inventory location description in command args, starting from start
-- returns location, next position in argument array
function library.parse_inventory_location(args, start)
	if #args >= start + 3 - 1 then
		-- tonumber returns nil when conversion failed
		local x = tonumber(args[start])
		local y = tonumber(args[start+1])
		local z = tonumber(args[start+2])
		if x and y and z then
			return { x = x, y = y, z = z }, start+3
		end
	end

	if #args >= start then
		if args[start] == "" or args[start] == '""' or args[start] == "nil" then
			return nil, start+1
		else
			return args[start], start+1
		end
	end

	return nil, start
end










-- Ensure that the item stack at <stack_loc> has at least <count> of <what>
-- If timeout is reached, clear work queue
-- else if stack is ensured, return to allow other queue items to execute
-- else prepend myself to beginning of work queue
function library.queued_ensure_stack(stack_loc, what, count, timeout)
	if timeout < 0 then
		l:err("Timeout reached while waiting for " .. count .. " of " .. what .. " at "
		  .. library.inventory_location_to_string(stack_loc.inv_loc) .. "[" .. stack_loc.list_name .. "][" .. stack_loc.list_index .. "]")
		queue:clear()
		return
	end
	local inv = core.get_inventory(stack_loc.inv_loc)
	if not inv then
		l:err("queued_ensure_stack: inventory missing!")
		queue:clear()
		return
	end
	local list = inv[stack_loc.list_name]
	if not list then
		l:err("queued_ensure_stack: list missing!")
		queue:clear()
		return
	end

	local stack = list[stack_loc.list_index]

	if stack:get_name() == what and stack:get_count() == count then
		return 0
	else
		queue:prepend(function() return library.queued_ensure_stack(stack_loc, what, count, timeout - 0.01) end)
		return 0.01
	end
end



function library.queued_move_stack(from_stack_loc, to_stack_loc, count)
	core.move_stack(from_stack_loc.inv_loc, from_stack_loc.list_name, from_stack_loc.list_index,
	                  to_stack_loc.inv_loc,   to_stack_loc.list_name,   to_stack_loc.list_index, count)
	return 0 -- no delay
end







-- Arguments:
-- - src pos, src list name
-- - dst pos, dst list name
-- - requirements table: array of { what, count }
-- Returns
-- - a list of moves to execute: { src, dst (ItemLoc) and count }
-- TODO
-- - metadata: same item name, different meta cannot stack (exact conditions?)
function library.can_multimove(src_pos, src_list_name, dst_pos, dst_list_name, requirements)
	local moves = {}

	local from_inv = core.get_inventory(src_pos)
	local src_list = (from_inv or {})[src_list_name] or {}

	local to_inv = core.get_inventory(dst_pos)
	local dst_list = (to_inv or {})[dst_list_name] or {}

	local src_cache = {} -- index -> amount taken
	local dst_cache = {} -- index -> amount deposited

	for _,req in pairs(requirements) do
		local need_count = req.count -- copy primitive
		local stack_max = ItemStack(req.what):get_stack_max()

		for src_index,src_stack in pairs(src_list) do
			if src_stack:get_name() == req.what then
				local can_take = math.min(src_stack:get_count() - (src_cache[src_index] or 0), need_count)
				if can_take > 0 then

					-- Attempt to distribute this stack across destinations
					for dst_index,dst_stack in pairs(dst_list) do
						if dst_stack:get_name() == "" or dst_stack:get_name() == req.what then

							local can_deposit = math.min(can_take, stack_max - dst_stack:get_count() - (dst_cache[src_index] or 0))
							if can_deposit > 0 then
								src_cache[src_index] = (src_cache[src_index] or 0) + can_deposit
								dst_cache[dst_index] = (dst_cache[dst_index] or 0) + can_deposit
								can_take = can_take - can_deposit
								need_count = need_count - can_deposit

								l:info("Move " .. can_deposit .. " from " .. src_index .. " to " .. dst_index)
								table.insert(moves, {
									src = library.ItemLoc:new(src_pos, src_list_name, src_index),
									dst = library.ItemLoc:new(dst_pos, dst_list_name, dst_index),
									count = can_deposit
								})
							end
						end
					end
					-- We've come to the end of destinations loop, that means
					-- we haven't found where to deposit the amounts required.
					if can_take > 0 then
						l:info("Could only move " .. (req.count-can_take) .." of " .. req.what .. " from " .. src_index)
						return false
					end
				end
			end
		end

		-- We've come to the end of sources loop, that means we haven't found
		-- everything required in amounts required.
		if need_count > 0 then
			-- Can't satisfy
			l:info("Only found " .. (req.count-need_count) .. " / " .. req.count .. " of " .. req.what)
			return false
		end
	end
	return moves
end





-- Attempts to ensure that item_name is present in hotbar. If there are actions
-- that will make this happen, then they will be appended to the work queue and
-- this function will return the library.ItemLoc of the hotbar slot. Else it will return nil.
--   nil src_inv means personal inventory
function library.setup_move_to_hotbar(my_inv, item_name, src_pos)

	-- skip if already in hotbar
	local index = library.find_item_index_in_hotbar(item_name, my_inv["main"])
	if index then
		return library.ItemLoc:new(nil, "main", index)
	end

	src_inv = core.get_inventory(src_pos)
	-- search whole inventory for requested item
	local loc = library.find_item(src_inv, item_name)
	if not loc then
		l:err("Unable to move " .. item_name .. " to hotbar. It was not found in provided inventory location: " .. library.inventory_location_to_string(src_pos))
		return nil
	end


	local from_stack = src_inv[loc.list_name][loc.index]
	local from_loc   = library.ItemLoc:new(src_pos, loc.list_name, loc.index)

	local hotbar_loc = library.ItemLoc:new(nil, "main", 8) -- should work in all cases™
	-- try to find empty slot in hotbar, prioritising main list.
	local empty_index_in_main = library.find_item_index(my_inv["main"], "")
	local empty_loc = nil

	if not empty_index_in_main then
		empty_loc = library.find_item(my_inv, "")
		if not empty_loc then
			l:err("Unable to move " .. item_name .. " to hotbar. There was no empty stack to move old hotbar contents out.")
			return nil
		end

		empty_loc = library.ItemLoc:new(nil, empty_loc.list_name, empty_loc.index)
	elseif empty_index_in_main > library.get_hotbar_itemcount() then
		empty_loc = library.ItemLoc:new(nil, "main", empty_index_in_main)
	else
		-- empty slot is in hotbar, use it as slot for item_name
		hotbar_loc = library.ItemLoc:new(nil, "main", empty_index_in_main)
	end

	-- move existing item out of hotbar, if needed
	local stack = my_inv[hotbar_loc.list_name][hotbar_loc.list_index]
	if empty_loc then -- find empty spot to move this item to, if needed
		queue:append(function() return library.queued_move_stack(hotbar_loc, empty_loc, stack:get_count()) end)
		queue:append(function() return library.queued_ensure_stack(hotbar_loc, "", 0, 5) end)
	end

	-- actually move items to hotbar
	queue:append(function() return library.queued_move_stack(from_loc, hotbar_loc, from_stack:get_count()) end)
	queue:append(function() return library.queued_ensure_stack(hotbar_loc, from_stack:get_name(), from_stack:get_count(), 5) end)

	return hotbar_loc
end



-- Move all items one stack at a time from list defined in from_inv[from_lname) to to_inv(to_lname)
-- for which predicate_fun returns true.
-- Returns true if queue was updated (call queue:walk() to get the partial move done
-- TODO: some generalisation of input/output storages, so it is not just one to another inventory list.
function library.setup_queued_move_items_step(from_pos, from_lname, to_pos, to_lname, predicate_fun)
	-- TODO: stop all this copying.
	local from_inv = core.get_inventory(from_pos)
	local from_list = from_inv[from_lname]
	local to_inv = core.get_inventory(to_pos) or {}
	local to_list = to_inv[to_lname]

	for chest_index,chest_stack in pairs(from_list) do
		if predicate_fun(chest_stack) then
			l:debug("Suitable item " .. chest_stack:get_name() .. " found in " .. library.p2s(from_pos) .. ":" .. chest_index)
			-- Try to find space for it. First try existing non-empty slots of same item, then try empty slots.
			-- move to fill only one stack at a time, the leftovers will be done on the next call from process loop.
			local remaining = chest_stack:get_count()
			local stack_max = chest_stack:get_stack_max()

			-- distribute to non-full stacks first
			for main_index,main_stack in ipairs(to_list) do
				if main_stack:get_name() == chest_stack:get_name()
				and main_stack:get_count() < stack_max then
					local to_move = math.min(remaining, (stack_max - main_stack:get_count()))
					remaining = remaining - to_move
					l:debug("Moving (partial) " .. to_move .. " items " .. chest_index .. " -> " .. main_index)

					local chest_loc = library.ItemLoc:new(from_pos, from_lname, chest_index)
					local dest_loc  = library.ItemLoc:new(to_pos,   to_lname,   main_index)
					queue:append(function() return library.queued_move_stack(chest_loc, dest_loc, to_move) end)
					local wait_name = ((remaining > 0) and chest_stack:get_name()) or ""
					queue:append(function()
					  return library.queued_ensure_stack(chest_loc, wait_name, chest_stack:get_count()-to_move, 5)
					end)
					queue:append(function()
					  return library.queued_ensure_stack(dest_loc, chest_stack:get_name(), main_stack:get_count()+to_move, 5)
					end)
				end
			end

			-- distribute leftover to empty stack
			local main_index = library.find_empty_index(to_list)
			if remaining > 0 and main_index then
				l:debug("Moving " .. remaining .. " items " .. chest_index .. " -> " .. main_index)
				local to_move = remaining
				local chest_loc = library.ItemLoc:new(from_pos, "main", chest_index)
				local empty_loc = library.ItemLoc:new(nil, "main", main_index)
				queue:append(function() return library.queued_move_stack(chest_loc, empty_loc, to_move) end)
				queue:append(function() return library.queued_ensure_stack(chest_loc, "", 0, 5) end)
				queue:append(function() return library.queued_ensure_stack(empty_loc, chest_stack:get_name(), to_move, 5) end)
				remaining = 0
			end

			if remaining < chest_stack:get_count() then
				return true
			end
		end
	end
	return false
end
--[[
farming: Suitable item nodes_nature:limestone_block found in (626,-1050,-782):5
farming: Moving (partial) 3 items 5 -> 5
farming: Moving 1 items 5 -> 6
stack at :5 gets split into 3 to :5 and 1 to:6

farming: Suitable item nodes_nature:limestone_block found in (626,-1050,-782):14
farming: Moving (partial) 3 items 14 -> 6
farming: Moving 1 items 14 -> 8
stack at :14 gets split into 3 to :6 and 1 to :8

farming: Suitable item nodes_nature:limestone_block found in (626,-1050,-782):14
farming: Moving (partial) 1 items 14 -> 8
This is the same stack that was already transfered above! Looks like loop was called outside walk()?

lib/inventory Error: Timeout reached while waiting for 2 of nodes_nature:limestone_block at nil[main][8]
--]]

--[[
farming: Suitable item nodes_nature:limestone_boulder found in (669,-1048,-817):14
farming: Moving 1 items 14 -> 13 -- this means :13 was empty
farming: Suitable item nodes_nature:limestone_boulder found in (669,-1048,-817):14
farming: Moving (partial) 1 items 14 -> 13 -- this means :13 was non-empty.. why.. old reference perhaps?
lib/inventory Error: Timeout reached while waiting for 2 of nodes_nature:limestone_boulder at nil[main][13]
--]]
--[[
farming: farming loop:
farming: Suitable item nodes_nature:granite_block found in (649,-1050,-821):1
farming: Moving (partial) 3 items 1 -> 12
farming: farming loop:
farming: Suitable item nodes_nature:granite_block found in (649,-1050,-821):1
farming: Moving 3 items 1 -> 14
lib/inventory Error: Timeout reached while waiting for 3 of nodes_nature:granite_block at nil[main][14]

Very odd... like if these three blocks are present in both source and the partial stack slots at the same time.
            Perhaps prediction issues? Perhaps confirmation for main inventory comes faster than for node inventory?
			Either way, instead waiting for source stack to become empty might work around.
			Or we should wait for both confirmations? <- This fixed it.
			Odd that it always involves interaction between partial move and full move.
--]]



-- Move all stacks from one inventory list to another
-- Returns total number of left-over items, not delay!
function library.setup_queued_move_list(from_inv_loc, from_list_name, to_inv_loc, to_list_name)
	local src_list  = core.get_inventory(from_inv_loc)[from_list_name]
	local dest_list = core.get_inventory(to_inv_loc)[to_list_name]

	-- list of from_loc & to_loc & count
	local move_list = {}
	-- list of totals in slots. Affected is used for ensure_stack
	-- index -> { name, count, affected -> true/false }
	local dsl = {} -- dest_setup_list

	for dest_i = 1, #dest_list do
		local dest_stack = dest_list[dest_i]
		-- maybe table.insert?
		dsl[dest_i] = { name = dest_stack:get_name(), count = dest_stack:get_count(), affected = false }
	end

	local total_remaining = 0

	for src_i = 1,#src_list do
		local item_stack = src_list[src_i]
		if item_stack:get_count() > 0 then --schedule move
			-- Figure out where to deposit it
			local stack_max = item_stack:get_stack_max()
			local remaining = item_stack:get_count()
			-- (Dumb way first, optimizations later)
			-- first see if there are already stacks that could accept some more items:
			for dest_i = 1, #dsl do
				local dest_item = dsl[dest_i]
				if remaining > 0 and dest_item.name == item_stack:get_name() and dest_item.count < stack_max then
					-- add this item there
					local move_count = math.min(remaining, stack_max - dest_item.count)
					remaining = remaining - move_count
					dest_item.count = dest_item.count + move_count
					dest_item.affected = true

					local from = { inv_loc = from_inv_loc, list_name = from_list_name, list_index = src_i }
					local to   = { inv_loc = to_inv_loc,   list_name = to_list_name,   list_index = dest_i }
					table.insert(move_list, { from_loc = from, to_loc = to, count = move_count })
					print("Will move " .. move_count .. " from " .. src_i .. " to " .. dest_i)
				end
			end

			-- now see if there are empty slots that would accept the remaining items:
			for dest_i = 1, #dsl do
				local dest_item = dsl[dest_i]
				if remaining > 0 and dest_item.name == "" then
					-- add this item there
					local move_count = math.min(remaining, stack_max - dest_item.count)
					remaining = remaining - move_count
					dest_item.name = item_stack:get_name()
					dest_item.count = dest_item.count + move_count
					dest_item.affected = true

					local from = { inv_loc = from_inv_loc, list_name = from_list_name, list_index = src_i }
					local to   = { inv_loc = to_inv_loc,   list_name = to_list_name,   list_index = dest_i }
					table.insert(move_list, { from_loc = from, to_loc = to, count = move_count })
					print("Will move " .. move_count .. " from " .. src_i .. " to empty " .. dest_i)
				end
			end

			if remaining > 0 then
				print("library.setup_queued_move_list Warning: no space for " .. item_stack:get_name())
				total_remaining = total_remaining + remaining
			end
		end
	end
	-- go through the lists and add work items
	-- schedule verification
	for i = 1,#dsl do
		local item = dsl[i]
		if item.affected == true then
			local loc = { inv_loc = to_inv_loc, list_name = to_list_name, list_index = i }
			queue:prepend(function() return library.queued_ensure_stack(loc, item.name, item.count, 5) end)
		end
	end
	-- schedule actual moves
	for i = 1,#move_list do
		local item = move_list[i]
		queue:prepend(function() return library.queued_move_stack(item.from_loc, item.to_loc, item.count) end)
		if i % 5 == 4 then -- add a small delay
			queue:prepend(function() return 0.2 end)
		end
	end

	return total_remaining
end



function library.is_tool_ok()
	local itemstack = core.localplayer:get_wielded_item()
	local wear = itemstack:get_wear()
	return wear < 60000
end


-- Check if there is enough space in main to deposit an item and to place armor if we die
function library.do_I_have_space()
	local armor = library.ItemLocations:new()
	local armor_inv_name = core.localplayer:get_name() .. "_armor"
	armor:collect(armor_inv_name, "armor", "")

	local main = library.ItemLocations:new()
	main:collect(nil, "main", "")
	return #main.locations >= 1 + (6 - #armor.locations)
end
