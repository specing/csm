--[[
Library for Client-Side Mods (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Logging helpers
--]]

library.loggers = {}

library.Logger = {}
function library.Logger:new(name, log_level)
	--register for manipulation
	if library.loggers[name] then
		return library.loggers[name]
	end

	local o = {
		name = name,
	}
	setmetatable(o, self)
	self.__index = self

	o:set_log_level(log_level or 0)
	library.loggers[name] = o
	return o
end

function library.Logger:set_log_level(level)
	if level then
		self.log_level = level
	else
		self:err("Invalid log level: '" .. (level or "nil") .. "'")
	end
end

function library.Logger:get_log_level()
	return self.log_level
end


function library.Logger:debug(str)
	if self.log_level > 0 then
		print(minetest.colorize("#856262", self.name .. ": " .. str))
	end
end

function library.Logger:info(str)
	print(minetest.colorize("#76bbd4", self.name .. ": " .. str))
end

function library.Logger:warn(str)
	print(minetest.colorize("#e58500", self.name .. " Warning: " .. str))
end

function library.Logger:err(str)
	print(minetest.colorize("#8b0303", self.name .. " Error: " .. str))
end



local l = library.Logger:new("library")

local function help()
	return ".log: logger configuration, usage:\n"
		.. ".log level <name> <n>\n"
		.. ".log list -- list loggers\n"
end

core.register_chatcommand("log", {
	func = function(param)
		local args = string.split(param, ' ')
		local command = args[1]
		if command == "list" then
			-- sort first
			local sorted_keys = {}
			for name,_ in pairs(library.loggers) do
				table.insert(sorted_keys, name)
			end
			table.sort(sorted_keys)
			for _,key in ipairs(sorted_keys) do
				local o = library.loggers[key]
				l:info("  '" .. key .. "' has level " .. o:get_log_level())
			end
		elseif command == "level" then
			if not args[2] then
				l:err(help() .. "which log facility?")
			elseif not tonumber(args[3]) then
				l:err(help() .. "which log level?")
			elseif not library.loggers[args[2]] then
				l:err(help() .. "logger does not exist")
			else
				library.loggers[args[2]]:set_log_level(tonumber(args[3]))
			end
		else
			l:err(help() .. "no command given.")
		end
	end,
})
