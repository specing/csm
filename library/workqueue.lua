--[[
Library of common functions for Client-Side Mods (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.



This file implements a generic work queue to be used by other CSMs.

Each function added to the workqueue may return a delay, which tells how
long before the next function can execute. This is inferior to fine locking,
but the latter would take much longer to implement.
--]]
local l = library.Logger:new("workqueue")

-- Global/master queue object
queue = List.new()
queue.active = false


-- speedup as core.after does not support OOP (?)
function walk_queue(self)
	if not self.active then
		return
	end

	while true do
		local task = List.popleft(self)
		if task then
			local delay = task() or 0
			if delay > 0 then
				core.after(delay, walk_queue, self)
				return
			end
		else
			self.active = false
			l:debug("Reached end of work queue")
			break
		end
	end
end

function queue:walk()
	if not self.active then -- already being walked
		self.active = true
		walk_queue(self)
	end
end




local function help()
	return ".q is the workqueue-management interface\n"
	    .. ".q clear                    -- clear the queue\n"
	    .. ".q pause                    -- stop walking through tasks. Does not stop current task\n"
	    .. ".q resume|walk              -- start or resume walking through enqueued tasks\n"
end

core.register_chatcommand("q", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "walk"
		or args[1] == "resume" then
			queue:walk()
		elseif args[1] == "clear" then
			queue:clear()
		elseif args[1] == "pause" then
			queue.active = false
		else
			l:info(help())
		end
		return true
	end,
})
