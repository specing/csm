--[[
Library of common functions for Client-Side Mods (CSM)
Copyright (C) 2020-2025 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.



This file's purpose is to source other files in this CSM.
--]]
local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)

library = library or {}

function is_table_empty(table)
	return next(table) == nil
end

library.p2s = minetest.pos_to_string

dofile(csm_path .. "clock.lua")
dofile(csm_path .. "logger.lua")
dofile(csm_path .. "list.lua")
dofile(csm_path .. "csm_base.lua")
dofile(csm_path .. "workqueue.lua")
dofile(csm_path .. "data_manager.lua")

dofile(csm_path .. "punchnode.lua")
dofile(csm_path .. "nodes.lua")
dofile(csm_path .. "debug.lua")
dofile(csm_path .. "positions.lua")
dofile(csm_path .. "recipes.lua")

dofile(csm_path .. "item_location.lua")
dofile(csm_path .. "inventory.lua")

dofile(csm_path .. "autocrafter.lua")
dofile(csm_path .. "jumpdrives.lua")
