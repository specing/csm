--[[
Library of common functions for Client-Side Mods (CSM) - node-related functions
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local l = library.Logger:new("lib/nodes")

-- printer is a function(string)
-- node can be nil and is used instead of get_node_or_nil()
function dump_node_at(printer, pos, node)
	if not node then
		node = core.get_node_or_nil(pos)
		if not node then
			l:err("Failed to obtain node data at position " .. minetest.pos_to_string(pos))
			return
		end
	end

	printer("Node at " .. minetest.pos_to_string(pos) .. ":"  .. dump(node))
	local meta = core.get_meta(pos)
	printer("Meta: " .. (meta and dump(meta:to_table()) or "unavailable?"))

	local inv = core.get_inventory(pos)
	if inv then
		for list_name,list in pairs(inv) do
			printer("  Contents of inventory list " .. list_name .. ":")
			for i = 1,#list do
				printer(string.format("    %03d -> %s", i, list[i]:to_string()))
			end
		end
	end
end



-- is_node_ok(node.name) should return
--   nil if it is not ok,
--   0 if it is a connection,
--   1 if it is a connection AND one of the nodes you are searching for
-- visited is a map of hashed_position -> 1
-- found is a map of index -> { pos = pos, node = node }
function find_face_connected_nodes(pos, is_node_ok, visited, found)
	local hashed_pos = core.hash_node_position(pos)
	if visited[hashed_pos] then
		return
	end

	local node = core.get_node_or_nil(pos)
	if node then
		local result = is_node_ok(node.name)
		if result then
			visited[hashed_pos] = 1
			if result == 1 then
				table.insert(found, { pos = pos, node = node })
			end

			find_face_connected_nodes({ x = pos.x - 1, y = pos.y, z = pos.z}, is_node_ok, visited, found)
			find_face_connected_nodes({ x = pos.x + 1, y = pos.y, z = pos.z}, is_node_ok, visited, found)
			find_face_connected_nodes({ x = pos.x, y = pos.y - 1, z = pos.z}, is_node_ok, visited, found)
			find_face_connected_nodes({ x = pos.x, y = pos.y + 1, z = pos.z}, is_node_ok, visited, found)
			find_face_connected_nodes({ x = pos.x, y = pos.y, z = pos.z - 1}, is_node_ok, visited, found)
			find_face_connected_nodes({ x = pos.x, y = pos.y, z = pos.z + 1}, is_node_ok, visited, found)
		end
	end
end



-- tool_item can be nil (currently wielded item) or an itemstack of something
-- This function is partially based on dragonfireclient's diglib.
function library.get_dig_time(pos, tool_item)
	local node = minetest.get_node_or_nil(pos)
	local nodedef = node and minetest.get_node_def(node.name)
	local groups = nodedef and nodedef.groups

	if not groups then
		return nil
	end

	if not tool_item then
		local player = minetest.localplayer
		tool_item = player and player:get_wielded_item()
	end

	local toolcaps = tool_item and tool_item:get_tool_capabilities()

	local dp = core.get_dig_params(groups, toolcaps)
	if dp.diggable then
		l:debug("node '" .. node.name .. "' is diggable with tool in "
		        .. tostring(dp.time) .. "s and adds " .. tostring(dp.wear) .. " wear.")
	end

	local inv = core.get_inventory(nil)
	local hand = inv and inv.hand and inv.hand[1] or ItemStack("")
	local hand_toolcaps = hand and hand:get_tool_capabilities()

	local hdp = core.get_dig_params(groups, hand_toolcaps)
	if hdp.diggable then
		l:debug("node " .. node.name .. "' is diggable with hand in "
		         .. tostring(hdp.time) .. "s and adds " .. tostring(hdp.wear) .. " wear.")
	end

	local time = math.min(dp.diggable and dp.time or math.huge, hdp.diggable and hdp.time or math.huge)

	if time == math.huge then
		return nil
	end

	return time, dp, hdp
end


-- Tests whether there is anywhere to attach this node to.
function library.can_place_node(name, pos, attach_pos)
	local check_positions = {
		{x = pos.x,     y = pos.y - 1, z = pos.z },
		{x = pos.x,     y = pos.y + 1, z = pos.z },
		{x = pos.x - 1, y = pos.y,     z = pos.z },
		{x = pos.x + 1, y = pos.y,     z = pos.z },
		{x = pos.x,     y = pos.y,     z = pos.z - 1},
		{x = pos.x,     y = pos.y,     z = pos.z + 1},
	}

	for _,check_pos in ipairs(check_positions) do
		local node = minetest.get_node_or_nil(check_pos) -- returns name=ignore for unloaded
		if node then
			local def = minetest.get_node_def(node.name)
			if def and def.pointable then
				l:debug("Found attachment point for " ..minetest.pos_to_string(pos)
				        .. " at " .. minetest.pos_to_string(check_pos))
				return check_pos
			end
		end
	end
	l:debug("No suitable attachment points for placing a node at " .. minetest.pos_to_string(pos))

	return nil
end


function library.place_node(name, pos, attach_pos)
	if not attach_pos then
		attach_pos = library.can_place_node(name, pos, attach_pos)
		if not attach_pos then
			l:err("Cannot place node " .. name .. " at " ..  minetest.pos_to_string(pos) .. " due to no attachment point")
			return 0
		end
	end
	-- Ensure we have this node selected
	l:debug("Placing " .. name .. " at " .. minetest.pos_to_string(pos))
	local item_stack = core.localplayer:get_wielded_item()
	local old_index = core.get_wield_index()

	local index = old_index
	if item_stack:get_name() ~= name then
		index = library.find_item_index_in_hotbar(name)
		if not index then
			l:err("No '" .. name .. "' left to place or not in hotbar!")
			return 0.2
		end
		core.set_wield_index(index)
	end

	core.place_node(attach_pos, pos)

	if index ~= old_index then
		core.set_wield_index(old_index)
	end
	return 0.15
end



function library.queued_ensure_node(pos, name, timeout)
	if timeout < 0 then
		print("Timeout reached while waiting for node " .. name .. " at " .. minetest.pos_to_string(pos))
		queue:clear()
		return
	end

	local node = core.get_node_or_nil(pos)
	if node and node.name == name then
		return 0
	else
		queue:prepend(function() return library.queued_ensure_node(pos, name, timeout - 0.01) end)
		return 0.01
	end
end

-- Stalls the workqueue until the node called name is removed from pos (use this after digging/before place)
function library.queued_ensure_node_removed(pos, name, timeout)
	if timeout < 0 then
		print("Timeout reached while waiting for node " .. name .. " to go away from " .. minetest.pos_to_string(pos))
		queue:clear()
		return
	end

	local node = core.get_node_or_nil(pos)
	if node and node.name ~= name then
		return 0
	else
		queue:prepend(function() return library.queued_ensure_node_removed(pos, name, timeout - 0.01) end)
		return 0.01
	end
end



local function step_dig_start(dig_under, dig_above, tool_index)
	l:debug("start dig of " .. minetest.pos_to_string(dig_under))
	core.set_wield_index(tool_index)
	core.dig_node_start(dig_under, dig_above)
	return 0.2
end

local function step_dig_end(dig_under, dig_above, tool_index)
	l:debug("end dig of " .. minetest.pos_to_string(dig_under))
	core.set_wield_index(tool_index)
	core.dig_node_end(dig_under, dig_above)
	return 0.05
end

function setup_queued_digging(pos, tool_index)
	if not library.is_tool_ok() then
		l:err("tool is bad");
		return false
	elseif not library.do_I_have_space() then
		l:err("not enough space left for stack+armor on death");
		return false
	else
		local node = core.get_node_or_nil(pos)
		if not node or not is_safe_to_dig(node.name) then
			l:err("Failed to obtain node or not safe to dig(.safe_mode)")
			return false
		end

		if not tool_index then
			tool_index = core.get_wield_index()
		end
		local old_wield_index = core.get_wield_index()

		local tool_item = core.get_inventory()["main"][tool_index]

		local dig_time = library.get_dig_time(pos, tool_item)
		if not dig_time then
			l:err("Node at position " .. minetest.pos_to_string(pos) .. ": '" .. node.name
			      .. "' cannot be dug with tool " .. tool_item:get_name() .. " or by hand.")
			return false
		end

		local dig_under = pos
		local dig_above = { x = dig_under.x, y = dig_under.y + 1, z = dig_under.z }
		l:debug("Will dig " .. minetest.pos_to_string(dig_under) .. " in " .. tostring(dig_time) .. "s.")
		queue:prepend (function()
			step_dig_end(dig_under, dig_above, tool_index)
			core.set_wield_index(old_wield_index)
		end)
		queue:prepend (function()
			step_dig_start(dig_under, dig_above, tool_index)
			return dig_time
		end)
		return 0
	end
end



function library.use_item(name, pos, face_pos)
	if not face_pos then
		face_pos = {x = pos.x, y = pos.y + 1, z = pos.z } -- from above
	end
	-- Ensure we have this node selected
	local item_stack = core.localplayer:get_wielded_item()
	local old_index = core.get_wield_index()

	local index = old_index
	if item_stack:get_name() ~= name then
		index = library.find_item_index_in_hotbar(name)
		if not index then
			l:err("No '" .. name .. "' left to use or not in hotbar!")
			return nil
		end
		core.set_wield_index(index)
	end

	l:debug("Using " .. name .. " at " .. minetest.pos_to_string(pos))
	core.use_item(pos, face_pos)

	if index ~= old_index then
		core.set_wield_index(old_index)
	end
	return 0.15
end
