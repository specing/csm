--[[
Library for Client-Side Mods (CSM): CSM base utilities
Copyright (C) 2020-2025 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--]]
local l = library.Logger:new("lib/base")



-- Adds usage: and standard commands from parse_chatcommand_on_off(),
-- plus handles newlines.
function library.create_help_function(cmd_name, lines)
	local str =  "." .. cmd_name .. " usage:\n"
	str = str .. "." .. cmd_name .. " on|off|start|stop|toggle -- :)\n"

	for i,line in ipairs(lines) do
		if i < #lines then
			str = str .. "." .. cmd_name .. " " .. line .. "\n"
		else
			str = str .. "." .. cmd_name .. " " .. line
		end
	end

	return function() return str end
end


-- for use when registering a CSmod control chatcommand.
function library.parse_chatcommand_on_off(state, args)
	if not state or state.enabled == nil then
		l:err("This CSM is passive (state or state.enabled does not exist)")
	elseif args[1] == "on" or args[1] == "start" then
		state.enabled = true
	elseif args[1] == "off" or args[1] == "stop" then
		state.enabled = false
	elseif args[1] == "toggle" then
		if state.enabled then
			state.enabled = false
			l:info("CSM turned off")
		else
			state.enabled = true
			l:info("CSM turned on")
		end
	else
		return false
	end
	return true
end


-- wrapper around register_chatcommand that handles mod on/off
function library.register_state_chatcommand(command, state, object)
	local real_func = object.func
	object.func = function(param)
		local args = string.split(param, ' ')
		if library.parse_chatcommand_on_off(state, args) then
			return true
		else
			return real_func(param, args) -- already split up args
		end
	end

	core.register_chatcommand(command, object)
end
