--[[
Client-Side Mod (CSM) library: register_on_punchnode refined API
Copyright (C) 2024 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

--local l = library.Logger:new("libpunchnode")

local registered_on_punchnode_once = {}
local registered_on_punchnode_once_per_pos = {}

-- Call this and fun will be called only for the first node that is punched
-- if any fun in list returns true, then the punch is not forwarded to server.
-- However, other handlers will still be run.
function library.register_on_punchnode_once(fun)
	table.insert(registered_on_punchnode_once, fun)
end

-- Call this and fun will be called only once per position that is punched
-- Note: if some other position is punched in the mean time, then the
--       handler will be called again.
-- if any fun in list returns true, then the punch is not forwarded to server.
-- However, other handlers will still be run.
function library.register_on_punchnode_once_per_pos(fun)
	table.insert(registered_on_punchnode_once_per_pos, fun)
end


local last_punched_pos = { x = 100000, y = 100000, z = 100000 }
local last_punched_pos_prevented = false

core.register_on_punchnode(function(pos, node)
	local prevent_punch = false

	for _,fun in ipairs(registered_on_punchnode_once) do
		if fun(pos, node) == true then
			prevent_punch = true
		end
	end
	registered_on_punchnode_once = {}


	-- Engine generates a lot of on_punchnode calls for every node we punch, supposedly
	-- as a way to signal that we are still punching (digging) it?
	if vector.distance(last_punched_pos, pos) > 0.1 then
		last_punched_pos = { x = pos.x, y = pos.y, z = pos.z }

		last_punched_pos_prevented = false
		for _,fun in ipairs(registered_on_punchnode_once_per_pos) do
			if fun(pos, node) == true then
				last_punched_pos_prevented = true
			end
		end
	end

	if last_punched_pos_prevented == true then
		prevent_punch = true
	end

	return prevent_punch
end)
