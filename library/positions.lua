--[[
Client-Side Mod (CSM) library: record positions for use by other mods
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local l = library.Logger:new("positions")


-- Compute distance. Each position can be either {x,y,z} or nil, the latter meaning avatar position.
-- in case localplayer does not have a position, a far away invalid one is used.
function library.distance(pos1_or_nil, pos2_or_nil)
	if not pos1_or_nil or not pos2_or_nil then
		local lplayer = minetest.localplayer
		local my_pos = lplayer and  lplayer:get_pos() or {x = 100000, y = 100000, z = 100000}

		pos1_or_nil = pos1_or_nil or my_pos
		pos2_or_nil = pos2_or_nil or my_pos
	end

	return vector.distance(pos1_or_nil, pos2_or_nil)
end



--local mod_storage = core.get_mod_storage()

positions = { pos_list = {} }
--local state = minetest.deserialize(mod_storage:get_string("positions_state")) or {
--	positions = {}
--}
--mod_storage:set_string("safe_mode_state", minetest.serialize(state))

local function help()
	return ".pos help:\n"
	    .. ".pos list                    -- list all stored positions\n"
	    .. ".pos <number> punch          -- Add position by punching it\n"
	    .. ".pos <number> floor|feet|head|ceiling [+/- <x> <y> <z>] -- Add position with specified origin and offset\n"
	    .. ".pos <number> del            -- Remove pos at given index in list\n"
end

-- API
function positions:get(n)
	return self.pos_list[n]
end


function positions:iterate_on_bounding_box(func)
	local min = self.min
	local max = self.max

	if not min then
		l:err("Need at least two positions to iterate")
		return false
	else
		for x = min.x, max.x do
		for y = min.y, max.y do
		for z = min.z, max.z do
			local pos = { x = x, y = y, z = z }
			func(pos)
		end
		end
		end
	end
end


-- return two positions: .min with minimum coordinates and .max with maximum coordinates of the bounding box
function positions:update()
	local min = { x = math.huge, y = math.huge, z = math.huge }
	local max = { x =-math.huge, y =-math.huge, z =-math.huge }

	for _,pos in ipairs(self.pos_list) do
		--l:debug("Pos #" .. i .. ": " .. minetest.pos_to_string(pos))

		min.x = math.min(min.x, pos.x)
		min.y = math.min(min.y, pos.y)
		min.z = math.min(min.z, pos.z)

		max.x = math.max(max.x, pos.x)
		max.y = math.max(max.y, pos.y)
		max.z = math.max(max.z, pos.z)
	end

	if #self.pos_list == 0 then
		l:debug("Cannot give a bounding box without at least one position defined")
		self.min = nil
		self.max = nil
		self.volume = 0
		return nil
	else
		local lx = (1 + max.x - min.x)
		local ly = (1 + max.y - min.y)
		local lz = (1 + max.z - min.z)
		self.volume = lx * ly * lz
		self.min = min
		self.max = max
		l:info("Area volume: " .. self.volume
		       .. " size= " .. minetest.pos_to_string({ x = lx, y = ly, z = lz })
		       .. " min=" .. minetest.pos_to_string(vector.round(self.min))
		       .. " max=" .. minetest.pos_to_string(vector.round(self.max)))
	end
end

function positions:set(n, pos)
	self.pos_list[n] = vector.round(pos)
	l:info("Added position " .. minetest.pos_to_string(pos))
	self:update()
end

function positions:clear(n)
	if self.pos_list[n] then
		l:info("Removing position " .. minetest.pos_to_string(self.pos_list[n]))
		self.pos_list[n] = nil
		self:update()
	else
		l:err("No position at index " .. n)
	end
end


core.register_chatcommand("pos", {
	func = function(param)
		local args = string.split(param, ' ')
		local n = tonumber(args[1])

		if args[1] == "list" then
			for i,pos in ipairs(positions.pos_list) do
				local rounded = vector.round (pos)
				l:info("  #" .. i .. " rounded: " .. minetest.pos_to_string(rounded) .. " actual:" .. minetest.pos_to_string(pos))
			end
		elseif n then
			local my_pos = core.localplayer:get_pos()

			local offset = nil

			local dx = tonumber(args[4])
			local dy = tonumber(args[5])
			local dz = tonumber(args[6])
			if dx and dy and dz then
				offset = { x = dx, y = dy, z = dz }
			elseif args[3] then
				l:err("All 3 offset arguments must be numbers (x,y,z)")
				return
			end


			if args[3] == "+" then
				-- + is default
				offset = offset -- kill luacheck warning
			elseif args[3] == "-" then
				offset = { x = -offset.x, y = -offset.y, z = -offset.z }
			elseif not args[3] then
				offset = {x=0,y=0,z=0}
			else
				l:err("Unrecognised operation between origin and offset: '" .. args[3] .. "'")
			end

			if args[2] == "punch" then
				l:info("Punch a position to add it.")
				library.register_on_punchnode_once(function(pos)
					positions:set(n, vector.add(pos, offset))
				end)
			elseif args[2] == "ceiling" then
				positions:set(n, vector.add({x = my_pos.x, y = my_pos.y + 2, z = my_pos.z }, offset))
			elseif args[2] == "head" then
				positions:set(n, vector.add({x = my_pos.x, y = my_pos.y + 1, z = my_pos.z }, offset))
			elseif args[2] == "feet" then
				positions:set(n, vector.add({x = my_pos.x, y = my_pos.y + 0, z = my_pos.z }, offset))
			elseif args[2] == "floor" then
				positions:set(n, vector.add({x = my_pos.x, y = my_pos.y - 1, z = my_pos.z }, offset))
			elseif args[2] == "del" then
				positions:clear(n)
			else
				l:err("Invalid arguments")
				l:info(help())
			end
		else
			l:info(help())
		end

		return true
	end,
})
