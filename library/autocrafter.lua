--[[
Library for Client-Side Mods (CSM): Crafting helper
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local l = library.Logger:new("lib/autocrafter")

-- ensure that the crafting grid is empty and/or move leftovers back to main inv
local function autocrafter_cleanup_crafting()
	local nremaining = library.setup_queued_move_list(nil, "craft", nil, "bag1contents")
	if nremaining > 0 then
		l:warn("Warning: nowhere to store left-overs")
	end

	return 0
end

local function autocrafter_finish_crafting(what, how_many, dest_loc)
	-- actual crafting
	core.craft_stack(how_many)
	-- schedule clean-up of crafting grid left-overs
	queue:prepend(autocrafter_cleanup_crafting)

	local result_loc = { inv_loc = nil, list_name = "craftresult", list_index = 1 }

	queue:prepend(function() return library.queued_move_stack(result_loc, dest_loc, how_many) end)
	queue:prepend(function() return library.queued_ensure_stack(result_loc, what, how_many, 5) end)
	return 0
end

-- what ... is an item string used for sanity checking, may be nil
-- how_many ... obvious
-- dest_loc ... invloc where to deposit the result, may be nil
-- recipe ... flattened crafting grid of invlocs of the source items
function autocrafter_setup_crafting(what, how_many, dest_loc, recipe)
	local inv = core.get_inventory()
	-- Is the crafting grid empty?
	local grid = inv["craft"]

	if not library.is_list_empty(grid) then
		l:err("Crafting grid not empty, aborting!")
		return
	end
	-- schedule crafting step
	queue:prepend(function() return autocrafter_finish_crafting(what, how_many, dest_loc) end)
	-- verify that the correct item will be crafted
	local preview_loc = { inv_loc = nil, list_name = "craftpreview", list_index = 1 }
	queue:prepend(function() return library.queued_ensure_stack(preview_loc, what, how_many, 5) end)
	-- schedule itemstack moving
	for i = 1,#recipe do
		local from = recipe[i]
		if from then
			local to = { inv_loc = nil, list_name = "craft", list_index = i }
			queue:prepend(function() return library.queued_move_stack(from, to, how_many) end)
		end
	end
end









-- Gets destination stacks for how_many of what, preffering stacks of what that are not yet full
-- returns {success, locations}.
local function get_destinations(inv_pos, list_name, what, how_many)
	local remaining = how_many
	local locations = {}
	locations.total_count = 0 -- amount of free space
	locations.success = true
	local stack_max = ItemStack(what):get_stack_max()
	-- collect non-full stacks
	local what_locs = library.ItemLocations:new()
	what_locs:collect(inv_pos, list_name, what)

	for _,loc in pairs(what_locs.locations) do
		local difference = stack_max - loc.count
		if difference > 0 then
			table.insert(locations, loc)
			remaining = remaining - difference
			locations.total_count = locations.total_count + difference
			if remaining <= 0 then
				return locations
			end
		end
	end
	-- allocate empty stacks
	local empty_locs = library.ItemLocations:new()
	empty_locs:collect(inv_pos, list_name, "")

	for _,loc in pairs(empty_locs.locations) do
		table.insert(locations, loc)
		remaining = remaining - stack_max
		locations.total_count = locations.total_count + stack_max
		if remaining <= 0 then
			return locations
		end
	end

	locations.success = false
	locations.remaining = remaining
	return locations
end

-- autocrafting should be devided into
--  computing how many items can be crafted based on:
--  - how many prerequisites there are
--  - how many result items can be stored at destinations
--  populating the crafting grid - multimove?
--  - setup
--  - execution
--  - verification
--  doing the crafting, when crafting grid is populated
--  moving of result to where it is wanted
-- A class to handle crafting items
local Autocrafter = {

}

function Autocrafter:new(result_name, count, recipe, result_loc)
	local o = {
		result_name = result_name,
		result_loc  = result_loc,
		recipe = recipe,
		sources = {},
		destinations = {},

		requested_count = count or 1, -- ItemStack(result_name):get_stack_max(), -- requested
		max_crafteable = 0, -- based on prerequisite count
		max_deliverable = 0, -- based on space in destination
	}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Autocrafter:to_string()
	local str = tostring(self.requested_count) .. " of '" .. self.result_name .. " made from "
	for _,what in pairs(self.recipe) do
		str = str .. "'" .. (what or "") .. "', "
	end
	return str
end

function Autocrafter:get_max_craft_count()
	local c = math.min(self.max_crafteable, self.max_deliverable)
	l:debug("Can craft at most " .. c .. " of " .. self.result_name)

	return c
end

function Autocrafter:can_craft()
	return self:get_max_craft_count() >= self.requested_count
end

--function Autocrafter:add_source_locations(

function Autocrafter:add_item_source(inv_loc, list_name)
	local required_item_counts = {}
	-- determine how much of each item is required to craft one result
	for _,name in pairs(self.recipe) do
		required_item_counts[name] = (required_item_counts[name] or 0) + 1
	end
	-- go through given inventory pointer and collect all locations where the source items can be found
	local max_crafteable = math.huge

	for name,count in pairs(required_item_counts) do
		l:debug("Recipe requires " .. count .. " of " .. name)

		if not self.sources[name] then
			self.sources[name] = library.ItemLocations:new()
		end
		self.sources[name]:collect(inv_loc, list_name, name)

		self.sources[name]:sort_by_count_dsc() -- clear smaller stacks first (from right to left)
		l:debug("Collected " .. self.sources[name].total_count .. " of " .. name)

		max_crafteable = math.min(max_crafteable, math.floor(self.sources[name].total_count / count))
	end

	self.max_crafteable = max_crafteable
end

function Autocrafter:add_destination(inv_pos, list_name)
	local destinations = get_destinations(inv_pos, list_name, self.result_name, self.requested_count)
	if not destinations.success then
		l:err("unable to place crafting result")
		return
	end

	self.destinations = destinations
	self.max_deliverable = destinations.total_count
	--local stack_max = ItemStack(result):get_stack_max()
	--self.max_crafteable = math.min(max_crafteable, stack_max - dest_loc.count)
end


-- Craft one stack of output items
function Autocrafter:craft()
	local my_inv = core.get_inventory()

	-- Steps for crafting
	-- 1. move items to grid
	-- 2. call craft
	-- 3. move outputs to destinations

	-- TODO: assumes input count to be the same as output count (trunks->planks wont work)
	local craft_count = self.requested_count

	local result_stack = my_inv["craftresult"][1]
	local preview_stack = my_inv["craftpreview"][1]

	-- We're done crafting, move results
	if result_stack:get_name() == self.result_name and result_stack:get_count() == craft_count then
		-- After this we're done, no need to return to this function
		local result_loc = library.ItemLoc:new(nil, "craftresult", 1)
		l:debug("craftresult slot contains all results, setting up moves to main inventory")

		local still_to_move = craft_count
		while still_to_move > 0 do
			local to_loc = self.destinations[#self.destinations]
			local to_move = math.min(still_to_move, result_stack:get_stack_max() - to_loc.count)

			l:debug("Will move " .. to_move .. " from " .. result_loc:to_string() .. " to " .. to_loc:to_string())
			l:debug("to_loc.count: " ..  to_loc.count)

			queue:prepend(function() return library.queued_ensure_stack(to_loc, self.result_name, to_loc.count + to_move, 5) end)
			queue:prepend(function() return library.queued_move_stack(result_loc, to_loc, to_move) end)

			still_to_move = still_to_move - to_move
			to_loc.count = to_loc.count + to_move
			if to_loc.count == result_stack:get_stack_max() then
				self.destinations[#self.destinations] = nil
			end
		end
		return 0
	end


	if preview_stack:get_name() == self.result_name then -- still not done crafting
		l:debug("Still " .. preview_stack:get_count() .. " items remaining in preview, crafting")
		queue:prepend(function() return self:craft() end) -- do next step
		core.craft_stack(1)
		return 0.03
	end


	local craft_grid = my_inv["craft"]
	for grid_index,item_string in pairs(self.recipe) do
		local grid_stack = craft_grid[grid_index]
		local grid_loc = library.ItemLoc:new(nil, "craft", grid_index)
		l:debug("Processing " .. grid_loc:to_string() .. " : " .. grid_stack:get_count() .. " of " .. grid_stack:get_name())

		if grid_stack:get_name() == "" then -- setup move
			queue:prepend(function() return self:craft() end) -- do next step
			queue:prepend(function() return library.queued_ensure_stack(grid_loc, item_string, craft_count, 5) end)

			local sources = self.sources[item_string]

			local still_to_move = craft_count
			while still_to_move > 0 do
				local from = sources.locations[#sources.locations]

				local to_move = math.min(from.count, still_to_move)

				l:debug("Will move " .. to_move .. " from " .. from:to_string() .. " to " .. grid_loc:to_string())
				queue:prepend(function() return library.queued_move_stack(from, grid_loc, to_move) end)

				still_to_move = still_to_move - to_move
				from.count = from.count - to_move
				if from.count == 0 then
					sources.locations[#sources.locations] = nil
				end
			end

			return 0
		elseif grid_stack:get_name() ~= item_string then
			l:err("Stack at " .. grid_index .. " should be empty or '" .. item_string .. "'!")
			return
		-- else all good, advance to next slot
		end
	end

	-- if we've reached here then grid is setup, wait for craftpreview to show up desired item count
	queue:prepend(function() return self:craft() end) -- do next step
	local preview_loc = { inv_loc = nil, list_name = "craftpreview", list_index = 1 }
	-- craftpreview only gets one item, regardless of how many can be made
	queue:prepend(function() return library.queued_ensure_stack(preview_loc, self.result_name, 1, 5) end)
	l:debug("reached the end of grid setup. This is horribly wrong")
end



-- what ... is an item string used for sanity checking, may be nil
-- how_many ... obvious
-- dest_loc ... invloc where to deposit the result, may be nil
-- recipe ... flattened crafting grid of names of the source items
-- item_sources ... library.ItemLocs of where to take the items-nil for now
local function autocrafter_setup_crafting2(what, how_many, dest_loc, recipe, item_sources)
	local inv = core.get_inventory()
	-- Is the crafting grid empty?
	local grid = inv["craft"]

	if not library.is_list_empty(grid) then
		l:err("Crafting grid not empty, aborting!")
		return
	end

	local ac = Autocrafter:new(what, how_many, recipe)
	ac:add_item_source(nil, nil)
	ac:add_destination(nil, "main")
	how_many = ac:get_craft_count()
	--ac:calculate_move()

	-- list of from_loc & to_loc & count
	local move_list = {}

	-- Go through recipe and move stuff
	for dest_index,item_string in pairs(recipe) do
		local to = library.ItemLoc:new(nil, "craft", dest_index)
		local locs = locations[item_string]
		local still_to_move = how_many

		while still_to_move > 0 do
			local from = locs.locations[#locs.locations]

			local to_move = math.min(from.count, still_to_move)
			table.insert(move_list, { from_loc = from, to_loc = to, count = to_move })
			l:debug("Will move " .. to_move .. " from " .. from:to_string() .. " to " .. to:to_string())

			still_to_move = still_to_move - to_move
			from.count = from.count - to_move
			if from.count == 0 then
				locs.locations[#locs.locations] = nil
			end
		end
	end

	--[[
		dest_loc = {}
		dest_loc.inv_loc = nil
		dest_loc.list_name = "main"
		dest_loc.index = index
	--]]

	-- schedule crafting step
	queue:prepend(function() return autocrafter_finish_crafting(what, how_many, dest_loc) end)
	-- verify that the correct item will be crafted
	local preview_loc = { inv_loc = nil, list_name = "craftpreview", list_index = 1 }
	queue:prepend(function() return library.queued_ensure_stack(preview_loc, what, 1, 5) end)

	-- schedule verifications
	for dest_index,item_string in pairs(recipe) do
		local to = { inv_loc = nil, list_name = "craft", index = dest_index }
		queue:prepend(function() return library.queued_ensure_stack(to, item_string, how_many, 5) end)
	end

	-- schedule moves to grid
	for i = 1,#move_list do
		local item = move_list[i]
		queue:prepend(function() return library.queued_move_stack(item.from_loc, item.to_loc, item.count) end)
		if i % 5 == 4 then -- add a small delay
			queue:prepend(function() return 0.2 end)
		end
	end
end




core.register_chatcommand("cr", {
	func = function(param)
		local args = string.split(param, ' ')

		local ac = Autocrafter:new("glowstone:minerals", tonumber(args[1]) or 64, {
			nil,                      "rackstone:redrack",   nil,
			"glowstone:glowing_dust", "rackstone:dauthsand", "glowstone:glowing_dust",
			nil,                      nil,                   nil, },
			nil)

		l:debug(ac:to_string())

		ac:add_item_source(nil, "main")
		ac:add_destination(nil, "main")

		if ac:can_craft() then
			ac:craft()
			queue:walk()
		else
			l:err("Unable to craft " .. ac:to_string())
		end
	end,
})

core.register_chatcommand("ctnt", {
	func = function(param)
		local args = string.split(param, ' ')

		local ac = Autocrafter:new("tnt:tnt", tonumber(args[1]) or 64, {
			"basictrees:jungletree_wood",  "tnt:tnt_stick",  "basictrees:jungletree_wood",
			"tnt:tnt_stick",               "tnt:tnt_stick",  "tnt:tnt_stick",
			"basictrees:jungletree_wood",  "tnt:tnt_stick",  "basictrees:jungletree_wood"},
			nil)

		l:debug(ac:to_string())

		ac:add_item_source(nil, "main")
		ac:add_destination(nil, "main")

		if ac:can_craft() then
			ac:craft()
			queue:walk()
		else
			l:err("Unable to craft " .. ac:to_string())
		end
	end,
})

core.register_chatcommand("ctntstick", {
	func = function(param)
		local args = string.split(param, ' ')

		local ac = Autocrafter:new("tnt:tnt_stick", tonumber(args[1]) or 64, {
			"default:paper", "tnt:gunpowder", "default:paper",
			nil, nil, nil,
			nil, nil, nil, },
			nil)

		l:debug(ac:to_string())

		ac:add_item_source(nil, "main")
		ac:add_destination(nil, "main")

		if ac:can_craft() then
			ac:craft()
			queue:walk()
		else
			l:err("Unable to craft " .. ac:to_string())
		end
	end,
})

core.register_chatcommand("cjwood", {
	func = function(param)
		local args = string.split(param, ' ')

		local ac = Autocrafter:new("basictrees:jungletree_wood", tonumber(args[1]) or 64, {
			"basictrees:jungletree_trunk", nil, nil,
			nil, nil, nil,
			nil, nil, nil, },
			nil)

		l:debug(ac:to_string())

		ac:add_item_source(nil, "main")
		ac:add_destination(nil, "main")

		if ac:can_craft() then
			ac:craft()
			queue:walk()
		else
			l:err("Unable to craft " .. ac:to_string())
		end
	end,
})


-- params: what result how_many
local function craft9(what, result, count)
	local item = ItemStack(what)
	if not item then
		l:err("Item '" .. what .. "' does not exist")
		return
	end
	local result_item = ItemStack(result)
	if not result_item then
		l:err("Result item '" .. result .. "' does not exist")
		return
	end

	local ac = Autocrafter:new(result_item:get_name(), tonumber(count) or result_item:get_stack_max(), {
		item:get_name(), item:get_name(), item:get_name(),
		item:get_name(), item:get_name(), item:get_name(),
		item:get_name(), item:get_name(), item:get_name(), },
		nil)

	l:debug(ac:to_string())

	ac:add_item_source(nil, "main")
	ac:add_destination(nil, "main")

	if ac:can_craft() then
		ac:craft()
		queue:walk()
	else
		l:err("Unable to craft " .. ac:to_string())
	end
end


core.register_chatcommand("craft9", {
	func = function(param)
		local args = string.split(param, ' ')
		return craft9(args[1], args[2], args[3])
	end,
})
-- shortcuts
core.register_chatcommand("craft9cobble", {
	func = function(param)
		return craft9("default:cobble", "moreblocks:cobble_compressed")
	end
})
core.register_chatcommand("craft9sulfur", {
	func = function(param)
		return craft9("technic:sulfur_lump", "technic:sulfur_block")
	end,
})
core.register_chatcommand("craft9desertcobble", {
	func = function(param)
		return craft9("default:desert_cobble", "moreblocks:desert_cobble_compressed")
	end,
})
core.register_chatcommand("craft9snow", {
	func = function(param)
		return craft9("default:snow", "default:snowblock")
	end,
})
