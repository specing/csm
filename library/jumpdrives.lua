--[[
Client-Side Mod (CSM) library: translate saved coordinates when a jumpdrive jumps
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local l = library.Logger:new("lib/jumpdrive")

local jd_watchlist = {}
function library.register_on_jumpdrive_jump(func)
	table.insert(jd_watchlist, func)
end

function library.translate_position_if_required(jump, pos)
	if    jump.from.x - jump.radius <= pos.x and pos.x <= jump.from.x + jump.radius
	  and jump.from.y - jump.radius <= pos.y and pos.y <= jump.from.y + jump.radius
	  and jump.from.z - jump.radius <= pos.z and pos.z <= jump.from.z + jump.radius
	  then
		local old_pos_str = minetest.pos_to_string(pos)

		pos.x = pos.x + jump.offset.x
		pos.y = pos.y + jump.offset.y
		pos.z = pos.z + jump.offset.z

		l:debug("Translated position " .. old_pos_str .. " to " .. minetest.pos_to_string(pos))
	end
end

-- List translation helper
function library.translate_position_list_if_required(jump, list)
	for i = 1,#list do
		local pos = list[i]
		library.translate_position_if_required(jump, pos)
	end
end


local last_jump = nil

--"Jump executed in 1227 ms"
local match_pattern = "Jump executed in.*ms"
core.register_on_receiving_chat_message(function(message)
	local captures = string.match(message, match_pattern)
	if captures and last_jump then
		l:info("Message captured: \"" .. message .. "\", executing jump watchlist handlers.")

		for i=1,#jd_watchlist do
			jd_watchlist[i](last_jump)
		end
		last_jump = nil

		return true
	end
	return false
end)

core.register_on_sending_fields(function(pos, formname, fields)
	if formname == "" and fields.jump then
		local x = tonumber(fields.x)
		local y = tonumber(fields.y)
		local z = tonumber(fields.z)
		local radius = tonumber(fields.radius)

		if x and y and z and radius then
			local new_pos = { x = x, y = y, z = z }

			local offset = vector.subtract(new_pos, pos)
			l:debug("Jump from: " .. minetest.pos_to_string(pos)
			        .. " to " .. minetest.pos_to_string(new_pos)
			        .. " offset " .. minetest.pos_to_string(offset))

			local jump = {}
			jump.from = pos
			jump.to   = new_pos
			jump.radius = radius
			jump.offset = offset

			l:debug("Waiting for \"Jump executed in.*ms\" message indicating jump success...")
			last_jump = jump -- let's see if it was successful or not
		end
	end
end)
