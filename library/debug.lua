--[[
Library for Client-Side Mods (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


--]]
local l = library.Logger:new("debug", 1)

enable_debug = false

core.register_chatcommand("ed", { -- enable debug
	func = function(param)
		if param == "on" then
			enable_debug = true
		else
			enable_debug = false
		end
		return true
	end,
})


library.register_on_punchnode_once_per_pos(function(pos, node)
	if not enable_debug then return end

	l:debug("Punched node at " .. minetest.pos_to_string(pos))
	dump_node_at(function(str) l:debug(str) end, pos, node)
	return false
end)


core.register_on_sending_fields(function(pos, formname, fields)
	if not enable_debug then return end
	local pos_str = pos and minetest.pos_to_string(pos) or "nil"
	l:debug("Formspec input to " .. pos_str .. ", formname: \"" .. formname .. "\" fields: "
	        .. dump(fields))
end)
