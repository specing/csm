--[[
Library for Client-Side Mods (CSM): inventory/item/stack location API
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

function library.inventory_location_to_string(loc)
	if not loc then
		return "nil"
	elseif type(loc) == "table" then
		if loc.x and loc.y and loc.z then
			return minetest.pos_to_string(loc)
		else
			return "unknown table"
		end
	elseif type(self.inv_loc) == "string" then
		return "detached(" .. loc .. ")"
	else
		return "unknown"
	end
end



library.ItemLoc = {}
-- Create a new item location, all parameters can be nil
function library.ItemLoc:new(inv_loc, list_name, index, count)
	local o = {}
	o.inv_loc = inv_loc
	o.list_name = list_name
	o.list_index = index

	o.count = count -- initially can be get_stack():get_count(), later used for keeping track of moves

	setmetatable(o, self)
	self.__index = self
	return o
end


function library.ItemLoc:get_count()
	return self.count
end


function library.ItemLoc:get_stack()
	if self:is_valid() then
		local inv = core.get_inventory(self.inv_loc)
		return inv[self.list_name][self.list_index]
	else
		return nil
	end
end


function library.ItemLoc:is_valid()
	-- inv_loc can be nil (player inv)
	return self.list_name and self.list_index
end


function library.ItemLoc:to_string()
	return library.inventory_location_to_string(self.inv_loc) .. "[" .. self.list_name .. "][" .. self.list_index .. "]"
end







library.ItemLocations = {}

function library.ItemLocations:new()
	local o = {
		total_count = 0,
		locations = {}, -- list of library.ItemLoc
	}
	setmetatable(o, self)
	self.__index = self
	return o
end

-- collect all locations in list_name that have item_name (and how many)
function library.ItemLocations:collect(inventory_loc, list_name, item_name)
	local inv = core.get_inventory(inventory_loc)
	if not inv then
		print("library.ItemLocations:collect Error: inv location does not exist?!")
		return
	end

	if list_name then
		local list = inv[list_name]

		if list then
			for i = 1, #list do
				if list[i]:get_name() == item_name then
					local loc = library.ItemLoc:new(inventory_loc, list_name, i, list[i]:get_count())
					table.insert(self.locations, loc)
					local stack = list[i]
					self.total_count = self.total_count + stack:get_count()
				end
			end
		else
			print("library.ItemLocations:collect Error: list " .. list_name .. " is nil?")
		end
	else
		-- add items from all lists
		for l_name,list in pairs(inv) do
			self:collect(inventory_loc, l_name, item_name)
		end
	end
end

function library.ItemLocations:sort_by_count_asc()
	table.sort(self.locations, function(lhs,rhs) return lhs:get_count() < rhs:get_count() end)
end

function library.ItemLocations:sort_by_count_dsc()
	table.sort(self.locations, function(lhs,rhs) return lhs:get_count() > rhs:get_count() end)
end
