--[[
TechPack repair helper Client-Side Mod (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.



Switch a techpack node on after the repair kit is used on it (it is repaired).
Break a techpack node if it's aging is above a threshold.
--]]

-- [Tubelib] state =stopped, counter = 0, aging = 0
local match_pattern = "%[Tubelib%] state =[%a]+, counter = [%d%a]+, aging = (%d+)"
local wrench_item_string = "tubelib:end_wrench"


minetest.register_on_item_use(function(item, pointed_thing)
	if item:get_name() == "tubelib:repairkit" and pointed_thing.type == "node" then
		print("Scheduling turn-on")
		-- Right click activate for pushers
		core.after(0.25, function()
			core.place_node(pointed_thing.under, pointed_thing.above)
		end)
		-- Press the button for the ones with formspecs
		core.after(0.25, function()
			core.send_fields(pointed_thing.under, "", { state_button = "" })
		end)
	end
end)


-- As the tubelib message below does not say which node we used the wrench on,
-- remember this info when it is used.
local used_on_thing = nil
minetest.register_on_item_use(function(item_stack, pointed_thing)
	if item_stack:get_name() == wrench_item_string then
		used_on_thing = pointed_thing
	end
end)



core.register_on_receiving_chat_message(function(message)
	local captures = string.match(message, match_pattern)
	--print("captures: " .. dump(captures))
	if not captures then return end

	local aging = tonumber(captures)
	if not (aging > 500) then print("Aging too low: " .. tostring(aging)); return end

	local inv = core.get_inventory()
	local main_inv = inv["main"]
	-- First step: smack the machine with the wrench to break it
	local wrench_index = library.find_item_index(main_inv, "tubelib:end_wrench")
	if not wrench_index then print("No wrench?"); return end

	if not used_on_thing or not used_on_thing.type == "node" then print("not pointing at a node"); return end

	local old_i = core.get_wield_index()

	core.set_wield_index(wrench_index)
	print("Breaking techpack node")
	core.place_node(used_on_thing.under, used_on_thing.above)
	core.set_wield_index(old_i)
end)
