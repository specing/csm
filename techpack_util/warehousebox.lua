--[[
TechPack helper Client-Side Mod (CSM) - warehousebox utilities
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]


-- Empty the filter slots of an otherwise empty warehousebox
local function empty_whb_filter(pos)
	local inventory = core.get_inventory(pos)
	if not inventory then
		print("Warehousebox has no inventory?")
		return
	end

	if not library.is_list_empty(inventory["main"])
	or not library.is_list_empty(inventory["shift"])
	or not library.is_list_empty(inventory["input"]) then
		print("WHB still has items inside, aborting")
		return
	end

	for from_i = 1, #inventory["filter"] do
		local stack = inventory["filter"][from_i]
		local result_slots = library.get_result_slots(core.get_inventory(nil), "main", stack:get_name())
		local i = next(result_slots)
		--print("result_slots: " .. dump(result_slots))
		if i then
			local v = result_slots[i]
			local to_move = math.min(stack:get_count(), v.remaining)
			core.move_stack(pos, "filter", from_i, nil, "main", v.index, to_move)
		else
			print("Unable to clear WHB filter")
		end
	end
end


library.register_on_punchnode_once_per_pos(function(pos, node)
	if node.name == "techpack_warehouse:box_gold"
	or node.name == "techpack_warehouse:box_copper"
	or node.name == "techpack_warehouse:box_steel" then
		empty_whb_filter(pos, inventory)
		return true
	end
	return false
end)
