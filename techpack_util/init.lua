--[[
Techpack utility Client-Side Mod (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.



This file's purpose is to source other files in this CSM.
--]]
local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)

dofile(csm_path .. "repairs.lua")
dofile(csm_path .. "warehousebox.lua")
