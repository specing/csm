Blocky Enhancement Suite (BES)
==============================

A collection of CSMs (Client-Side Mods), primarily aimed at automating the menial
tasks on BlS. A lot of them might also be useful on other servers.

License: GNU Affero GPL version 3 or any later version.


INSTALL
=======

1. Compile `https://framagit.org/specing/minetest`, branch `5.4.1-specing`
   (you can run it with `./bin/minetest` instead of installing).

2. Clone this to `~/.minetest/clientmods` or link the desired mods.

3. For every desired mod, set `load_mod_<name> = true` in ~/.minetest/clientmods/mods.conf`
