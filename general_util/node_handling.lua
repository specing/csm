--[[
General utility Client-Side Mod (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.




node-handling currently does auto-harvesting of honey when punching a hive.
--]]

local function harvest(pos, src_inv, list_name, item_name)
	if src_inv[list_name] then
		local result_slots = library.get_result_slots(core.get_inventory(nil), "main", item_name)
		local to_move = src_inv[list_name][1]:get_count()
		print ("Attempting to move " .. to_move)
		print (dump(result_slots))

		for _,v in pairs(result_slots) do
			if to_move == 0 then break end
			core.move_stack(pos, list_name, 1, nil, "main", v.index, math.min(to_move, v.remaining))
			to_move = to_move - v.remaining
		end
	end
end



library.register_on_punchnode_once_per_pos(function(pos, node)
	if node.name == "mobs:beehive" then -- real bee hive
		harvest(pos, core.get_inventory(pos), "beehive", "mobs:honey")
		return true
	elseif node.name == "xdecor:hive" then -- fake bee hive
		harvest(pos, core.get_inventory(pos), "honey", "xdecor:honey")
		return true
	end
	return false
end)
