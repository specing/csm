--[[
Minetest+specing Client-Side Mod (CSM) for using anvil+hammer to auto-repair tools/armor
Copyright (C) 2022 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--]]
local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)

local l = library.Logger:new("AnvilAutoRepair")
-- z coordinate increases when going north

function queued_wait_for_node(pos, name, timeout, next_function)
	if timeout < 0 then
		l:err("Timeout reached while waiting for node " .. name .. " at " .. minetest.pos_to_string(pos))
		return
	end

	local node = core.get_node_or_nil(pos)
	if node and node.name == name then
		return next_function()
	else
		queue:prepend(function() return queued_wait_for_node(pos, name, timeout - 0.01, next_function) end)
		return 0.01
	end
end



local function find_node_in_radius(center, radius, name)
	for x = center.x - radius, center.x + radius do
		for z = center.z - radius, center.z + radius do
			for y = center.y - radius, center.y + radius do
				local pos = { x = x, y = y, z = z }
				local node = core.get_node_or_nil(pos)
				if node and node.name == name then
					return pos
				end
			end
		end
	end

	return nil
end

-- Ensure that the item stack at <stack_loc> has at least <count> of <what>
-- if stack is ensured, execute another function
function queued_wait_for_stack(stack_loc, what, count, timeout, next_function)
	if timeout < 0 then
		l:err("Timeout reached while waiting for " .. count .. " of " .. what
		      .. " at " .. stack_loc.list_name .. "[" .. stack_loc.list_index .. "]")
		queue:clear()
		return
	end
	local inv = core.get_inventory(stack_loc.inv_loc)
	if not inv then
		l:err("queued_wait_for_stack: inventory missing!")
		return
	end
	local list = inv[stack_loc.list_name]
	if not list then
		l:err("queued_wait_for_stack: list missing!")
		return
	end

	local stack = list[stack_loc.list_index]

	if stack:get_name() == what and stack:get_count() == count then
		return next_function()
	else
		queue:prepend(function() return queued_wait_for_stack(stack_loc, what, count, timeout - 0.01, next_function) end)
		return 0.01
	end
end


local function queued_smack_anvil(anvil_pos, next_function)
	local hammer_index = library.find_item_index_in_hotbar("anvil:hammer")
	if not hammer_index then
		l:err("Hammer not found in hotbar!")
		return
	end

	local node = core.get_node_or_nil(anvil_pos)
	if node and node.name == "anvil:anvil" then
		local inventory = core.get_inventory(anvil_pos)
		local stack = inventory["input"][1]

		if stack:get_wear() < 1000 then
			l:info("Tool '" .. stack:get_name() .. "' repaired")
			queue:prepend(next_function)
		else
			l:debug("Executing one step of tool repair. Current wear: " .. stack:get_wear())
			local oldindex = core.get_wield_index()
			core.set_wield_index(hammer_index)
			core.dig_node(anvil_pos, { x=anvil_pos.x, y=anvil_pos.y+1, z=anvil_pos.z})
			core.set_wield_index(oldindex)

			queue:prepend(function() return queued_smack_anvil(anvil_pos, next_function) end)
		end
	else
		l:err("There is no anvil at anvil's position")
	end

	return 0.1
end

local repairs_on = true

-- TODO Set wield index to something that will not break due to auto-chop.
--[[
local function on_fail()
	local food_index = library.find_item_index_in_hotbar("farming:cucumber")

	if food_index then
		core.set_wield_index(food_index)
	end
end
--]]

function repair_tool()
	if repairs_on then
		core.after(1, repair_tool)
	else
		return
	end

	-- check if we are not falling?

	-- Find anvil
	local anvil_index = library.find_item_index_in_hotbar("anvil:anvil")
	if not anvil_index then
		l:err("Anvil not found in hotbar!")
		return
	end

	-- Find hammer
	local hammer_index = library.find_item_index_in_hotbar("anvil:hammer")
	if not hammer_index then
		l:err("Hammer not found in hotbar!")
		return
	end

	-- Find spot to place anvil at
	local my_pos = vector.round(core.localplayer:get_pos())
	local anvil_pos = find_node_in_radius(my_pos, 2, "air")
	               or find_node_in_radius(my_pos, 2, "vacuum:vacuum")
	               or find_node_in_radius(my_pos, 2, "default:water_source")
	               or find_node_in_radius(my_pos, 2, "default:water_flowing")

	if not anvil_pos then
		l:err("Could not find empty spot to place anvil in radius of 2 around you")
		return
	end

	-- Find if tool has to be repaired. This is done last to warn player of
	-- inability to repair long before it becomes a problem.
	if library.is_tool_ok() then
--		l:info("Tool is ok, skipping repair")
		return
	end
	local tool_stack = core.localplayer:get_wielded_item()
	l:info("Will place anvil at " .. minetest.pos_to_string(anvil_pos))

	-- Place anvil
	local oldindex = core.get_wield_index()
	core.set_wield_index(anvil_index)
	local above = { x = anvil_pos.x, y = anvil_pos.y+1, z = anvil_pos.z, }
	core.place_node(anvil_pos, above)
	core.set_wield_index(oldindex)

	-- Abort if anvil not placed (and switch to non-tool slot in inv!) such as due to protection
	queue:prepend(function()
		queued_wait_for_node(anvil_pos, "anvil:anvil", 5, function()
	-- Place tool on anvil (right click)
			l:info("Anvil placed, placing tool on it..")
			core.place_node(anvil_pos, above)
	-- Wait for tool being placed
			local loc = { inv_loc = anvil_pos, list_name = "input", list_index = 1 }
			return queued_wait_for_stack(loc, tool_stack:get_name(), 1, 5, function()
				l:info("Tool is now on anvil, repairing...")

	-- Find/wield hammer
	-- Smack anvil a few times
				queued_smack_anvil(anvil_pos, function()
	-- Select empty slot
	-- Right click anvil to recover tool
					core.place_node(anvil_pos, above)
	-- Dig anvil
					return queued_wait_for_stack(loc, "", 0, 5, function()
						local oldindex = core.get_wield_index()
						core.set_wield_index(hammer_index)
						core.dig_node(anvil_pos, above)
						core.set_wield_index(oldindex)
					end)
				end)

				return 0.1
			end)
		end)
	end)
	queue:walk()
end




local function help()
	return ".anvil on|off\n"
end


local function anvil_chatcommand_func(param)
	local args = string.split(param, ' ')

	if args[1] == "off" then
		l:info("turning off")
		repairs_on = false
	elseif args[1] == "on" then
		l:info("turning on")
		repairs_on = true
		repair_tool()
	else
		l:err("unknown command")
		l:info(help())
	end
	return true
end


core.register_chatcommand("anvil", {
	func = anvil_chatcommand_func,
})
