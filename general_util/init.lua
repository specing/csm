--[[
General utility Client-Side Mod (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.




This file's purpose is to source other files in this CSM.
--]]
local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)

dofile(csm_path .. "node_handling.lua")
dofile(csm_path .. "protect.lua")
dofile(csm_path .. "auto_repair_anvil.lua")
dofile(csm_path .. "disconnect_on_damage.lua")
dofile(csm_path .. "disconnect_at_night.lua")


core.register_chatcommand("send_pos", {
	func = function(param)
		local my_pos = core.localplayer:get_pos()
		local str = "I am located at " .. minetest.pos_to_string(vector.round(my_pos))
		if param and #param > 0 then
			minetest.send_chat_message("/msg " .. param .. " " .. str)
		else
			minetest.send_chat_message(str)
		end
		return true
	end,
})




local eggs_spam = false
local eggs_thrown = 0

local function throw_egg()
	if not eggs_spam then return end
	local main_inv = core.get_inventory().main
	local egg_index = library.find_item_index(main_inv, "mobs:egg")
	if not egg_index then
		print("No eggs left, " .. eggs_thrown .. " thrown.")
		eggs_spam = false
		eggs_thrown = 0
		return
	end

	local old_i = core.get_wield_index()
	core.set_wield_index(egg_index)
	core.use_item()
	core.set_wield_index(old_i)

	eggs_thrown = eggs_thrown + 1
	core.after(0.2, throw_egg)
end

core.register_chatcommand("egg_spam", {
	func = function(param)
		if eggs_spam then
			eggs_spam = false
		else
			eggs_spam = true
			core.after(0.2, throw_egg)
		end
		return true
	end,
})
