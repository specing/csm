--[[
Client-Side Mod (CSM) to disconnect on damage (useful when xdotooling)
Copyright (C) 2022 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local mod_storage = core.get_mod_storage()

local state = minetest.deserialize(mod_storage:get_string("dod_state")) or {}

state.log_level = nil -- migrated to .log
local l = library.Logger:new("disconnect_on_damage")

state.enabled = true
state.enabled = false



local function help()
	return ".disconnect_on_damage:\n"
	    .. ".dod on|off -- enable/disable\n"
end

core.register_chatcommand("dod", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "on" then
			state.enabled = true
			mod_storage:set_string("dod_state", minetest.serialize(state))
		elseif args[1] == "off" then
			state.enabled = false
			mod_storage:set_string("dod_state", minetest.serialize(state))
		else
			l:info(help())
		end

		return true
	end,
})


local function on_hp_change(old_hp, new_hp)
	local damage = old_hp - new_hp
	if damage < 0 then return end

	if not state.enabled then return end

	l:info(tostring (damage) .. " damage taken. HP " .. tostring(old_hp) .. " -> " .. tostring(new_hp))
	if new_hp <= 0 then
		l:info("couldn't save ya, oh well :/")
	elseif old_hp <= 2 * math.abs(damage) + 1 then -- +1 is just in case
		l:info("Taking another hit would kill you, disconnecting")
		minetest.disconnect()
	elseif new_hp < 10 then
		l:info("New HP below threshold, disconnecting")
		minetest.disconnect()
	end
end


local function on_damage_taken(damage)
	if not state.enabled then return end

	local lplayer = core.localplayer or minetest.localplayer
	local new_hp = 20
	if lplayer then
		new_hp = lplayer:get_hp()
	else
		l:err("core.localplayer is nil?!")
	end

	local old_hp = new_hp + damage

	l:debug("Calling on_hp_change from legacy on damage taken method: ")
	on_hp_change(old_hp, new_hp)
end


if core.register_on_hp_change then
	core.register_on_hp_change(on_hp_change)
else
	l:warn("Your engine lacks a register_on_hp_change lua handler. This program may not work well.")
	core.register_on_damage_taken(on_damage_taken)
end
