--[[
Protection helper Client-Side Mod (CSM) - for the areas mod
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--]]
local l = library.Logger:new("protect")

local function help()
	return "First add at least 1 position with the .pos command, then"
	    .. ".protect                    -- Send /area_pos1/2 corresponding to the bounding box encompassing all positions.\n"
	    .. ".protect <name:string>      -- Like above, but also send a /protect <name> command"
end

core.register_chatcommand("protect", {
	func = function(param)
		if not positions.min then
			l:err("Unable to obtain bounding box from library/positions")
			l:info(help())
			return true
		end
		local min = positions.min
		local max = positions.max

		min = vector.round(min)
		local pos1_str = "/area_pos1 "..tostring(min.x).." "..tostring(min.y).." "..tostring(min.z)
		l:debug("Sending: " .. pos1_str)
		core.send_chat_message(pos1_str)

		max = vector.round(max)
		local pos2_str = "/area_pos2 "..tostring(max.x).." "..tostring(max.y).." "..tostring(max.z)
		l:debug("Sending: " .. pos2_str)
		core.send_chat_message(pos2_str)


		local center = vector.round(vector.divide(vector.add(max, min), 2))
		l:info("Protecting area around " .. minetest.pos_to_string(center))

		local area_size = vector.round(vector.add(vector.subtract(max, min), vector.new(1,1,1)))
		l:info("Protected area size: " .. minetest.pos_to_string(area_size))

		if param and #param > 0 then
			local protect_str = "/protect " .. param
			l:debug("Sending: " .. protect_str)
			core.send_chat_message(protect_str)
		else
			l:err("No argument provided, issue /protect yourself.")
		end
		return true
	end,
})
