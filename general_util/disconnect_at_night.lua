--[[
Client-Side Mod (CSM) to disconnect at night
Copyright (C) 2022 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local mod_storage = core.get_mod_storage()

local state = minetest.deserialize(mod_storage:get_string("dan_state")) or {}

state.log_level = nil -- migrated to .log
local l = library.Logger:new("disconnect_at_night")
state.light_min = state.light_min or 14
--state.enabled = false


local function loop()
	if not state.enabled then return end

	local lplayer = core.localplayer or minetest.localplayer
	if not lplayer then
		l:err("No local player?!")
		return
	end
	local my_pos = lplayer:get_pos()

	local light = minetest.get_node_light(vector.add(my_pos, {x=0,y=1,z=0}), nil)

	l:debug("Light above me: " .. tostring(light))

	if light < state.light_min then
		l:info("Light dropped below threshold (" .. tostring(state.light_min) .. ")")
		minetest.disconnect()
	end

	core.after(3, loop)
end



local function help()
	return ".disconnect_at_night:\n"
	    .. ".dan on|off -- enable/disable\n"
	    .. ".dan <n>    -- disconnect below this light level\n"
end

core.register_chatcommand("dan", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "on" then
			state.enabled = true
			core.after(1, loop)
		elseif args[1] == "off" then
			state.enabled = false
		elseif tonumber(args[1]) then
			state.light_min = tonumber(args[1])
		else
			l:info(help())
		end
		mod_storage:set_string("dan_state", minetest.serialize(state))

		return true
	end,
})

if state.enabled then
	core.after (10, loop)
end
