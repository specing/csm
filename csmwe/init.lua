--[[
Minetest+specing world-editing Client-Side Mod (CSM)
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--]]
local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)

WorldEdit = {}
dofile(csm_path .. "node_info.lua")
dofile(csm_path .. "node_storage.lua")

local l = library.Logger:new("WorldEdit")
local node_storage = WorldEdit.NodeStorage:new()
-- z coordinate increases when going north


function queued_ensure_param2(pos, param2, timeout)
	if timeout < 0 then
		print("Timeout reached while waiting for param2 " .. param2 .. " at " .. minetest.pos_to_string(pos))
		queue:clear()
		return 0
	end

	local node = core.get_node_or_nil(pos)
	if node and node.param2 == param2 then
		return 0
	else
		queue:prepend(function() return queued_ensure_param2(pos, param2, timeout - 0.01) end)
		return 0.01
	end
end


local function queued_rotate_node(pos, new_param2)
	local name = "screwdriver:screwdriver"
	local oldindex = core.get_wield_index()
	local index = library.find_item_index_in_hotbar(name)
	if not index then
		l:err("No '" .. name .. "' in hotbar!")
		return 0
	end

	local under = { x = pos.x, y = pos.y,   z = pos.z, }
	local above = { x = pos.x, y = pos.y+1, z = pos.z, }
	local node = core.get_node_or_nil(above)
	if not node then
		l:err("Node is nil!")
		return 0
	end
	local param2 = node.param2
	-- TODO: fix the pos/above/below mess
	l:debug("Node at " .. minetest.pos_to_string(above) .. " has param2: " .. param2 .. ", desired: " .. new_param2)

	-- use rotate turns 0->1->2->3->0
	-- place rotate turns 0->4->8->12->16->20->0
	local target_vert  = math.floor(new_param2/4)
	local current_vert = math.floor(param2/4)

	if target_vert ~= current_vert then
		l:debug("Executing one vertical rotation")
		core.set_wield_index(index)
		core.place_node(above, under)
		core.set_wield_index(oldindex)

		queue:prepend(function() return queued_rotate_node(pos, new_param2) end)
		queue:prepend(function() return queued_ensure_param2(above, (current_vert + 4) % 24, 5) end)
		return 0
	end

	local target_horiz = new_param2 % 4
	local current_horiz = param2 % 4

	if target_horiz ~= current_horiz then
		l:debug("Executing one horizontal rotation")
		core.set_wield_index(index)
		core.use_item(above, under)
		core.set_wield_index(oldindex)
		queue:prepend(function() return queued_rotate_node(pos, new_param2) end)
		queue:prepend(function() return queued_ensure_param2(above, (current_horiz + 1) % 4, 5) end)
		return 0
	end

	return 0
end


local state = {
	count = 0,
	layer_count = 0,
	lights_period = 0, -- when this is 0, modulo is nan and the comparison with 0 will always be false
	nodes_to_skip = {},
}
state.nodes_to_skip["air"] = 1
state.nodes_to_skip["ignore"] = 1
state.nodes_to_skip["vacuum:vacuum"] = 1


local function after_layer()
	l:info("layer " .. state.layer_count .. " complete (" .. state.count .. " nodes total)")
	-- place lights
	if 0 == state.layer_count % state.lights_period then
		local x = (state.maxx + state.minx)/2
		local z = (state.maxz + state.minz)/2
		local y = state.active_node.y

		local pos        = { x = x,  y = y + 2, z = z }
		local attach_pos = { x = x,  y = y + 3, z = z }
		queue:prepend(function() library.place_node("moreblocks:slab_super_glow_glass_1", pos) end)
	end
	compact_inventory()
end



local function compute_next_step_saved()
	local my_pos = vector.round (core.localplayer:get_pos())
	local anchor_pos = positions:get(1)

	local node_data = node_storage:get_next_position(anchor_pos, my_pos)
	if not node_data then
		l:info("world-editing complete")
		return
	elseif node_data == true then -- have work, but elsewhere. Pause
		core.after(1, compute_next_step_saved)
		return
	end

	-- TODO:
	local my_inv = core.get_inventory()
	-- 1) dig existing other node
	-- 2) find new node in inventory
	-- 3) move new node to hotbar (and find/make free hotbar slot for it)
	if not library.setup_move_to_hotbar(my_inv, node_data.node.name) then
		l:err("Ran out of " .. node_data.node.name .. "?!?!")
		return
	end
	-- 4) DONE select this node  DONE
	-- 5) DONE place
	local x = node_data.pos.x + anchor_pos.x
	local y = node_data.pos.y + anchor_pos.y
	local z = node_data.pos.z + anchor_pos.z

	local pos        = { x = x,  y = y - 0, z = z }
	queue:append(function() return library.place_node(node_data.node.name, pos) end)
	queue:append(function() return library.queued_ensure_node(pos, node_data.node.name, 5) end)
	-- 6) rotate if needed
	queue:append(function() return queued_rotate_node(pos, node_data.node.param2) end)

	queue:append(compute_next_step_saved)
end


local function planar_manhattan(pos1, pos2)
	return math.abs(pos1.x - pos2.x) + math.abs(pos1.z - pos2.z)
end

local function compute_next_step()
	local my_pos = vector.round (core.localplayer:get_pos())
	-- compute next active node
	while true do
		local an = state.active_node
		an.x = an.x + 1
		if an.x > state.maxx then -- next row
			an.x = state.minx
			an.z = an.z + 1
		end
		if an.z > state.maxz then -- next layer
			an.x = state.minx
			an.z = state.minz
			an.y = an.y - 1

			state.layer_count = state.layer_count + 1
			after_layer()
		end

		if an.y < state.miny then
			l:info("world-editing complete")
			return
		end

		local dist = planar_manhattan(my_pos, an)
		l:debug("checking " .. minetest.pos_to_string(an) .. " dist: " .. dist)
		if not state.dist or dist <= state.dist then

			local node = core.get_node_or_nil(an)
			-- skip (it actually was nil once...)
			if not node then
				l:debug("Unable to obtain node at " .. minetest.pos_to_string(an))
			elseif not state.nodes_to_skip[node.name] then
				break
			end
		end
	end

	state.count = state.count + 1
	setup_queued_digging(state.active_node)
	queue:append(compute_next_step)
end


local function help()
	return ".csmwe off\n"
	    .. ".csmwe copy [file_name]     -- Copy nodes in bounding box selected with .pos\n"
	    .. ".csmwe load [file_name]     -- Load nodes from filename in minetest's current directory\n"
	    .. ".csmwe fill [item_string]   -- fill bounding box with item_string or node in hand. Use csmwe paste to apply\n"
	    .. ".csmwe paste                -- Paste stored nodes with lowest pos corresponding to .pos 1\n"
	    .. ".csmwe down [radius_x] [radius_z] [depth] [height] -- by default digs a 10 nodes deep 5x5 hole where you are standing\n"
	    .. ".csmwe downmanhattan [dist] [depth] [height] -- digs all nodes in x/z manhattan distance <= dist, defaults to 1x1 manhole\n"
	    .. ".csmwe lights <number>      -- set the layer period at which autominer places lights above your head\n"
	    .. ".csmwe ignore <node name>   -- add nodename to list of nodes not dug by autominer\n"
	    .. ".csmwe unignore <node name> -- remove nodename from list of nodes not dug by autominer"
end


local function csmwe_chatcommand_func(param)
	local args = string.split(param, ' ')

	if args[1] == "off" then
		l:info("turning off")
		queue:clear()
	elseif args[1] == "copy" then
		node_storage = WorldEdit.NodeStorage:new() -- overwrite old node_storage
		node_storage:from_bounding_box(positions)
		if args[2] then
			node_storage:save(args[2])
		end
	elseif args[1] == "fill" then
		node_storage = WorldEdit.NodeStorage:new() -- overwrite old node_storage
		local stack = core.localplayer and core.localplayer:get_wielded_item()

		if args[2] then
			node_storage:fill_bounding_box(positions, args[2])
		elseif stack:get_name() ~= "" then
			node_storage:fill_bounding_box(positions, stack:get_name())
		else
			l:err("no item name given nor is any item held in hand. Aborting.")
		end
	elseif args[1] == "load" and args[2] then
		node_storage = WorldEdit.NodeStorage:load(args[2]) -- overwrite old node_storage
	elseif args[1] == "paste" then
		if not positions:get(1) then
			l:err("You need to specify anchor position (minimal x,y,z) to paste a build")
		else
			node_storage:convert_for_placement()

			local inventory = core.get_inventory()
			local missing_material = node_storage:get_missing_materials(inventory)
			for name,count in pairs(missing_material) do
				l:info(string.format("Missing material: need %8d more of '%s'", count, name))
			end

			if next(missing_material) then
				l:err("Missing some material and cannot build. See above.")
				return
			end

			queue:prepend(compute_next_step_saved)
			queue:walk()
		end
	elseif args[1] == "down" then
		local maxx = tonumber(args[2]) or 2
		local maxz = tonumber(args[3]) or 2
		local miny = tonumber(args[4]) or 10
		local maxy = tonumber(args[5]) or 0

		local my_pos = vector.round (core.localplayer:get_pos())
		state.minx = my_pos.x - maxx
		state.maxx = my_pos.x + maxx
		-- -1 as I'm standing one layer above the floor.
		state.miny = my_pos.y - 1 - miny + 1
		state.maxy = my_pos.y - 1 + maxy
		state.minz = my_pos.z - maxz
		state.maxz = my_pos.z + maxz
		-- -1 is due to compute_next_step incrementing it
		state.active_node = { x = state.minx - 1, y = state.maxy, z = state.minz }
		state.count = 0
		state.layer_count = 0

		queue:prepend(compute_next_step)
		queue:walk()
	elseif args[1] == "downmanhattan" then
		local dist = tonumber(args[2]) or 0
		local miny = tonumber(args[3]) or 10
		local maxy = tonumber(args[4]) or 0

		state.dist = dist -- this is checked to turn on manhattan distance logic
		local my_pos = vector.round (core.localplayer:get_pos())
		state.minx = my_pos.x - dist
		state.maxx = my_pos.x + dist
		-- -1 as I'm standing one layer above the floor.
		state.miny = my_pos.y - 1 - miny + 1
		state.maxy = my_pos.y - 1 + maxy
		state.minz = my_pos.z - dist
		state.maxz = my_pos.z + dist
		-- -1 is due to compute_next_step incrementing it
		state.active_node = { x = state.minx - 1, y = state.maxy, z = state.minz }
		state.count = 0
		state.layer_count = 0

		queue:prepend(compute_next_step)
		queue:walk()
	elseif args[1] == "lights" then
		state.lights_period = tonumber(args[2]) or 0
	elseif args[1] == "ignore" and type(args[2]) == "string" then
		state.nodes_to_skip[args[2]] = 1
		l:info("Added " .. args[2] .. " to list of nodes to ignore.")
	elseif args[1] == "unignore" and type(args[2]) == "string" then
		state.nodes_to_skip[args[2]] = nil
		l:info("Removed " .. args[2] .. " from the list of nodes to ignore.")
	else
		l:err("unknown command")
		l:info(help())
	end
	return true
end


core.register_chatcommand("csmwe", {
	func = csmwe_chatcommand_func,
})
