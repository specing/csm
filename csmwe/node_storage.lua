--[[
Minetest+specing world-editing Client-Side Mod (CSM)
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local l = library.Logger:new("WorldEdit")


local NodeStorage = {}
function NodeStorage:new()
	local o = {
		bb = nil, --bounding box structure
		fname = nil,
		nodes = {},
	}
	setmetatable(o, self)
	self.__index = self

	o:invalidate_info()

	return o
end


function NodeStorage:invalidate_info()
	self.info = {}
end





-- convert node names that were changed on place into their pre-place names
-- (e.g. cables)
function NodeStorage:convert_for_placement()
	for i = 1,#self.nodes do
		local node_data = self.nodes[i]
		if node_data then
			local node_name = node_data.node.name
			local info = WorldEdit.node_info[node_name]
			if info then
				if info.aliased_to then
					node_data.node.name = info.aliased_to
				elseif info.no_place then
					self.nodes[i] = nil
				end
			end
		end
	end
	self:invalidate_info()
end


function NodeStorage:fill_bounding_box(bb, item_string)
	if not bb.min then
		l:err("Need at least two positions to fill an area")
	else
		self.bb = bb
		-- save all
		l:debug("Anchor position: " .. minetest.pos_to_string(bb.min))
		for x = bb.min.x, bb.max.x do
		for y = bb.min.y, bb.max.y do
		for z = bb.min.z, bb.max.z do
			local pos = { x = x, y = y, z = z }
			local rel_pos = vector.subtract(pos, bb.min)
			local node = { name = item_string, param2 = 0 }
			table.insert(self.nodes, { pos = rel_pos, node = node} )
		end
		end
		end

		self:print_bill_of_materials()
	end

end


function NodeStorage:from_bounding_box(bb)
	-- needs two positions to save
	if not bb.min then
		l:err("Need at least two positions to save a build")
	else
		self.bb = bb
		-- save all
		l:debug("Anchor position: " .. minetest.pos_to_string(bb.min))
		for x = bb.min.x, bb.max.x do
		for y = bb.min.y, bb.max.y do
		for z = bb.min.z, bb.max.z do
			local pos = { x = x, y = y, z = z }
			local rel_pos = vector.subtract(pos, bb.min)
			local node = core.get_node_or_nil(pos)
			if node then
				--l:debug("Saved node at " .. minetest.pos_to_string(rel_pos) .. ": '"..node.name.."', param2: " ..node.param2)
				table.insert(self.nodes, { pos = rel_pos, node = node} )
			else
				l:debug("Unable to obtain node at " .. minetest.pos_to_string(pos))
				table.insert(self.nodes, nil)
			end
		end
		end
		end

		self:print_bill_of_materials()
	end
end


-- count number of stored nodes of each type = bill of materials
function NodeStorage:get_bill_of_materials()
	if self.info.bill_of_materials then return self.info.bill_of_materials end

	local BOM = {}
	for _,node_data in pairs(self.nodes) do -- can an index be nil?
		local node_name = node_data.node.name
		BOM[node_name] = (BOM[node_name] or 0) + 1
	end
	self.info.bill_of_materials = BOM
	return BOM
end


function NodeStorage:print_bill_of_materials()
	local BOM = self:get_bill_of_materials()

	for name,count in pairs(BOM) do
		l:info(string.format("Bill of materials: need %8d of '%s'", count, name))
	end
end


-- Go through inventory at inv and return name->count pairs of missing stuff (modify BOM)
function NodeStorage:get_missing_materials(inv)
	local BOM = self:get_bill_of_materials()
	local need = table.copy(BOM) -- deep copy

	for _,list in pairs(inv) do
		for index = 1,#list do
			local stack = list[index]
			local count = stack:get_count()
			local name  = stack:get_name()

			local need_count = need[name]
			if need_count then
				need_count = need_count - count
				if need_count > 0 then
					need[name] = need_count
				else
					need[name] = nil
				end
			end
		end
	end

	return need
end


-- for 3x3 structure 1 in height
--  1  2  3  4  5  6  7  8  9
-- x0 x0 x0 x1 x1 x1 x2 x2 x2  countx=3
-- y0 y0 y0 y0 y0 y0 y0 y0 y0  county=1
-- z0 z1 z2 z0 z1 z2 z0 z1 z2  countz=3

-- x,y,z are relative to the saved structure
function NodeStorage:coordinates_to_index(x, y, z)
	local countx = self.bb.max.x - self.bb.min.x + 1
	local county = self.bb.max.y - self.bb.min.y + 1
	local countz = self.bb.max.z - self.bb.min.z + 1

	if x < 0 or x >= countx
	or y < 0 or y >= county
	or z < 0 or z >= countz then
		--l:debug(" pos " .. x .. ", " .. y .. ", " .. z .. " is out of bounds")
		return nil
	end
	-- first array index is occupied by node at bb.min.{x,y,z}

	local index = 1 + x * county*countz + y * countz + z
	l:debug("Position " .. x .. ", " .. y .. ", " .. z .. " has index " .. index)

	return index
end

function NodeStorage:get_node(x, y, z)
	local index = self:coordinates_to_index(x, y, z)
	if not index then
		return nil
	end

	local node_data = self.nodes[index]
	if node_data then
		return node_data
	else
		l:debug("node at index " .. index .. " does not exist")
		for i,stored_node in pairs(self.nodes) do -- can an index be nil?
			local node_name = stored_node.node.name
			l:debug("Stored node " .. i .. ": " .. node_name)
		end
		return nil
	end
end

function NodeStorage:set_node(x, y, z, data)
	local index = self:coordinates_to_index(x, y, z)

	self.nodes[index] = data
end

-- TODO: lua can't really list files in a directory...
function NodeStorage:load(fname)
	local file = io.open(fname, "r")

	if file then
		local contents = file:read("*all")
		file:close()
		local object = minetest.deserialize(contents)
		object.fname = fname
		setmetatable(object, self)
		object.__index = self

		l:debug("Nodes right after load:")
		for i,node_data in pairs(object.nodes) do -- can an index be nil?
			local node_name = node_data.node.name
			l:debug("Stored node " .. i .. ": " .. node_name)
		end
		object:invalidate_info()
		object:print_bill_of_materials()
		l:debug("Nodes  after load:")
		for i,node_data in pairs(object.nodes) do -- can an index be nil?
			local node_name = node_data.node.name
			l:debug("Stored node " .. i .. ": " .. node_name)
		end
		return object
	else
		l:err("Unable to open file '" .. fname .. "' for loading!")
	end
end


function NodeStorage:save(fname)
	self.fname = fname
	local file = io.open(fname, "w")

	if file then
		file:write(minetest.serialize(self))
		file:flush()
		file:close()
	else
		l:err("Unable to open file '" .. fname .. "' for saving!")
	end
end


-- Iterate through all stored node data
-- world origin is the point in the world that anchors our build (0,0,0)
-- my_pos is my position in world
function NodeStorage:get_next_position(world_origin, my_pos)

	-- figure out where we are located in the saved structure
	local rel_pos_x = my_pos.x - world_origin.x
	local rel_pos_y = my_pos.y - world_origin.y
	local rel_pos_z = my_pos.z - world_origin.z

	l:debug("Relative position wrt. structure anchor: " .. rel_pos_x .. ", " .. rel_pos_y .. ", " .. rel_pos_z)

	local radius = 3
	for x = rel_pos_x - radius, rel_pos_x + radius do
	for y = rel_pos_y - radius, rel_pos_y + radius do
	for z = rel_pos_z - radius, rel_pos_z + radius do
		local stored_node = self:get_node (x,y,z)
		if stored_node then
			-- check if position is not occupied yet
		    local wx = x + world_origin.x
		    local wy = y + world_origin.y
		    local wz = z + world_origin.z
		    local wpos = { x = wx,  y = wy, z = wz }

			l:debug("Node relpos: " .. wx .. ", " .. wy .. ", " .. wz)
			l:debug("Node saved relpos: " .. minetest.pos_to_string(stored_node.pos))
			l:debug("Placement coordinates: " .. minetest.pos_to_string(wpos))
			l:debug("My position: " .. minetest.pos_to_string(my_pos))

			local world_node = minetest.get_node_or_nil(wpos)

			-- check if this node is already what it needs to be
			if world_node and world_node.name then
				if world_node.name == stored_node.node.name then
					self:set_node(x,y,z, nil) -- remove node from build schematics
				else
					local attach_pos = library.can_place_node(stored_node.node.name, wpos, nil)
					if attach_pos then -- TODO pass attach_pos to avoid recomputation.
						return stored_node
					end
				end
			end
		end
	end
	end
	end

	-- If we're done, return nil, else return false
	if is_table_empty(self.nodes) then
		return false
	else
		return true
	end
end
--[[
	if not self.current_index then
		self.current_index = 1
	elseif self.current_index > #self.nodes then
		self.current_index = nil
	end

	if not self.current_index then
		return nil
	else
		local index = self.current_index
		self.current_index = index + 1

		local node_data = self.nodes[index]
		-- easy, dumb way
		if node_data then
			return node_data
		else
			return self:get_next_position()
		end
	end
--]]
WorldEdit.NodeStorage = NodeStorage
