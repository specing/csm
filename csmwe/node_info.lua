--[[
Minetest+specing world-editing Client-Side Mod (CSM)
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--]]

WorldEdit.node_info = {
	   ["air"] = {
		no_place = 1,
	}, ["default:lava_flowing"] = {
		no_place = 1,
	}, ["default:river_water_flowing"] = {
		no_place = 1,
	}, ["default:water_flowing"] = {
		no_place = 1,
	}, ["vacuum:vacuum"] = {
		no_place = 1,
	}, ["technic:corium_flowing"] = {
		no_place = 1, -- untested
	}, ["technic:hv_digi_cable_plate_2"] = {
		aliased_to = "technic:hv_digi_cable_plate_1",
	}, ["technic:hv_digi_cable_plate_5"] = {
		aliased_to = "technic:hv_digi_cable_plate_1",
	}, ["technic:mv_digi_cable_plate_2"] = {
		aliased_to = "technic:mv_digi_cable_plate_1",
	}, ["technic:mv_digi_cable_plate_5"] = {
		aliased_to = "technic:mv_digi_cable_plate_1",
	},
}
