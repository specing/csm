--[[
Terumet utility Client-Side Mod (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.




Refuse to dig certain nodes with anything but ore saws.
--]]
local l = library.Logger:new("oresaw")

local ore_saw_nodes = {
	['asteroid:copperore']=1,
	['asteroid:diamondore']=1,
	['asteroid:goldore']=1,
	['asteroid:ironore']=1,
	['asteroid:meseore']=1,

	['default:stone_with_coal']=1,
	['default:stone_with_copper']=1,
	['default:stone_with_diamond']=1,
	['default:stone_with_gold']=1,
	['default:stone_with_iron']=1,
	['default:stone_with_mese']=1,
	['default:stone_with_tin']=1,

	['moreores:mineral_mithril']=1,
	['moreores:mineral_silver']=1,

--	['nether:heart_ore']=1,
--	['nether:sulfur_ore']=1,
	['nether:titanium_ore']=1,

	['quartz:quartz_ore']=1,

	['technic:mineral_chromium']=1,
	['technic:mineral_lead']=1,
--	['technic:mineral_sulfur']=1,
	['technic:mineral_uranium']=1,
	['technic:mineral_zinc']=1,

	['terumet:ore_dense_raw']=1,
	['terumet:ore_raw']=1,
	['terumet:ore_raw_desert']=1,
	['terumet:ore_raw_desert_dense']=1,

	['titanium:titanium_in_ground']=1,
}


local enabled = true
library.register_on_punchnode_once_per_pos(function(pos, node)
	if not enabled then return end

	local itemstack = core.localplayer:get_wielded_item()
	if not (itemstack:get_name() == "terumet:tool_ore_saw_adv" or itemstack:get_name() == "terumet:tool_ore_saw")
	and ore_saw_nodes[node.name] then
		local my_inv = core.get_inventory(nil)
		local saw_index = library.find_item_index(my_inv["main"], "terumet:tool_ore_saw_adv")
		               or library.find_item_index(my_inv["main"], "terumet:tool_ore_saw")

		if not saw_index then
			l:err("Use ore-saw on ore blocks, cowardly refusing to dig.")
			return true -- do not start digging
		end
		l:debug("Using ore-saw at index " .. tostring(saw_index))
		local old_index = core.get_wield_index()
		core.set_wield_index(saw_index)
		core.use_item(pos, vector.add(pos, vector.new(0, 1, 0)))
		core.set_wield_index(old_index)
		return true
	end
	return false
end)



local function help()
	return "Invalid use of .oresaw, help:\n"
	    .. ".oresaw on|off\n"
end


core.register_chatcommand("oresaw", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "on" then
			enabled = true
		elseif args[1] == "off" then
			enabled = false
		else
			l:info(help())
		end

		return true
	end,
})
