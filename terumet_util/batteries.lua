--[[
Terumet utility Client-Side Mod (CSM) - battery handling
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local empty_batteries = {
	["terumet:item_batt_therm"] = 1,
	["terumet:item_batt_cop"] = 1,
}
local full_batteries = {
	["terumet:item_batt_therm_full"] = 1,
	["terumet:item_batt_cop_full"] = 1,
}

-- Battery charging (punch thermobox) with empty battery in main inventory list and "wielding" empty slot
local function handle_thermobox(pos, inventory)
	-- "battery" is the heat output list
	local itemstack = core.localplayer:get_wielded_item()
	local inventory = core.get_inventory(pos)
	local my_inv = core.get_inventory(nil)

	local battery_name = inventory["battery"][1]:get_name()
	if full_batteries[battery_name] then
		local index = library.find_empty_index(my_inv["main"])
		if index then
			core.move_stack(pos, "battery", 1, nil, "main", index, 1)
		else
			print("Nowhere to place charged battery in your inventory.")
		end
	elseif battery_name == "" then
		local index = nil
		for name,_ in pairs(empty_batteries) do
			index = library.find_item_index(my_inv["main"], name)
			if index then break end
		end

		if index then
			core.move_stack(nil, "main", index, pos, "battery", 1, 1)
			core.after(2, handle_thermobox, pos)
		else
			print("Heat output slot is empty and there is no empty battery in your inventory.")
		end
	elseif empty_batteries[battery_name] then
		-- wait some more.
		print("Battery not charged yet, waiting some more time...")
		core.after(1, handle_thermobox, pos)
	else
		print("Unknown item in batt slot: " .. battery_name)
	end
end


library.register_on_punchnode_once_per_pos(function(pos, node)
	if node.name == "terumet:mach_thermobox" then
		handle_thermobox(pos)
	end
	return false
end)
