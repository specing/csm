--[[
Terumet auto-reformer CSM
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.



When close to a designated chest, this CSM will swap all worn-down tools/armor
with repaired equivalents in the chest.
--]]

local l = library.Logger:new("reformer")
-- Hardcoded for now. This is actually a chest node.
local reformer_pos = { y = 81, x = 11999, z = 11997 }


local move_stack = function(from_inv, from, from_i, to_inv, to, to_i, count)
--	print("moving " .. tostring(count)
--	      .. " from " .. library.inventory_location_to_string(from_inv) .. "[" .. from .. "][" .. tostring(from_i)
--		  .. "] to " ..  library.inventory_location_to_string(to_inv)   .. "[" .. to   .. "][" .. tostring(to_i) .. "]")
	core.move_stack(from_inv, from, from_i, to_inv, to, to_i, count)
end

-- General idea:
-- Go BACKWARDS* through reformer chest's inventory. If we have the item
-- in one of our inventories (main, bag, armor, ...) AND the item has wear:
--   place used item into reformer (into free slots at end)
--   take new item out of the reformer and into our inventory.
--
-- * reformer will shift everything left and require another get_inventory()
--   if this procedure is not followed (our cached list would be invalidated).

local reformer = {
	-- take index
	--            -1 means that we just arrived in range
	--            -2 means that we wait to get ento range
	--            -3 means that we finished repair and wait to get out of range
	--               before starting another cycle
	take_index = -3, -- index where we take items
	put_index = nil, -- index where we place items

	chest_list_name = "",
	chest_list = nil,

	-- list of {itemname, invname, listname, index}
	inventory_map = {}
}

function reformer:log(message)
	print("[reformer] " .. message)
end

function reformer:reset()
	self.take_index = -3
	self.put_index = nil
	self.inventory_map = {}
end

-- TODO: handle cases where the same item appears 1+ times in reformer
-- and in my inventory (or both). The former is easy as we only handle it once
-- The latter can be handled by making a table of list->index->true/nil
-- for indices that we already handled.
-- Or, we could (at init), make a itemname->[{invname, listname, index}, ..]
-- mapping, and then remove items as we repair them.
function reformer:add_inventory(inv_loc)
	local inventory = core.get_inventory(inv_loc)

	for listname,list in pairs(inventory) do
		for index,stack in pairs(list) do
			if stack:get_wear() > 0 then
				self:log("Adding " .. stack:get_name() .. " to list")
				table.insert(self.inventory_map, { stack:get_name(), inv_loc, listname, index })
			end
		end
	end
end


-- Called from globalstep
function reformer:deprecated_step(distance)
	if self.take_index >= 0 then
		print("Case >=0")
		if distance >3 then self:reset(); return; end

		local chest = core.get_inventory(reformer_pos)["main"]
		local stack = chest[self.take_index]
		--self:log("Looking for " .. stack:get_name() .. " wear " .. stack:get_wear())

		for i = 1, #self.inventory_map do
			local entry = self.inventory_map[i]

			if stack:get_name() == entry[1] then
				self:log("Repairing " .. tostring(entry[3]).."[" .. tostring(entry[4]) .. "]: " .. stack:get_name())
				-- put worn tool into reformer chest
				move_stack(entry[2], entry[3], entry[4], reformer_pos, "main", self.put_index, 1)
				-- take good tool out of reformer chest
				move_stack(reformer_pos, "main", self.take_index, entry[2], entry[3], entry[4], 1)
				--
				table.remove(self.inventory_map, i)
				break
			end
		end

		self.take_index = self.take_index - 1
		self.put_index = self.put_index - 1

		if self.take_index == 0 then
			self:log("Repairs complete!")
			-- We've repaired everything that was possible, see if anything remains
			for i = 1, #self.inventory_map do
				local entry = self.inventory_map[i]
				local loc = library.inventory_location_to_string(entry[1])
				self:log("Unable to repair item at: " .. tostring(entry[3]).."[" .. tostring(entry[4]) .. "]: in " .. loc)
			end

			self:reset()
		end
	elseif self.take_index == -1 and distance < 2 then
		print("Case -1")
		-- Initialise. In the chest we find: loads of repair material+tools/armor.
		local chest = core.get_inventory(reformer_pos)
		if not chest then
			self:reset();
			return -- map not loaded yet?
		else
			print("stuff : " .. dump(chest))
		end

		self:add_inventory(nil); -- my own inventory
		self:add_inventory(core.localplayer:get_name() .. "_armor"); -- detached
		-- use the first inventory list
		for list_name, list in pairs(chest) do
			self.chest_list_name = list_name
			self.chest_list = list
			break
		end
		if not self.chest_list then self:log("reformer: chest has no inventory!"); return false end

		self.put_index = #self.chest_list -- the very last slot

		for i = self.put_index, 1, -1 do
			local stack = self.chest_list[i]
			if stack:get_count() > 0 then
				self:log ("Repairs started, using " .. tostring(i) .. " as the first take index: " .. stack:get_name())
				self.take_index = i
				--self.take_index = -3
				break
			end
		end
	elseif self.take_index == -2 and distance < 2 then
		print("Case -2")
		self.take_index = -1
	elseif self.take_index == -3 and distance > 10 then
		print("Case -3")
		self.take_index = -2
--	else
--		print("Case ??")
	end
end

--[[
core.register_globalstep(function(dtime)
	if not core.localplayer then return end
	local my_pos = core.localplayer:get_pos()
	local distance = vector.distance(my_pos, reformer_pos)
	reformer:step(distance)
end)
--]]

function reformer:step()
	if not reformer.enable then return end

	local my_pos = core.localplayer:get_pos()
	if vector.distance(my_pos, reformer_pos) > 5 then
		core.after(3, function() self:step() end)
		return
	end

	local my_inv    = core.get_inventory()
	local chest_inv = core.get_inventory(reformer_pos)

	for my_list_name,my_list in pairs(my_inv) do
		for my_index,my_stack in pairs(my_list) do
			if my_stack:get_wear() > 100 then
				l:debug("Looking for repaired " .. my_stack:get_name()
				        .. " at " .. my_list_name .. "[" .. my_index .. "] with wear: " .. my_stack:get_wear())

				for chest_list_name,chest_list in pairs(chest_inv) do
					for chest_index,chest_stack in pairs(chest_list) do
						if my_stack:get_name() == chest_stack:get_name() and chest_stack:get_wear() < 100 then
							l:debug("Found suitable replacement in chest at index " .. chest_index .. ", wear: " .. chest_stack:get_wear())

							-- Find empty index in chest to hold my damaged tool. In reverse, as whatever that is repairing
							-- tools/armor will put repaired ones at start.
							local empty_index = library.find_item_index_rev(chest_list, "")
							if not empty_index then
								l:err("No empty space left in chest")
								core.after(5, function() self:step() end)
								return
							end

							local empty_loc =         library.ItemLoc:new(reformer_pos, chest_list_name, empty_index)
							local repaired_tool_loc = library.ItemLoc:new(reformer_pos, chest_list_name, chest_index)
							local damaged_tool_loc =  library.ItemLoc:new(nil,          my_list_name,    my_index)

							queue:append(function() return library.queued_move_stack(damaged_tool_loc, empty_loc, my_stack:get_count()) end)
							queue:append(function() return library.queued_ensure_stack(damaged_tool_loc, "", 0, 5) end)
							queue:append(function() return library.queued_move_stack(repaired_tool_loc, damaged_tool_loc, chest_stack:get_count()) end)
							queue:append(function() return library.queued_ensure_stack(damaged_tool_loc, chest_stack:get_name(), chest_stack:get_count(), 5) end)
							queue:append(function() l:info("Successfully repaired "..my_stack:get_name().." at "..my_list_name.."["..my_index.."]") end)
							queue:append(function() return self:step() end)
							queue:walk()
							return
						end
					end
				end
			end
		end
	end

	-- if nothing was repaired, then schedule another round of repairs
	core.after(3, function() self:step() end)
end

core.register_chatcommand("reformer", {
	func = function(param)
		reformer.enable = true
		reformer:step()
		return true
	end,
})
