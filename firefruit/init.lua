--[[
Client-Side Mod (CSM) to display a countdown timer of protection
Copyright (C) 2022 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local mod_storage = core.get_mod_storage()

local state = minetest.deserialize(mod_storage:get_string("firefruit_state")) or {}

state.log_level = nil -- migrated to .log
local l = library.Logger:new("firefruit")

state.time_left = state.time_left or 0
state.timer_running = state.timer_running or false

state.enabled = true


local function update_timer (time_change)
	state.time_left = state.time_left + time_change

	local lplayer = core.localplayer
	if not lplayer then
		l:err ("No localplayer?!")
		return -- bail on WTF
	end

	if state.time_left <= 0 then -- remove HUD
		if state.hud_id then
			lplayer:hud_remove(state.hud_id)
			state.hud_id = nil
		end
		state.time_left = 0
	else
		if not state.hud_id then
			state.hud_id = lplayer:hud_add({
				hud_elem_type = "text",
				position = { x = 0.5, y = 0.45 },
				number = 0xFF0000,
			})

			if not state.hud_id then
				l:err("Wtf, unable to create firefruit HUD?!")
				return
			end
		end

		lplayer:hud_change(state.hud_id, "text", "fire immune for: " .. tostring(state.time_left))

		if not state.timer_running then
			state.timer_running = true
			core.after (1, function()
				state.timer_running = false -- prevent multiple timers
				update_timer (-1)
			end)
		end
	end
end

minetest.register_on_item_use(function(item_stack, _)
	if item_stack:get_name() == "firetree:fruit" then
		update_timer (10)
	end
end)
