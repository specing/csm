read_globals = {
	"ItemStack",
	"INIT",
	"DIR_DELIM",
	"dump", "dump2",
	"fgettext", "fgettext_ne",
	"vector",
	"VoxelArea",
	"profiler",
	"Settings",

	string = {fields = {"split", "trim"}},
	table  = {fields = {"copy", "getn", "indexof", "insert_all"}},
	math   = {fields = {"hypot"}},

	"core",
	"minetest",

	"gamedata",
	os = { fields = { "tempfolder" } },
	"_",

	library = {
		fields = {
			--init
			"p2s",

			"inventory_location_to_string",
			"ItemLoc",
			"ItemLocations",

			"create_help_function",
			"parse_chatcommand_on_off",
			"register_state_chatcommand",

			"Data_Manager",

			"distance",

			"get_random_recipe",
			"list_recipes",
			"load_recipes",
			"parse_cmdline_itemstring",
			"parse_cmdline_recipe",
			"print_recipe",
			"recipe_items_to_counts",

			-- inventory
			"can_multimove",
			"get_hotbar_itemcount",
			"find_empty_index",
			"find_item_index",
			"find_item_index_rev",
			"find_item_index_exclude",
			"find_item_index_in_hotbar",
			"find_item",
			"get_item_range",
			"get_remaining_space",
			"get_result_slots",
			"is_list_empty",
			"parse_inventory_location",
			"queued_ensure_stack",
			"queued_move_stack",
			"setup_move_to_hotbar",
			"setup_queued_move_items_step",
			"setup_queued_move_list",
			"is_tool_ok",
			"do_I_have_space",

			"Logger",

			"place_node",
			"queued_ensure_node",
			"queued_ensure_node_removed",
			"use_item",

			"register_on_jumpdrive_jump",
			"register_on_punchnode_once",
			"register_on_punchnode_once_per_pos",

			"translate_position_if_required",
			"translate_position_list_if_required",
		}
	},

	queue = {
		fields = {
			"append",
			"clear",
			"prepend",

			"walk",
		}
	},

	"add_test",
	"is_table_empty",
}

files["library/init.lua"] = {
	globals = {
		"is_table_empty",
	}
}

files["library/inventory.lua"] = {
	globals = {
		"library.can_multimove",
		"library.get_hotbar_itemcount",

		"library.find_empty_index",
		"library.find_item_index",
		"library.find_item_index_rev",
		"library.find_item_index_exclude",
		"library.find_item_index_in_hotbar",

		"library.setup_queued_move_items_step",
	}
}

files["library/list.lua"] = {
	globals = {
		"List",
	}
}

files["library/data_manager.lua"] = {
	globals = {
		"library.Data_Manager",
	}
}

files["library/logger.lua"] = {
	globals = {
		"library.loggers",
		"library.Logger",
	}
}

files["library/punchnode.lua"] = {
	globals = {
		"library.register_on_punchnode_once",
		"library.register_on_punchnode_once_per_pos",
	}
}

files["library/positions.lua"] = {
	globals = {
		"library.distance",
	}
}

files["library/csm_base.lua"] = {
	globals = {
		"library.create_help_function",
		"library.parse_chatcommand_on_off",
		"library.register_state_chatcommand",
	}
}

files["library/recipes.lua"] = {
	globals = {
		"library.get_random_recipe",
		"library.list_recipes",
		"library.load_recipes",
		"library.parse_cmdline_itemstring",
		"library.parse_cmdline_recipe",
		"library.print_recipe",
		"library.recipe_items_to_counts",
	}
}

files["library/workqueue.lua"] = {
	globals = {
		"queue",
	}
}

files["testing/init.lua"] = {
	globals = {
		"add_test",
	}
}

