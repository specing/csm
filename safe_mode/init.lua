--[[
Client-Side Mod (CSM) to prevent punching/digging dangerous nodes
Copyright (C) 2021-2023 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

-- a record consists of:
--   node_names: list of applicable nodes
--   tool_whitelist:
--   tool_blacklist:
--     digging is allowed if tool is in whitelist or whitelist does not exist and it is not in blacklist
--   list of applicable or non-applicable servers (default: all applicable)
--
-- Upon loading or changes, a LUT is made for current server with node_name -> record pointers

local mod_storage = core.get_mod_storage()
local data_manager = library.Data_Manager:new("safe_mode", mod_storage)
local l = library.Logger:new("safe_mode")

data_manager:register_setting("enabled", "account", true)
data_manager:register_setting("records", "global", {})
data_manager:register_setting("enabled_records", "server", {})--TODO


local state = minetest.deserialize(mod_storage:get_string("safe_mode_state")) or {}

if state.records then -- migrate to DM
	data_manager:set_obj("records", state.records)
	local enabled_records = {}
	for r_name,_ in pairs(state.records) do
		table.insert(enabled_records, r_name)
		l:info("Adding " .. r_name .. " to enabled records.")
	end
	data_manager:set_obj("enabled_records", enabled_records)
	-- delete old format of settings
	mod_storage:set_string("safe_mode_state", "")
end

state.log_level = nil -- migrated to .log

local records = data_manager:get_obj("records")
-- Pre-fill some useful records
records.drawers = records.drawers or {
	-- drawers drop everything when dug. TODO: perhaps check for contents before refusing to dig?
	node_names = {
		["drawers:acacia_wood1"] = 1,
		["drawers:acacia_wood2"] = 1,
		["drawers:acacia_wood4"] = 1,
		["drawers:aspen_wood1"] = 1,
		["drawers:aspen_wood2"] = 1,
		["drawers:aspen_wood4"] = 1,
		["drawers:junglewood1"] = 1,
		["drawers:junglewood2"] = 1,
		["drawers:junglewood4"] = 1,
		["drawers:pine_wood1"] = 1,
		["drawers:pine_wood2"] = 1,
		["drawers:pine_wood4"] = 1,
		["drawers:wood1"] = 1,
		["drawers:wood2"] = 1,
		["drawers:wood4"] = 1,
	},
}

records.geocache = records.geocache or {
	node_names = {
		-- digging it wipes history
		["geocache:block"] = 1,
	},
}

records.nuke = records.nuke or {
	node_names = {
		-- produces a mess
		["technic:hv_nuclear_reactor_core_active"] = 1,
	},
}
records.oresaw = records.oresaw or {
	node_names = {
		['asteroid:copperore']=1,
		['asteroid:diamondore']=1,
		['asteroid:goldore']=1,
		['asteroid:ironore']=1,
		['asteroid:meseore']=1,

		['default:stone_with_coal']=1,
		['default:stone_with_copper']=1,
		['default:stone_with_diamond']=1,
		['default:stone_with_gold']=1,
		['default:stone_with_iron']=1,
		['default:stone_with_mese']=1,
		['default:stone_with_tin']=1,

		['moreores:mineral_mithril']=1,
		['moreores:mineral_silver']=1,

	--	['nether:heart_ore']=1,
	--	['nether:sulfur_ore']=1,
		['nether:titanium_ore']=1,

		['quartz:quartz_ore']=1,

		['technic:mineral_chromium']=1,
		['technic:mineral_lead']=1,
	--	['technic:mineral_sulfur']=1,
		['technic:mineral_uranium']=1,
		['technic:mineral_zinc']=1,

		['terumet:ore_dense_raw']=1,
		['terumet:ore_raw']=1,
		['terumet:ore_raw_desert']=1,
		['terumet:ore_raw_desert_dense']=1,

		['titanium:titanium_in_ground']=1,
	},
	tool_whitelist = {
		["terumet:tool_ore_saw"] = 1,
		["terumet:tool_ore_saw_adv"] = 1,
	}
}

data_manager:set_obj("records", records)


-- Pre-process records to produce a single node->record mapping (speed-up)
local node_to_records = nil

local function update_node_mappings()
	node_to_records = {}

	for _,record in pairs(records) do
		for node_name,_ in pairs(record.node_names) do
			if node_to_records[node_name] then
				l:warn("record for " .. node_name .. " already exists, skipping")
			else
				node_to_records[node_name] = record
			end
		end
	end
end
update_node_mappings()




function is_safe_to_dig(node_name)
	local record = node_to_records[node_name]
	local enabled = data_manager:get_obj("enabled")

	if enabled and record then
		local tool_stack = core.localplayer:get_wielded_item()
		local tool_name = tool_stack:get_name()

		l:debug("Testing tool " .. tool_name .. " for use on " .. node_name)
		if record.tool_whitelist then
			if record.tool_whitelist[tool_name] then
				return true,record
			end
		else
			if record.tool_blacklist and not record.tool_blacklist[tool_name] then
				return true,record
			end
		end

		return false,record
	else
		return true
	end
end

-- Used for registering new nodes
local last_punched_pos = { x = 100000, y = 100000, z = 100000, }
local selected_record = nil

core.register_on_punchnode(function(pos, node)
	if currently_not_allowed then
		return true
	end

	local allowed,record = is_safe_to_dig(node.name)
	if not allowed then
		-- Don't spam the warnings
		if vector.distance(pos, last_punched_pos) < 0.1 then
			return true
		end

		-- TODO: Protect against digging by letting only one punch through.
		-- Some nodes need to be punched for their effect, e.g. geocache.
		last_punched_pos = pos

		local str = "refusing to dig safeguarded node"
		if record and record.tool_whitelist then
			str = str .. "with tool in hand, allowed tools: "
			for tool_name,_ in pairs(record.tool_whitelist) do
				str = str .. tool_name .. ", "
			end
		elseif record and record.tool_blacklist then
			str = str .. "with tool in hand, disallowed tools: "
			for tool_name,_ in pairs(record.tool_blacklist) do
				str = str .. tool_name .. ", "
			end
		--else -- just plain not allowed
		end

		l:err(str)
		return true
	end

	last_punched_pos = pos
	return false
end)



local function help()
	return ".safe_mode help:\n"
	    .. ".safe_mode on\n"
	    .. ".safe_mode off [number]      -- turn off (and save) or temporarily for <number> seconds.\n"
	    .. ".safe_mode list              -- list dangerous nodes\n"
	    .. ".safe_mode record            -- unselect record\n"
	    .. ".safe_mode record delete     -- delete selected record\n"
	    .. ".safe_mode record <record name> -- select record\n"
	    .. ".safe_mode record <record name> add -- add record and select it\n"
	    .. "The following work on a record:\n"
	    .. ".safe_mode reg               -- Registers new dangerous node by punching it\n"
	    .. ".safe_mode reg <string>      -- Registers new dangerous node name <string>\n"
	    .. ".safe_mode unreg <name>      -- unregister node <name>\n"
	    .. ".safe_mode unreg <index>     -- unregister node at <index> in list"
	    .. ".safe_mode tool white[list] <string> -- add <string> as a white-listed tool (also removes it from blacklist)"
	    .. ".safe_mode tool black[list] <string> -- add <string> as a black-listed tool (also removes it from whitelist)"
	    .. ".safe_mode tool delist <string> -- remove <string> from tool white/black-lists"
end

core.register_chatcommand("safe_mode", {
	func = function(param)
		local args = string.split(param, ' ')

		if args[1] == "on" then
			data_manager:set_obj("enabled", true)
			return
		elseif args[1] == "off" then
			data_manager:set_obj("enabled", false)
			local n = tonumber(args[2])
			if n then
				l:info("WIll re-enable after " .. n .. " seconds.")
				core.after(n, function()
					data_manager:set_obj("enabled", true)
					l:info("re-enabled!")
				end)
			-- else TODO.. temporary setting...
			end
			return
		elseif args[1] == "list" then
			for record_name,record in pairs(records) do
				if not selected_record or selected_record == record then
					if selected_record then
						l:info("Showing only selected record " .. record_name .. ":")
					else
						l:info("Record " .. record_name .. ":")
					end

					l:info("  nodes:")
					local i = 1
					for name,_ in pairs(record.node_names) do
						l:info("     " .. i .. ": '" .. name .. "'")
						i = i + 1
					end

					if record.tool_whitelist then
						l:info("  These tools are white-listed / allowed for digging:")
						i = 1
						for name,_ in pairs(record.tool_whitelist) do
							l:info("    " .. i .. ": '" .. name .. "'")
							i = i + 1
						end
					end
					if record.tool_blacklist then
						l:info("  These tools are black-listed / disallowed for digging:")
						i = 1
						for name,_ in pairs(record.tool_blacklist) do
							l:info("    " .. i .. ": '" .. name .. "'")
							i = i + 1
						end
					end
				end
			end
			return
		elseif args[1] == "record" then
			if args[2] == "delete" then
				for record_name,record in pairs(records) do
					if selected_record == record then
						records[record_name] = nil
						selected_record = nil
						data_manager:set_obj("records", records)
						update_node_mappings()
						return
					end
				end
				l:err("Record does not exist")

			elseif args[2] then
				selected_record = records[args[2]]

				if args[3] == "add" then
					selected_record = {}
					selected_record.node_names = {}
					records[args[2]] = selected_record
					data_manager:set_obj("records", records)
					update_node_mappings()
				elseif not selected_record then
					l:err("Record does not exist")
				end
			else
				selected_record = nil
			end
			return

		elseif args[1] == "reg" then
			if not selected_record then
				l:err("first select the record!")
				return
			end

			if args[2] then
				selected_record.node_names[args[2]] = 1
				update_node_mappings()
				data_manager:set_obj("records", records)
				l:info("Added node " .. args[2] .. " to dangerous nodes")
			else
				l:info("Punch a node to add it.")
				library.register_on_punchnode_once(function(_,node)
					selected_record.node_names[node.name] = 1
					update_node_mappings()

					data_manager:set_obj("records", records)

					l:info("Added node " .. node.name .. " to dangerous nodes")
				end)
			end
		elseif args[1] == "unreg" then
			if not selected_record then
				l:err("first select the record!")
				return
			end

			local name = args[2]

			local n = tonumber(name)
			if n then
				for key,_ in pairs(selected_record.node_names) do
					if n == 1 then
						name = key
					end
					n = n - 1
				end
			end

			if selected_record.node_names[name] then
				l:info ("Removing '" .. name .. "' from dangerous nodes.")
				selected_record.node_names[name] = nil

				data_manager:set_obj("records", records)
				update_node_mappings()
			else
				l:err ("Node with name or index '" .. name .. "' does not exist.")
			end
		elseif args[1] == "tool" and args[2] then
			if not selected_record then
				l:err("first select the record!")
				return
			end

			local tool_stack = core.localplayer:get_wielded_item()
			local tool_name = tool_stack:get_name()
			if args[3] then
				tool_name = args[3]
			end

			-- makes following code less complex
			selected_record.tool_whitelist = selected_record.tool_whitelist or {}
			selected_record.tool_blacklist = selected_record.tool_blacklist or {}

			if args[2] == "white" or args[2] == "whitelist" then
				l:info("Added '" .. tool_name .. "' under allowed tools")
				selected_record.tool_blacklist[tool_name] = nil
				selected_record.tool_whitelist[tool_name] = 1
			elseif args[2] == "black" or args[2] == "blacklist" then
				l:info("Added '" .. tool_name .. "' as a forbidden tool")
				selected_record.tool_whitelist[tool_name] = nil
				selected_record.tool_blacklist[tool_name] = 1
			elseif args[2] == "delist" then
				l:info("Removed '" .. tool_name .. "' from white/black list if they contained it")
				selected_record.tool_whitelist[tool_name] = nil
				selected_record.tool_blacklist[tool_name] = nil
			else
				l:info(help())
			end

			if is_table_empty(selected_record.tool_whitelist) then
				selected_record.tool_whitelist = nil
			end
			if is_table_empty(selected_record.tool_blacklist) then
				selected_record.tool_blacklist = nil
			end

			data_manager:set_obj("records", records)
		else
			l:info(help())
		end

		return true
	end,
})
