--[[
Minetest+specing testing Client-Side Mod (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.




This CSM's purpose is to demonstrate the interact functionality present
in Minetest+specing. It will not function with the official Minetest client.
--]]
local l = library.Logger:new("testing")

local all_tests = {}
function add_test(keyword, func, desc)
	table.insert(all_tests, { keyword = keyword, func = func, desc = desc })
end

local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)

dofile(csm_path .. "inspect.lua")
dofile(csm_path .. "interact_inventory.lua")
dofile(csm_path .. "interact_world.lua")


-- Dump server info for clarity, this is copied from Minetest's clientmods/testing:
do
	local server_info = core.get_server_info()
	print("Server version: " .. server_info.protocol_version)
	print("Server ip: " .. server_info.ip)
	print("Server address: " .. server_info.address)
	print("Server port: " .. server_info.port)

	print("CSM restrictions: " .. dump(core.get_csm_restrictions()))

	local l1, l2 = core.get_language()
	print("Configured language: " .. l1 .. " / " .. l2)
end

-- This is an example function to ensure it's working properly, should be removed before merge
core.register_chatcommand("dump", {
	func = function()
		return true, dump(_G)
	end,
})

-- Dig nodes
-- "under" is the position of the node being dug, "above" is the
-- position of the node on our side of the face being dug.
core.register_chatcommand("dig_below", {
	func = function()
		local my_pos = core.localplayer:get_pos()
		local under = { x = my_pos.x, y = math.floor(my_pos.y), z = my_pos.z, }
		local above = { x = my_pos.x, y = math.ceil(my_pos.y),  z = my_pos.z, }
		core.dig_node(under, above)
		return true
	end,
})

-- Place nodes
-- "above" is the position of the new node, "under" is where we are attaching to
core.register_chatcommand("place_below", {
	func = function()
		local my_pos = core.localplayer:get_pos()
		local under = { x = my_pos.x, y = math.floor(my_pos.y), z = my_pos.z, }
		local above = { x = my_pos.x, y = math.ceil(my_pos.y),  z = my_pos.z, }
		core.place_node(under, above)
		return true
	end,
})


core.register_chatcommand("use_below", {
	func = function()
		local my_pos = core.localplayer:get_pos()
		local under = { x = my_pos.x, y = math.floor(my_pos.y), z = my_pos.z, }
		local above = { x = my_pos.x, y = math.ceil(my_pos.y),  z = my_pos.z, }
		core.use_item(under, above)
		return true
	end,
})


core.register_chatcommand("use_item", {
	func = function(param)
		core.use_item(param)
		return true
	end,
})


core.register_chatcommand("craft_stack", {
	func = function(param)
		local count = tonumber(param)
		if count then
			core.craft_stack(count)
		else
			print("How many to craft?")
		end
		return true
	end,
})



core.register_chatcommand("get_wield_index", {
	func = function()
		print("Index: " .. tostring(core.get_wield_index()))
		return true
	end,
})

core.register_chatcommand("set_wield_index", {
	func = function(param)
		core.set_wield_index(tonumber(param))
		return true
	end,
})





core.register_chatcommand("test", {
	func = function(param)
		local args = string.split(param, ' ')

		for i = 1,#all_tests do
			local keyword = all_tests[i].keyword
			if args[1] == keyword then
				all_tests[i].func(args)
				return true
			end
		end

		l:err("Invalid use of .test, help:")
		for i = 1,#all_tests do
			l:info(".test " .. all_tests[i].keyword .. " -- " .. all_tests[i].desc)
		end
		return true
	end,
})
