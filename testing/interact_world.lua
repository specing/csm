--[[
Minetest+specing testing Client-Side Mod (CSM): dig/place stuff
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local l = library.Logger:new("testing")

local function test_facings()
	local my_pos = vector.round (core.localplayer:get_pos())
	-- Position where to place the node
	local pos1 = { x = my_pos.x + 1, y = my_pos.y + 1, z = my_pos.z }
	-- Position where to attach the node
	local attach1 = { x = pos1.x - 1,  y = pos1.y, z = pos1.z }
	core.after(1, function() core.place_node(pos1, attach1) end)

	local pos2 = { x = my_pos.x + 3, y = my_pos.y + 1, z = my_pos.z }
	local attach2 = { x = pos2.x + 1,  y = pos2.y, z = pos2.z }
	core.after(2, function() core.place_node(pos2, attach2) end)

	local pos3 = { x = my_pos.x + 5, y = my_pos.y + 1, z = my_pos.z }
	local attach3 = { x = pos3.x,  y = pos3.y - 1, z = pos3.z }
	core.after(3, function() core.place_node(pos3, attach3) end)

	local pos4 = { x = my_pos.x + 7, y = my_pos.y + 1, z = my_pos.z }
	local attach4 = { x = pos4.x,  y = pos4.y + 1, z = pos4.z }
	core.after(4, function() core.place_node(pos4, attach4) end)

	local pos5 = { x = my_pos.x + 9, y = my_pos.y + 1, z = my_pos.z }
	local attach5 = { x = pos5.x,  y = pos5.y, z = pos5.z - 1}
	core.after(5, function() core.place_node(pos5, attach5) end)

	local pos6 = { x = my_pos.x + 11, y = my_pos.y + 1, z = my_pos.z }
	local attach6 = { x = pos6.x,  y = pos6.y, z = pos6.z + 1}
	core.after(6, function() core.place_node(pos6, attach6) end)
end
add_test("facings", test_facings, "Test placing wielded item with all the possible facings. In the +x direction")





-- Useful for manipulating untargetable nodes (water)
local test_interact_mode = "use"
local test_interact_item_name = "screwdriver:screwdriver"

local function test_interact(pos, name, mode)
	local old_index = core.get_wield_index()
	local index = library.find_item_index_in_hotbar(name)
	if not index then
		l:err("No '" .. name .. "' in hotbar!")
		return
	end

	l:info("Applying " .. name .. " mode " .. mode .. " on " .. minetest.pos_to_string(pos))
	if index ~= old_index then
		core.set_wield_index(index)
	end

	local under = { x = pos.x, y = pos.y,   z = pos.z, }
	local above = { x = pos.x, y = pos.y+1, z = pos.z, }

	if mode == "use" then
		core.use_item(under, above)
	elseif mode == "ruse" then
		core.use_item(above, under)
	elseif mode == "place" then
		core.place_node(above, under)
	elseif mode == "rplace" then
		core.place_node(under, above)
	else
		l:err("unknown use mode: " .. mode)
	end

	if index ~= old_index then
		core.set_wield_index(old_index)
	end
end

local function test_interact_func(args)
	-- args start from 2, as 1 is occupied by command itself
	if args[2] == "mode" then
		test_interact_mode = args[3]
	elseif args[2] == "tool" then
		test_interact_item_name = args[3]
	elseif args[2] == "use" then
		local pos = positions:get(1)
		if pos then
			test_interact(pos, test_interact_item_name, test_interact_mode)
		else
			l:err("Add a position first (.pos help)")
		end
	else
		l:err("invalid use")
		l:info("use: mode use|ruse|place|rplace")
		l:info("use: tool <tool string>")
		l:info("use: use")
	end

	return true
end
add_test("interact", test_interact_func, "Test interacting on the first position with the given item/tool and mode")
