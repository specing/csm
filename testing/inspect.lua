--[[
Minetest+specing testing Client-Side Mod (CSM), inspect part
Copyright (C) 2020,2022 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local l = library.Logger:new("testing")


local function test_find_help(problem, expected)
	if problem and expected then
		l:err("find: " .. problem .. ", expected: " .. expected)
	end
	return l:info("Usage: find in radius <r> nodes [pattern1[ pattern2..]]"
	              .. " [containing pattern1|\"\"[ pattern2.. ]]")
end
local function test_find(args)
	local arg_i = 2
	local arg = args[arg_i]
	if arg ~= "in" then
		return test_find_help("first arg is " .. (arg or "not provided"), "in")
	end

	arg_i = arg_i + 1
	arg = args[arg_i]
	if arg ~= "radius" then
		return test_find_help("2nd arg is " .. (arg or "not provided"), "radius")
	end

	arg_i = arg_i + 1
	arg = args[arg_i]
	local radius = tonumber(arg)
	if not arg or not radius or radius > 50 then
		return test_find_help("3rd arg is " .. (arg or "not provided"), "radius (number) <= 50 (perf)")
	end

	arg_i = arg_i + 1
	arg = args[arg_i]
	if arg ~= "nodes" then
		return test_find_help("4th arg is " .. (arg or "not provided"), "nodes")
	end

	local node_patterns = {}
	while true do
		arg_i = arg_i + 1
		arg = args[arg_i]
		if not arg or arg == "containing" then
			break
		end

		table.insert(node_patterns, arg)
		l:info("Added '" .. arg .. "' to node patterns")
	end
	if #node_patterns == 0 then
		table.insert(node_patterns, "")
	end

	local item_patterns = {}
	-- arg is either null or 'containing'
	while true do
		arg_i = arg_i + 1
		arg = args[arg_i]
		if not arg then
			break
		elseif arg == '""' then
			table.insert(item_patterns, "")
		end

		table.insert(item_patterns, arg)
		l:info("Added '" .. arg .. "' to item patterns")
	end

	-- Right, let's search.
	l:info("Item/node search radius set to " .. radius)
	local my_pos = vector.round(core.localplayer:get_pos())
	local vector_radius = { x = radius, y = radius, z = radius }
	local min = vector.subtract(my_pos, vector_radius)
	local max = vector.add(my_pos, vector_radius)

	local item_to_total = {}

	local pos = { x = min.x, y = min.y, z = min.z }
	for x = min.x, max.x do
		pos.x = x
	for y = min.y, max.y do
		pos.y = y
	for z = min.z, max.z do
		pos.z = z

		local node = core.get_node_or_nil(pos)
		local node_name = node and node.name or "nil"

		for _,npattern in ipairs(node_patterns) do
			if string.find(node_name, npattern) then
				local str = "Found " .. node_name .. " at " .. library.p2s(pos)

				if #item_patterns > 0 then
					local inv = core.get_inventory(pos)
					if inv then
						for _,list in pairs(inv) do
							for _,stack in pairs(list) do
								local stack_name = stack:get_name()
								for _,ipattern in ipairs(item_patterns) do
									if string.find(stack_name, ipattern) then
										local stack_count = stack:get_count()
										l:info(str .. " containing " .. stack_count .. " of " .. stack_name)
										item_to_total[stack_name] = (item_to_total[stack_name] or 0) + stack_count
--										item_totals[name] = (item_totals[name] or 0) + stack:get_count()
--										if item_lut[name] then
--											table.insert(hits, pos)
--										end
										break -- pattern search loop
									end
								end
							end
						end
					end

				else -- no item patterns, print just node info
					l:info(str)
				end
--				l:info("Items at " .. minetest.pos_to_string(pos) .. "(in " .. node_name .. "): " .. str)
				break -- no further pattern matching necessary.
			end
		end

	end
	end
	end

	l:info("Summary of totals:")
	l:info("| item name | max stack | total count |")
	l:info("|-----------|-----------|-------------|")
	for name,total in pairs(item_to_total) do
		local stack_max = ItemStack(name)
		stack_max = (stack_max and stack_max:get_stack_max()) or "?"
		l:info("| " .. name .. " | " .. stack_max .. " | " .. total .. " |")
	end
end
add_test("find", test_find, "find in radius <r> nodes [ containing X]")



local function test_find_items(args)
	local accessible = 0
	local item_totals = {}

	local item_lut = {}

	local min = positions.min
	local max = positions.max

	for _,name in pairs(args) do
		local radius = tonumber(name)
		if not radius then
			item_lut[name] = true
		else
			l:info("Item search radius set to " .. radius)
			local my_pos = vector.round(core.localplayer:get_pos())
			local vector_radius = { x = radius, y = radius, z = radius }
			min = vector.subtract(my_pos, vector_radius)
			max = vector.add(my_pos, vector_radius)
		end
	end

	local hits = {}

	if not min then
		l:err("Need at least two positions to iterate")
		return false
	else
		local pos = { x = min.x, y = min.y, z = min.z }
		for x = min.x, max.x do
			pos.x = x
		for y = min.y, max.y do
			pos.y = y
		for z = min.z, max.z do
			pos.z = z

			local inv = core.get_inventory(pos)
			if inv then
				local str = ""
				accessible = accessible + 1
				local node = core.get_node_or_nil(pos)
				local node_name = node and node.name or "nil"

				for _,list in pairs(inv) do
					for _,stack in pairs(list) do
						local name = stack:get_name()
						item_totals[name] = (item_totals[name] or 0) + stack:get_count()
						if item_lut[name] then
							table.insert(hits, pos)
						end

						if name ~= "" then
							str = str .. name .. " " .. tostring(stack:get_count()) .. ", "
						end
					end
				end
				l:info("Items at " .. minetest.pos_to_string(pos) .. "(in " .. node_name .. "): " .. str)
			end
		end
		end
		end
	end
	l:info("Was able to access " .. tostring(accessible) .. " inventories, with:")
	for name,total in pairs(item_totals) do
		l:info("  count of '" .. name .. "': " .. tostring(total))
	end

	for _,pos in pairs(hits) do
		l:info("  requested item found at " .. minetest.pos_to_string(pos))
	end
end
add_test("find-items", test_find_items, "Find items in bounding box of positions (.pos help)")



local function test_inspect_invloc(inv_loc)
	local inv = core.get_inventory(inv_loc)
	if not inv then
		if not inv_loc then -- wtf, I have no inventory?
			l:err("You have no inventory!?")
		elseif type(inv_loc) == "string" then
			l:err("There is no detached inventory at " .. inv_loc)
		else
			l:err("Node at " .. minetest.pos_to_string(inv_loc) .. " has no inventory.")
		end
		return
	end

	for list_name,list in pairs(inv) do
		l:info("  Contents of inventory list " .. list_name .. ":")
		for i = 1,#list do
			l:info(string.format("    %03d -> %s", i, list[i]:to_string()))
		end
	end
end



local function test_find_nodes(args)
	local node_names = {}

	local min = positions.min
	local max = positions.max

	for _,name in pairs(args) do
		local radius = tonumber(name)
		if not radius then
			table.insert(node_names, name)
			--node_lut[name] = true
		else
			l:info("Item search radius set to " .. radius)
			local my_pos = vector.round(core.localplayer:get_pos())
			local vector_radius = { x = radius, y = radius, z = radius }
			min = vector.subtract(my_pos, vector_radius)
			max = vector.add(my_pos, vector_radius)
		end
	end

	if not min then
		l:err("Need at least two positions to iterate")
		return false
	end

	-- TODO: investigate: .test find-nodes 10 bag
	-- This crashes Minetest 5.6.1 with /usr/lib/gcc/x86_64-pc-linux-gnu/12/include/g++-v12/bits/stl_vector.h:1123: std::vector<_Tp, _Alloc>::reference std::vector<_Tp, _Alloc>::operator[](size_type) [with _Tp = unsigned int; _Alloc = std::allocator<unsigned int>; reference = unsigned int&; size_type = long unsigned int]: Assertion '__n < this->size()' failed.
	-- Aborted
	local groups = minetest.find_nodes_in_area(min, max, node_names, true)
	for node_name,pos_list in pairs(groups) do
		local str = "Found requested node '" .. node_name .. "' at "
		local second_to_last = #pos_list - 1
		for _,pos in pairs(pos_list) do
			str = str .. library.p2s(pos) .. ", "
		end
--[[
		for i = 1,second_to_last do
			str = str .. library.p2s(pos_list[i]) .. ", "
		end
		if #pos_list > 0 then
			str = str .. library.p2s(pos_list[#pos_list])
		end
		l:info(str)
--]]
	end
--[[
		local pos = { x = min.x, y = min.y, z = min.z }
		for x = min.x, max.x do
			pos.x = x
		for y = min.y, max.y do
			pos.y = y
		for z = min.z, max.z do
			pos.z = z

			local node = core.get_node_or_nil(pos)
			local node_name = node and node.name or "nil"

			if node_lut[node_name] then
				l:info("Found requested node '" .. node_name .. "' at " .. library.p2s(pos))
			end
		end
		end
		end
	end
--]]
end
add_test("find-nodes", test_find_nodes, "Find nodes in bounding box of positions (.pos help) or in radius")



local function test_inspect_inv(args)
	if not args[2] then
		l:info("Contents of your inventory:")
		test_inspect_invloc(nil)
	elseif args[2] == "pos" then
		for i = 1, 100000 do -- random high limit
			local pos = positions:get(i)
			if not pos then break end
			l:info("Contents of inventory at " .. minetest.pos_to_string(pos))
			test_inspect_invloc(pos)
		end
	else
		l:info("Contents of detached inventory '" .. args[2] .. "':")
		test_inspect_invloc(args[2])
	end
end
add_test("inspect-inv", test_inspect_inv, "Dump inventory of nil(myself)/string(detached)/\"pos\"[itions](.pos help)")



local function test_inspect_nodes()
	for i = 1, 100000 do -- random high limit
		local pos = positions:get(i)
		if not pos then break end
		dump_node_at(function(str) l:info(str) end, pos)
	end
end
add_test("inspect-nodes", test_inspect_nodes, "Print node metadata and inventory for all positions (.pos help)")



local function test_inspect_dig_time()
	for i = 1, 100000 do -- random high limit
		local pos = positions:get(i)
		if not pos then break end

		local node = minetest.get_node_or_nil(pos)
		local node_name = (node and node.name) or "nil"
		local pos_string = minetest.pos_to_string(pos)

		local hand_dig_time = library.get_dig_time(pos, ItemStack(""))
		local wield_dig_time = library.get_dig_time(pos, nil)
		l:info("Digging time of '" .. node_name .. "' at " .. pos_string
			.. " by hand is " .. (tostring(hand_dig_time) or "nil")
		    .. " and by wielded item " .. (tostring(wield_dig_time) or "nil"))
	end
end
add_test("dig-time", test_inspect_dig_time, "Print digging time for various nodes (.pos help) using tool in hand")



local function test_list_players()
	local players = minetest.get_player_names()

	local pstring = ""
	for _,player_name in pairs(players or {}) do
		pstring = pstring .. player_name .. ", "
	end
	l:info("Players: " .. pstring)
end
add_test("list-players", test_list_players, "List online players, as reported by get_player_names()")

core.after(10+20*math.random(), test_list_players)
