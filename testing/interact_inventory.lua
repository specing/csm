--[[
Minetest+specing testing Client-Side Mod (CSM): inventory operations
Copyright (C) 2021 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local l = library.Logger:new("testing")


local function test_move_stack(args)
	l:debug("Move_stack arguments: " .. dump(args))

	local from_inv,index = library.parse_inventory_location(args, 2)
	local from_list = args[index]
	local from_index = tonumber(args[index+1])

	local to_inv,index2 = library.parse_inventory_location(args, index+2)
	local to_list = args[index2]
	local to_index = tonumber(args[index2+1])

	local count = tonumber(args[index2+2]) or 0

	l:debug("Move_stack parsed arguments: " .. dump(
	  { from_inv, from_list, from_index, to_inv, to_list, to_index, count, index, index2 }))

	if type(from_list) ~= "string" then print("from list name is not a string"); return true; end
	if not from_index then print("from index is not a number"); return true; end
	if type(to_list) ~= "string" then print("to list name is not a string"); return true; end
	if not to_index then print("to index is not a number"); return true; end

	core.move_stack(from_inv, from_list, from_index, to_inv, to_list, to_index, count)
	return true
end
add_test("move-stack", test_move_stack,
	"Move a stack. args: <fromloc> <from list name> <from index> <toloc> <to list name> <to index> <count>")



-- Minetest 5.3.0 detached inventory exploit, fixed in december 2020.
-- Provided here as a museum piece, probably useless nowadays.
local function test_steal_armor(args)
	local player_name = args[2]
	if player_name then
		local my_inv = core.get_inventory()

		local inv_name = player_name .. "_armor"
		local lists = core.get_inventory(inv_name)
		if not lists then
			l:err("Unable to obtain inventory at " .. inv_name)
			return
		end
		local armor = lists["armor"]

		l:info("Player '" .. player_name .. "'s armor: ")
		local empty_index = 0
		for i = 1,#armor do
			local stack = armor[i]
			if stack then
				empty_index = library.find_item_index(my_inv["main"], "", empty_index + 1)
				if not empty_index then break end
				l:info(string.format("  stealing armor:%d (%s) -> main:%d", i, stack:to_string(), empty_index))
				core.move_stack(inv_name, "armor", i, nil, "main", empty_index, 1)
			end
		end
	else
		l:err("Usage: steal-armor <player name>. Armor will be deposited to free slots in your main inv")
	end
end
add_test("steal-armor", test_steal_armor, "Steal someone's armor (Minetest 5.3.0 exploit)")
