--[[
Execute commands on connect
Copyright (C) 2022 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local modstorage = core.get_mod_storage()

local function maybe_send()

	local info = minetest.get_server_info()

	if info and info.address then
		if info.address == "arklegacy.duckdns.org" then
			core.after(5+8*math.random(), function()
				minetest.send_chat_message("/players")
			end)
		end
	else
		core.after(5+8*math.random(), maybe_send)
	end
end

maybe_send()
