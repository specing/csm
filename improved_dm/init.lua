--[[
slightly improved direct message Client-Side Mod (CSM)
Copyright (C) 2020 Fedja Beader

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local csm_name = assert(core.get_current_modname())
local csm_path = core.get_modpath(csm_name)

local l = library.Logger:new("dm")


local function help()
	l:err("Usage: dmdest <nickname> -- sets nickname for future .d[m]-ing")
	l:err("Usage: d <text> -- sends DM")
end


local dm_target = nil
core.register_chatcommand("dmdest", {
	func = function(param)
		if param and #param > 0 then
			dm_target = param
		else
			help()
		end
		return true
	end,
})
core.register_chatcommand("d", {
	func = function(param)
		if not param or #param == 0 then
			help()
		elseif not dm_target then
			l:err("Set DM target nickname first (.dmdest <nickname>)")
		else
			core.send_chat_message("/msg " .. dm_target .. " " .. param)
		end
		return true
	end,
})
